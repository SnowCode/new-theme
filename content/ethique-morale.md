+++
title = "Ethique et morale"
date = 2024-05-19T18:17:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
Nous allons voir ici la différence entre le droit, l'éthique la morale et la déontolgie. 
<!-- more -->

On dit qu'un problème est **éthique** ou **morale** si il met en jeu des idéaux qui donnent sens à notre vie ou des règles auxquelles on se sent soumis.

Il faut donc bien distinguer le droit de l'éthique et la morale. Le droit est basé sur la morale et pour certaines personnes la morale est basée sur le droit. Mais il est important de comprendre que l'éthique et la morale sont strictement personnel pour chaque individu, contrairement au droit.

Il y a beaucoup de définition des mots "ethique" et "morale" mais une définition proposée dans le cours est celle-ci : 

- L'**éthique** est l'étude philosophique de la morale. C'est une réflexion argumentée en vue du bien agîr.
- La **morale** est l'objet de l'éthique et un ensemble de valeurs et de principes moraux (bien, mal, juste, injuste, etc)

Il est donc important de comprendre que ce n'est pas parce que quelque chose est illegal qu'il est immoral ou que par ce que quelque chose est immoral il est illégal.

Ce n'est pas non plus par ce que la morale est personnelle qu'il n'y a pas de sanction, il peut y avoir une sanction par le désapprouvement par une personne ou par un groupe.

## Déontologie
La **déontologie** est un ensemble d'obligation refletant des valeurs et des principes jugés fondamentaux que des personnes sont tenues de respecter dans leur profession. 

Le code de déontologie est donc adopté par les personnes afin de protéger le public et préserver la réputation de leur profession et de leur place dans la profession. 

