+++
title = "How to be more free and autonomous"
date = 2024-05-25T14:22:00+02:00
draft = true

[taxonomies]
tags = [ "english", "autonomy", "freedom", "anarchy", "diy", "lifestyle" ]
+++

There are many tasks of daily life which I was never taught to do. Things such as cooking, cleaning, maintaining relationships for instance. 

And there also are tasks which I was taught to do (either explicitely, or implicitly by seeing everybody doing it) but where the ways to do so don't satisfy me. For instance, heating an entire house for the sake of feeling warm, when you could just hot water bottles (due to ecological and economical reasons for instance).

So I always tried to find other ways to do things, break the social rules surrounding how I should live my life. This can be as simple as finding a sort of "method" to help me in tasks such as cooking or eating. Or as complicated as adopting different ways to communicate to others and yourself or see relationships differently.

Since I do this thing constantly I often feel anxious and frustrated from not having everything clear in my mind. Also, sharing is VERY important to me, so I wanted to try to make things clearer for me while also making it in a way that could easily be shared to others, hence this page.

## How to keep yourself cool in hot weather
I handle hot weather very badly compared to cold weather. Many people use air conditioning in order to stay cool. I don't like that because the idea of depending on a pretty polluting, complicated, and expensive system just for my basic comfort in every summer makes me feel pretty unsafe and uncomfortable.

That's why I try to find some alternatives to feel more comfortable even if it's crazy hot outside. I'm not always satisfied because I have to be honest, it's a lot less efficient than AC, but usually it does help me quite a lot.

- Use a hand fan, it's really effective and practical and is also pretty cool in my opinion
- Use a water spray and spray myself, it sounds dumb but it's incredibly effective
- Close all the curtains of the house in order to limit the heat as much as possible
- Open every windows that have a mosquito net at night to let cool air in
- Try to stay as far away from ceiling as possible (the basements are usually the best because they're feel cool)
- Always keep water to drink. Something even better is to use some ice and a isolated bottle to keep the water fresh
- Try to avoid moving too much (it's pretty hard considering I always walk in circle)
- Use a circulating fan, while it still uses electricity, it's way better than AC. 
	- I also discovered [old circulating fans](https://solar.lowtechmagazine.com/2014/09/the-revenge-of-the-circulating-fan/). I don't have any but I think it's pretty cool and deserve to come back to daily life.
- Of course, dress lightly (or don't use any clothes at all :p)
- Put a wet cloth on your neck or running put water on your wrists to cool yourself down. I discovered this idea just now I think it's really cool and will try it out some time.
- Take a shower, especially since you'll likely get dirty a lot more quickly.
- Of course, ice cream :p

## How to cook, eat and clean
For me eating always has been a pretty difficult thing, I was very easily triggered by food texture or flavors. Also my parents hate cooking so I never really learned to cook or appreciate cooking.

Another problem I had is that I have a hard time noticing hunger or "fullness" so I would often eat either too much, too little or forget to eat altogether.

I also learned the existence of something called "intuitive" eating which consists of instead of working with numbers or randomness, trying to feel our body in order to understand when we're full or when we're hungry.

Another thing that also made me feel uncomfortable and very anxious is that I never know *what* to eat. One day I saw some people explaining cooking in a way that really made it transparent to me too. So that's what I want to share with you :)

### Cooking
1. Set a regular alarm clock to remind you of eating 
2. Pick a source of carohydrates, for instance : bread, potatoes, pasta, noodles, pizza dough, etc.
3. Pick a source of proteins, for instance : meat, eggs, tofu, beans, mushrooms, etc.
4. Pick a source of micronutrients (like vitamins and stuff) : tomatoes, salad, carrots, onions, peppers, etc.
5. Pick some flavors you like (so spices and/or sauces, if you don't know just take pepper and salt) : ketchup, BBQ, mayonnaise, pepper, salt, curry, paprika, etc.
6. Pick a cooking method (basically how to prepare those ingredients), for instance : oven, frying pan, mixing, mixing and heating, grill, etc. Usually the fastest is the frying pan.
7. Apply the cooking method on all your ingredients. If you don't know how to do so, or how to prepare some ingredients, there are plenty of stuff online to help you with that.

If you want to be more inspired and get some new ideas there are a TON of ideas online of people sharing some ideas of meal.

Also, I would say to not worry about quantities to much, if you do too much it's OK you'll have some for the next day, if you add too much spices you can offset  it with cream, if you add too little spices you can always add some later.

If it's undercooked, you can also still cook it again, and if it's overcooked, usually you can remove some parts of it.

### Eating
For eating and better understanding your body and notice when to stop eating here are the tips I've done and seems to work well:

- Eat slowly and focus on the sensations in your body as you eat
- Drink water while eating (better a lot than too little)
- Standing up helps with better feeling how food feels in your body
- Remember it's better to eat too little than too much, you can always take some later if you're hungry again. And if you're afraid of waste, don't worry you can still put it in the fridge and eat it the next day.

### Cleaning
I find it very important to clean stuff immediatly afterward instead of waiting for the dirty dish pile to be full.

Of course if you have a dishwasher use it, but if you don't you can quickly run water on everything and use a tiny bit of soap on it. 

If there is a lot of dishes (usually when there are more than yourself to eat), you might want to invite some people to help you with the dishes, you can then talk while doing it and it makes it more fun :p

Finally, clean the surfaces of the hotplate and the countertop by simply using a dishcloth.

## How to feel better (handle conflict, understand yourself, communicate better, empathaize with others, and so much more)
This section, in my opinion is the most important one. It means so much to me I'm actually anxious about writing it because I'm scared of not doing it right so I'm going to try my best.

This is about non-violent communication or "empathic communication". 

I've always had a very hard time understanding my own emotions, and for many reasons I also felt responsible for the emotions of others around me making me feel very frustrated, angry and depressed most of the time due to the pressure.

Not only that but I noticed that feeling bad while trying to help somebody who also feels bad is the worst idea ever because you end up hurting yourself AND the other person in the process.

The idea behind NVC is that in order to empathise and help others or in order to communicate and maintain relationships with others you first need to understand yourself to make sure that your intentions while communicating are curious, open and calm instead of waiting for some precise outcome and being stressed.

![Communcation flowshart from ZeNVC illustrating the idea explaine above](/communication-flowshart.png)

I also read the book about NVC and I was amazed about how NVC can be used in so many ways. For example it can be used to express gratitude (explaining what you feel toward a certain action and what needs have been met), help negotiate a price with someone (I've seen people using NVC with Pay What You Want), etc.

I would really recommend you to read Marshall Rosenberg's book about NVC to learn about more examples and more details about NVC. 

My personal practice of NVC is to try to do something called the "Yoga of self connection", it might sounds cheezy but it works extremely well.

![Picture illustrating the idea of yoga of self connection](/yoga-self-connection.png)

The idea is this :

1. Go to a place where you're at peace, it can be your bedroom, the toilets, outside, etc.
2. Take a deep breath, put your hands on your head and notice what thoughts are going on in your head, what you saw, what you heard, etc.
3. Take a deep breath, put your hands on your heart and notice what you *feel* in your body (example : sadness, stomach ache, hunger, anxiety, confusion, anger, etc)
4. Take a deep breath, put your hands on your belly and notice what you *need*, those needs are universal and you can usually deduce them from your feelings or by imagining a dream situation. For instance, those can be clarity, calm, peace, connection, meaning, etc.
5. Take a deep breath, put your hands "open to recieve" and think about a pratical way to fill this need. 

To do this, take your time, especially if you never done it before. I personally think the process itself is fairly easy, what makes it harder is to apply it consistently before talking, before listening, when you're in bad situations, etc. I still struggle to apply it on time but even when I do it a little late (for example after a conflict instead of before) it still helps me a LOT.

So once you've self reflected, if you want to go talk or listen to someone ask yourself what are your intentions when doing so. Do you want to listen/talk to them out of curiosity, without expecting any specific outcome and while being relaxed? Or are you tense, wanting a specific thing from them and wanting to be right?

If you are in the first situation, then it's cool go ahead :) You can then express what you've learned in your self-reflection for instance. And doing this self reflection also helps you understand the needs of the people you're listening.

I think I rambled quite a lot here, so I'm just going to leave you with some recommendations of stuff to read/watched which I personally think changed my life to give me a lot of clarity, peace, joy and connection with others.

- [Marshall Rosenberg's Sanfransisco Workshop](https://youtube.com/watch?v=l7TONauJGfc) is a 3 hour, free video explanation of NVC. It's pretty fun to watch and very complete.
- Marshall Rosenberg's book "Non violent communication, a language of life" which talks about the same tihngs as the workshop but with more details, background and examples.
- [The YouTube channel "Cup of empathy"](https://youtube.com/channel/UC2AK8ipP18I0alKXZXJgzbQ) gives a lot of examples of how NVC looks like by doing it herself. I find it incredible because you really feel connected to her and feel the power of NVC by seeing it applied to real examples. 
- [The YouTube channel "Giraffe NVC"](https://yewtu.be/channel/UCqyV2p_xGVr5gSfldc_ov-A) gives short videos of talks by Marshall Rosenberg, some of which help add more context around NVC and better understand ways to handle some precise tough situations.
- [ZeNVC handouts](https://zenvc.org/handouts/) are infographics to explain and guide through NVC. The ones I used here came from that website.
- [Praticing self care step-by-step](https://www.thetrevorproject.org/blog/practicing-self-care-step-by-step/) is a pretty cool infographic about basic self care (getting your most basic needs met) which isn't really NVC but is useful to do before doing NVC because it will make things easier to understand yourself if your basic needs are met
- [Mapping Our Madness](https://snowcode.ovh/mapping/) is a workbook which you can complete to be your "default" plan for you (or others around you) to use when you're unable to do NVC because you're in crisis. 

## General process
I consider every spending and energy use to be a dependency on civilization. If you want to feel more free and autonomous, you thus have to reduce this dependency or choose whom you want to be dependent on.

This is how I want to proceed : 

1. List every major categories of spending and energy use (ordered by how important they are), for instance food you eat outside, food you cook, medicine, etc.
2. For each of them, ask yourself a bunch of questions
	- Is it really necessary or can you cut it off?
	- Can you reduce the usage of it ?
	- Can you change the thing (machine for instance) to become more sustainable, cheaper and resilient ?
	- Can you replace it with something else ?
	- Can you make it yourself ?
	- Can you get it for cheaper or for free ?
	- Could I mutualize the cost or the thing with other people ?
3. Keep track of what you do to not get lost and eventually measure the impact of it.

This thus requires to have a good knowledge of your own needs in order to find what are the different needs pushing you to consume different categories of things.

Also remember that whatever happens, you will never be 100% self sufficient, we are social creatures and need others to survive. So self-resilience is and will always be collective resilence as well.

Also remember that usually, things that you don't buy, will cost you time. So pay attention to the time resources you have as well. That's why I think it's unrealistic to expect to keep the same lifestyle and be more free at the same time.

This part was the most important bit, the rest is just my personal ideas and experimentations about them.

I just want to share those ideas because 

## Cooking, eating, etc

### Cooking your meal
1. Set a regular timer to remind yourself to eat at a certain time, in order to not start cooking when you're already starving. For example 12h30 everyday.
2. Pick a source of carbohydrates from your fridge or go get some at the grocery store. It can be potatoes, bread, pasta, rice, etc.
3. Pick a source of protein from your fridge or go get some. It can be eggs, lentils, tofu, meat, mushrooms, etc.
4. Pick some vegetables/fruits from your fridge or the grocery store. It can be carrots, onions, tomatoes, peppers, salad, etc.
5. Choose a way to cook. It can be in the oven (as a casserole), in a frying pan, mixed as a soup or juice, etc. 
6. Choose some spices and/or sauce that you like 
7. If you're not sure lookup how to prepare each ingredients or cook in a certain way, look it up !
8. Prepare each ingredient 
9. Cook them and add the spices/sauces. Be patient and let it cook long enough. Taste it to check if the spices and cooking are to your liking.

### Eating your meal
1. Eat slowly in order to feel your body better
2. Drink while eating it can often play in the feelings of hunger or fullness as well
3. Notice the feelings in your belly and your body in general as you eat. Feel the hunger fading away and pay attention to not feel uncomfortable either.
4. If there is some food left, keep it away in the fridge so you can have some tomorrow (or at the next meal).

### Cleaning up
1. Rinse everything you use and put a little bit of soap on it. You just have to pass it on everything quickly and rinse it again.
2. Either let it dry on something or dry it yourself and store it somewhere
3. Wash the cooking plates, sink, etc quickly to avoid having dirty stuff getting everywhere.

Congratulations, you just cooked your ow healthy meal, started a new routine, cleaned everything by yourself, learned how to listen to your body better, tried new things, and might even have prepared your own meal for the next day. 