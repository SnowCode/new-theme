+++
title = "Welcome to my new blog !"
date = 2024-05-14
[taxonomies]
tags = [ "blogging", "about", "english" ]
+++

I hope I'll post frequent updates and stuff here, soon. See you soon I hope!

This is the first post in a maybe serie and at the time I write this I don't even know when or even if it will ever be published.

It's just that I never managed to actually sustain a blog and it frustrates me. I've started a blog quite some time ago, then I've also tried managing a gemini capsule.

<!-- more -->

The thing is I never really managed to continue doing so. So I thought I could maybe do some tumblr-like content in the sense that tumblr seems to be very varied with many different medias (links, videos, audio, text, images) of different lengths (short posts, long posts).

And I noticed that at the time when I was on Activity Pub, I also was quite active there. So my idea here is just to stop worrying, dumping ideas, images or anything in here when I want to and publish it later.

Even if, to be honest I am mostly doing this out of procrastination right now x)

## How I plan to do this
I recently found [Kottke](https://kottke.org/)'s blog and I really find it awesome. The design is great and the content is incredibly varied. 

The tag features means that even though there can be any sort of content (long, short, images, link, many different topics, etc), it remains organized.

When I successfully gemlogged on Gemini, it was also due to the fact I made the barrier to logging as low as possible by creating a kind of "microblog" and creating a shell alias so I would just have one command to type to write my thoughts and move on.

So I did the same here, I setted up a Zola blog with `after-dark` theme (I will probably change that in the future). And I created an alias to create automatically a new entry based on a template.

```bash
alias post="cd ~/blog/content && cp ../template.md"
```

My template in my case is this : 

```bash
+++
title = "Welcome to my new blog !"
date = 2024-05-14

[taxonomies]
tags = [ "tag1", "tag2" ]
+++

<!-- more -->
```

The top part represents the meta data, and the "more" tag is a divider. Anything before this tag will be shown on the home page as a short description, all the rest will only be available when you click on the post.

That way I can write both short and long entries like I want it! I would also like to write course notes and stuff here, but I don't yet know how to make it easy to read and organized. It's just that I'm kind of getting sick of using Bookstack and I always wanted to blog.

So maybe I could slowly transition to here. Also, I forgot to mention but this blog will very likely be bi-lingual (English and French posts together).
