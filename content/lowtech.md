+++
title = "Why low-tech is the real future"
date = 2022-06-01 15:08:35+01:00
[taxonomies]
tags = [ "english", "anarchy" ]
+++
We often hear that high techs are going to save the world, that electric cars, geoingeenering, medicine, etc; are going to save the world from global warming, crisis, etc. I disagree with this statement. I think many problems occuring in the world are occuring because of high technology and I also believe no technology is neutral. I think we should, in the opposite manner, thrive toward "low techs" instead. <!-- more -->

## High tech problems
First, high-techs are expensive and not durable. They require a lot of complex infrastructure to be maintained, sustaining all this infrastructure costs a lot. So not everyone can afford this type of technology. For instance, producing a phone requires the extraction of many resources in all parts of the world, to be assembled in another part of the world, then shipped through other expensive technologies to your place where you can start using it, which is itself dependent on an entire infrastructure of mobile network, internet and electricity grid. 

Second, high-techs are unsustainable. This entire complex infrastructure is based on the exploitation of finite resources worldwide, in a world where those resources are becoming always more scarce, this model is doomed to failure. The entire industry is based on fossil fuels and many materials have to be extracted, there is pretty much no part of the process from extraction, creation, usage and throwing away that isn't harmful to the environment. 

Third, high-techs are based on a system and infrastructure that is so complex it makes us dependent on the entire system. It also means repearing things become always more difficult and troubleshooting issues within such complex technologies is becoming close to impossible. For instance, there is no way to repear most expensive technologies yourself, even with expertise, without buying parts from far away. Of course you still have the option of second-hand and parts from other devices, but it remains limited on our current extra-consumtion of resources.

Fourth, high-techs makes us dependent on the entire system, and is a way to impose domination. As things can't be produced locally, nor repeared locally, they require you to depend on an exploitative system of capitalism and inequality. It kills the local scale and the basic relationship of individuals within their community. To illustrate that, rural exodus is a good example. People left their local scale, villages, etc. To work for industries to the profit of the system that exploits them through wage labor, and maintain its domination by creating a dependency on high-techs (industrialized goods) that become necessary to live "in society". 

## Low tech solutions
Low techs are the opposite of high techs in all points. It's local, cheap, simple, social and sustainable.

Low techs are cheap, because of their simplicity and minimalism they are very easy to build, they can sometimes even be made from litteral junk. For instance, a simple wood gasifier can be made from just two old cans, a bit of wood, a match and a knife.

Low techs are simple, understanding how it works and thus how it can be improved and repeared is easy. Which makes it more a "tool" and less a "mysterious black box". It doesn't mean science is not involved, for instance the wood gasifer works by having the wood being heated in a low-oxygen environment, which releases "syngases" which are then burned in the upper part of the stove. It allows a much greater energy efficiency than traditional open fire (5-15% for open fire, 30-45% for wood gasifier and 40% for average gas stove).

Low techs are also local and social, because they are cheap, simple and sustainable, they can also be made by everyone, for everyone. They make us more independent from global industrial systems. You don't need capitalism to function to recycle old cans, re-use old materials, learn bushcraft skills, etc. 

Low techs are sustainable. Most so called "traditional" technologies are low-techs. And because of their nature of being cheap, simple, local, etc, they can hardly do real damage to the environment. There is very limited environemental damages from the production and usage of those technologies, and after use they can also be recycled to make otehr low techs once again.

Low techs often are heavily inspired from "traditional" technologies which means they have been used for a long time and we know they work. They aren't experimental, we already know where they perform best and how. You might not know how to repear your central heating, but you know how to use blankets and hot water bottles. 

Finally, low techs aren't a return to the past, they are a path to the future, they require important lifestyle changes, but I think it's better to choose to make those changes ourselves and take our freedom, rather than having lifestyles imposed to us by the economical system we live in or by climate crisis itself.

## Conclusion
To conclude, high techs aren't the solution to all our problems, far from it. High techs are more complex, more expensive, more exploitative, unfair and unsustainable. And it doesn't even mean it performs better than low tech alternatives. 

In contrast, low techs are simple, cheap, social, local, sustainable and fair. I don't think all high techs should be avoided, some are hard to avoid like internet or computers for instance. But we can try making them "lower techs" by re-using, recycling, decreasing our usage of it, etc. There's a personal equilibrium to find there.

Would you be ready to change (parts of) your lifestyle and replace some high technologies by low(er) ones? If so where would you start?

## Going further
If you are interested by this topic you might be interested by a few resources:

* Low Tech Lab Wiki which collects some tutorials for making and using low techs
* Bushcraft which gives very good skillsets and techniques for living in the woods
* Solar.LowTechMagazine.com which also talks about high tech problems more indepth, older technologies that remains interesting and new low tech inovations. 
* [ADD MORE LOW TECH BOOKS HERE, INCLUDING FRENCH]
+ add more references
