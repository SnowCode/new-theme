+++
title = "Sécurité réseau"
date = 2024-05-16T11:46:00+02:00
draft = true

[taxonomies]
tags = [ "sécurité", "cours", "réseau", "synthèse", "français" ]
+++

La sécurité est une préoccupation assez récente, beaucoup d'applications n'étaient pas sécurisées avant (POP3, Telnet, FTP, IMAP, etc). L'ajout de la sécurité a été fait par après car il était impossible de faire migrer tout le monde vers d'autres solutions. Mais il est important que la sécurité soit déjà présente au coeur même de chaque application.

<!-- more -->

Surtout que la mise en réseau n'arrange rien car il y a beaucoup plus de possibilités d'attaques, surtout qu'une très grande proportion du traffic internet concerne des tentatives d'attaque.

## Qu'est ce que la sécurité réseau et comment ?

La sécurité réseau recouvre un ensemble de domaine tel que la **confidentialité des informations** (grâce à du chiffrement, de façon à ce que l'information soit uniquement compréhensible par l'émetteur et le récepteur), l'**authentification des entités** (vérification de l'identité, encore une fois grâce à du chiffrement), l'**intégrité des messages** (détection de la modification d'un message, ne pas pouvoir le renier, encore une fois grâce aux hash cryptographiques), et enfin la **disponibilité et le contrôle d'accès** (protéger contre les (D)DoS, et contrôler l'accès aux ressources de l'application).

- Il faut donc utiliser le **chiffrement PARTOUT** pour permettre de garantir toutes ces conditions.

- Il faut également utiliser un **pare-feu** qui va limiter et contrôler les accès à une machine ou à un réseau avec un ensemble de règles (en fonction des ports, du type de traffic, des IP, etc).

- Il faut aussi **opter pour une solution centralisée** pour mieux pouvoir monitorer les serveurs.

- Il faut pouvoir être **pret à réagir**, pouvoir monitorer les attaques et en cas extrème de pouvoir restaurer les backups.

- Il faut aussi **rester informer** sur les nouvelles à propos de la sécurité afin de pouvoir réagir rapidement et préventivement

- Enfin, il faut **mettre à jour les applications** afin d'éviter les bugs et bénéficier des améliorations de sécurité. Il faut cependant faire attention à ce que les mises à jour n'impacte pas les données de l'application, et n'intègre pas de nouvelles vulnérabilités.

## Le chiffrement

Il est indispensable d'utiliser du chiffrement asymétrique car tout autre méthode pourrait être interceptée et rejouée.

On va alors utiliser un système de "challenge cryptographique", une fois que l'utilisateur·ice veut se connecter, le serveur va lui demander de signer/chiffrer un texte aléatoire avec sa clé privée. Le serveur demande alors quel est la clé publique, et l'utilisateur·ice envois sa clé publique.

![](/home/snowcode/blog/static/2024-05-16-12-37-11-image.png)

Ce système permet de faire un échange sécurisé dans la majorité des cas, sauf dans le cas d'un **man-in-the-middle attack**.

![](/home/snowcode/blog/static/2024-05-16-12-39-09-image.png)

Ici, Eve intercepte les données et retourne sa clé publique à la place de celle d'Alice, elle est donc authentifiée et peut intercepter le traffic sans qu'Alice ne s'en rendent compte.

Contre cela on utilise donc des systèmes de tiers de confiance. Il y a deux systèmes de tiers de confiance principaux les **KDC (Key Distribution Center)** (utilisées pour les systèmes à clé secrète, c'est-à-dire de la cryptographie symmétrique ou la même clé peut être utilisé pour chiffrer et déchiffrer) et les **autorités de certifications** (qui est utilisée dans les systèmes à clé publique-privée, c'est-à-dire lorsqu'il y a une clé différente pour chiffrer et déchiffrer).

### KDC (Key Distribution Center)

Dans le système du KDC, on a un serveur au quel chaque membre souhaitant communiquer donne une clé secrète.

Lorsque deux machines veulent communiquer, l'une demande au serveur en chiffrant sa demande, de se connecter à l'autre. 

Ensuite le serveur génère une clé de session et la même clé de session chiffrée avec la clé secrète du destinataire.

Ensuite on envois cette clé chiffrée au destinatire afin que tout le monde aie connaissance de la clé de session. La communication est à présent établie.

 ![](/home/snowcode/blog/static/2024-05-16-13-55-04-image.png)

### Autorités de certifications

Le KDC possède un gros problème car il est central, il connait les clés secrète de tout le monde (donnée très sensible) et la clé ne peut pas à l'origine être envoyée au serveur de manière sécurisée. 

Alors pour sécuriser sur Internet le protocole le plus utilisé est **SSL/TLS (Secure Server Socket et Transport Layer Security)**. TLS représente les dernières versions de SSL.

Dans ce système on possède une **autorité de certification** qui va permettre de vérifier l'identité d'une personne en la signant avec son propre certificat.

Dans ce système l'administrateur·ice système va générer une clé privée et une clé publique. Iel va ensuite passer cette clé publique à l'autorité de certification qui va faire certaines verifications dont au minimum vérifier par un challenge que le client possède bien la clé privée qui va avec la clé publique.

Si tout est correct, l'autorité de certification va signer la clé publique en disant "Je confirme que helmo.be a bien la clé suivante". 

Ensuite lorsque le navigateur va charger la page, il va vérifier si le certificat du site est bien signé par l'une des autorités de certifications qu'il connait, si oui il vérifie que le certificat n'est pas expiré. Si tout est bon, la page peut alors être affichée.

![How Certificates and HTTPS work](https://everydaycyber.net/wp-content/uploads/2021/12/EDC-What-is-a-certificate-authority-CA-and-what-do-they-do-How-Certificates-and-HTTPS-work.png)

Les navigateurs ont donc une liste d'autorités de certifications auquel ils font confiance, mais il faut aussi savoir qu'il peut y avoir une hierarchie d'autorité de certification.

![https://everydaycyber.net/wp-content/uploads/2021/12/EDC-What-is-a-certificate-authority-CA-and-what-do-they-do-Chain-Of-Trust.png](https://everydaycyber.net/wp-content/uploads/2021/12/EDC-What-is-a-certificate-authority-CA-and-what-do-they-do-Chain-Of-Trust.png)

Les navigateurs ont ainsi seulement besoin de connaître les autorités racine, pour pouvoir vérifier les certificats qui ont été autorisé par les autorités approuvées par les autorités racines.

Les vérifications qui sont faites par les autorités de certifications varient, certains vont vérifier les données légales de l'entité qui demande la certification, d'autres vont simplement vérifier qu'elle a bien la clé privée (c'est nottament le cas de *Let's Encrypt* qui est une autorité de certification gratuite et qui ne fait aucune vérification légale).

Un site sécurisé par Let's Encrypt ne signifie pas pour autant qu'il n'est pas fiable bien que beaucoup de sites de phishing l'utilise pour ne pas avoir à révèler leur identité. Let's Encrypt est surtout majoritairement utilisé pour des fins parfaitement légitimes.

#### Fonctionnement de SSL/TLS

La plus pars des sites Internet sont sécurisé à l'aide de l'un de ces protocoles. Lorsque Alice va sur le site de Lea, Lea va envoyer son certificat, Alice va ensuite vérifier qu'il est valide en le vérifiant, ensuite Alice va générer une clé de session qu'elle va communiquer à Lea en le chiffrant avec sa clé publique.

Lea et Alice ont donc maitenant la clé de session qui leur permette de communiquer. On utilise alors la clé de session secrète car elle est plus rapide à utiliser pour chiffrer les données que de tout faire avec la clé publique.

A savoir que ce n'est pas par ce que le site est marqué comme sécurisé que ce n'est pas une arnaque. Si vous allez sur un site de fishing marqué comme sécurisé cela veut seulement dire que personne d'autre que vous et l'anarqueur·euse n'aura accès à vos données.

## Contrôle d'accès (firewall et proxy)

Il est très important de chiffrer le canal, mais il faut aussi faire attention que n'importe qui ne puisse pas y entrer. Cela se fait via des mécanismes de firewall ou de proxy. Mais il faut évidemment que l'application soit elle-même bien configurée [pour empécher n'importe qui d'accéder au panel admin](https://maia.crimew.gay/posts/fuckstalkerware-5/). 

Pour ce qui est du **pare-feu (firewall)**, cela consiste à isoler des éléments du reste du réseau. Par exemple en mettant en place un DMZ (demilitarized zone) qui consiste à faire en sorte que seule une machine du réseau soit accessible sur le réseau, ou encore en mettant en place un ensemble de règles sur le traffic entrant et/ou sortant.

Par exemple on peut définir que le traffic sortant est illimité mais que seul le port 80, 443 et 22 d'une machine particulière sont accessible depuis internet (traffic entrant).

Mais il est aussi possible de filtrer avec plus de finesse par exemple en utilisant des outils tel que Fail2Ban (Stateful inspection) qui va filtrer les paquets pour interdire l'accès à des adresses IP ayant fait trop de tentatives. 

Ou encore CrowdSec qui consiste à créer une base de donnée d'adresses IP d'attaquant partagée afin de pouvoir plus facilement détecter les adresses IP malicieuses.

Ou encore des NGFW (Next Generation Firewall) qui permet d'identifier les applications, le type de site, le type de traffic, etc. Ces derniers fonctionnent en catégorisant un maximum de site internet et en créant des règles sur base de ces catégories.

Enfin pour pouvoir centraliser tout le traffic à un endroit on peut utiliser un proxy qui va traiter les requètes en amont. C'est par exemple le cas de Cloudflare qui va protéger la véritable adresse IP des sites internet et qui va nottament protéger les sites contre les attaques DoS et DDoS.

Une bonne règle générale avec les firewall est de faire les choses sur base du motto "*Tout ce qui n'est pas explicitement autorisé est interdit*".

## IPSec (IP Security)
Le concept de l'IPSec est de sécuriser la couche réseau elle-même plus tot que de sécuriser la couche application.

Cela consiste à chiffrer les données des paquets d'une manière ou d'une autre (clé publique/privée, clé secrète ou clé de session). Bien-sûr tout le paquet n'est pas chiffré, seul les données le sont, sinon les routeurs ne saurait pas à qui donner le paquet.

Un autre avantage de l'IPSec est qu'il est impossible de se faire passer pour quelqu'un d'autre (IP Spoofing, c'est-à-dire définir comme adresse source d'un paquet, une adresse différente de la sienne pour se faire passer pour quelqu'un d'autre, ce qui est la raison pour laquel l'IP n'est pas une bonne source d'authentification).

L'IPSec utilise deux protocoles particuliers le premier est **le protocole AH (Authentication Header)** qui permet d'authentifier la source et garanti l'intégrité des données, mais cela *sans chiffrement*.

Ensuite il y a le protocole **ESP (Encapsulated Security Payload)** qui fournit l'authentification, l'intégrité *et* la confidentialité via le chiffrement.

Contrairement à la couche réseau IP traditionelle qui n'a pas de connexions, il faut faire une sorte de connexion en IPSec. Cette connexion est appellée **association sécurisée (SA)** et est nuidirectionnelle. Cette connexion identifie le protocole à utiliser (AH ou ESP), l'adresse IP source (et uniquement la source puis ce que c'est unidirectionnel), et un identifiant de la connexion sur 32 bits appellé *Security Parameter Index (SPI)*. Tous les paquets d'une même association sécurisée auront le même SPI.

### Le protocole AH
Comme dit plus tot ce protocole fournit une authentification et un contrôle des données mais sans confidentialité.

Il va insèrer une entête intermédiaire entre l'entête IP et les données du paquet. Et va utiliser un numéro de protocole particulier (51) pour indiquer que AH est utilisé.

L'entête AH contient, un champ "next header" pour indiquer la couche de transport à utiliser, le champ SPI pour identifier la connexion, le numéro de séquence pour éviter les attaques par rejeu (un attanquant qui intercepterait des paquets chiffrer pour les réenvoyer) ainsi qu'un chmap authentification qui assure l'intégrité des données.

### Protocole ESP
Le protocole assure l'intégrité, l'authentification et la confidentialité des données. 

On commence par établir une SA (connexion initiale) pour que la source puisse envoyer des messages de manière sécurisée à la destination.

Les données sont alors encapsulées dans une structure ESP où tout le contenu est chiffré à l'exception de l'entête qui contient les informations pour la destination. Les données sot alors chiffrées avec un algorithme choisis entre les différentes entités.

Encore une fois la structure contient un champ "next header" qui indique la couche de transport à utiliser.

L'IPSec ne peut pas être utilisé sur Internet comme protocole de couche réseau car les routeurs Internet ne le supporte pas. Donc l'IPSec est utilisé soit sur Internet en étant encapsulé dans des paquets IP, soit sur des grands réseaux locaux ou la sécuirté est critique.

## La sécurité des réseaux sans-fil
La manière de sécuriser un réseau sans fil va varier en fonction de l'usage.

Il existe plusieurs normes mais seule le WPA2 et le WPA3 ont une sécurisé suffisante. Le WEP et le WPA sont tous les deux peu sûr.

Pour une **entreprise**, il est préférable de mettre en place un système d'authentification par login et mot de passe différent pour chaque personne. Cela est par exemple le cas d'HELMo avec le réseau *HELMo-Wifi* ou *eduroam*.

![](../static/securite-reseau_20240517103842575.png)

Pour le **public**, il est préférable d'utiliser un portail captif imposant un système d'enregistrement ou d'identification.

Et enfin pour un usage **personnel** il est préférable de faire les choses simple avec un mot de passe partagé entre tous·tes les utilisateur·ice·s. Il est aussi possible de sécuriser d'avantage en cachant le réseau (en empéchant qu'il soit automatiquement détecté), mais cela est peu pratique.

![](../static/securite-reseau_20240517103916838.png)

## Les VPN
Un VPN **Virtual Private Network** permet d'intégrer dans le réseau interne une machine connnectée à l'extérieur du réseau via l'établissement d'un tunnel.

Il existe différentes sortes de VPN, certaines se base sur **PPTP (Point-to-Point Tunneling Protocol)** qui utilise MS-CHAPv2 pour l'authentification, ce système est peu sûr.

Il existe une autre sorte de VPN se basant sur **IPSec** qui est très sûr mais complexe à mettre en place.

Enfin il y a des VPN (et c'est probablement la majorité d'entre eux) qui se base sur **SSL/TLS** par exemple c'est le cas de OpenVPN qui est utilisé à HELMo. Il a l'avantage d'être sécurisé et beaucoup plus simple à mettre en place.

> Pour en savoir plus sur les VPN, vous pouvez regarder [cette vidéo](https://www.youtube.com/watch?v=1mtSNVdC7tM)

Cela pose cependant certains problèmes de sécurités car un VPN intègre une machine externe au réseau interne d'une entreprise. Une entreprise peut donc être attaquée par une machine extérieur et cela augmente donc le risque d'attaques. Il faut mettre en place un système de protection pour limiter le nombre de machines accessibles par le VPN, bien protéger le réseau avec un firewall et garder des logs afin de pouvoir analyser de potentielles attaques.