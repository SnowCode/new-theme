+++
title = "Index de mes synthèses de cours"
date = 2024-05-18T17:02:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "français", "synthèse", "pdf", "index" ]
+++
Cette page constitue un index de mes différentes synthèses de cours afin de pouvoir plus facilement les retrouver <3
<!-- more -->

Je vais essayer de graduellement migrer les synthèses de mon [autre site](https://books.snowcode.ovh) ici, cela va cependant prendre du temps j'ai donc décidé de faire cette page pour vous indiquer où trouver des synthèses pour chaque cours.

Une fois que j'ai fini les synthèses pour un cours, je les combine dans un PDF final afin de pouvoir avoir toutes les synthèses dans un seul fichier à télécharger.

Il est donc préférable d'utiliser les PDF si il est disponible car il représente la version finale des synthèses.

Les synthèses dont je suis le plus fier sont indiquée avec l'émoji "🌟" et sont placé en haut. Les autres sont souvent trop peu complète ou pas assez clair à mon gout.


## Bloc 1
| Cours | Par partie | Combine |
| ----- | --------- | --- | 
| 🌟 Architecture des ordinateurs | [Bookstack](https://books.snowcode.ovh/books/architecture-des-ordinateurs) | Bientot® |
| 🌟 Programmation de base | [Bookstack](https://books.snowcode.ovh/books/java-les-bases-et-la-poo/chapter/les-bases-du-language) | Bientot® |
| 🌟 Bases de données | [Bookstack](https://books.snowcode.ovh/books/bases-de-donnees) | Bientot® |
| 🌟 Français | [Bookstack](https://books.snowcode.ovh/books/francais) | Bientot® |
| 🌟 Mathématiques appliquées à l'informatique | [Bookstack](https://books.snowcode.ovh/books/mathematiques-appliquees-a-linformatique) | Bientot® |
| 🌟 Programmation orientée objet | [Bookstack](https://books.snowcode.ovh/books/java-les-bases-et-la-poo/chapter/la-programmation-orientee-objet) | Bientot® |
| Analyse | [Bookstack](https://books.snowcode.ovh/books/analyse/chapter/bloc-1) | Bientot® |
| E-Business | [Bookstack](https://books.snowcode.ovh/books/e-business-orga/chapter/e-business) | Bientot® |
| Organisation du secteur de l'informatique | [Bookstack](https://books.snowcode.ovh/books/e-business-orga/chapter/organisation-du-secteur-de-linformatique) | Bientot® |
| Développement web | [Bookstack](https://books.snowcode.ovh/books/developpement-web) | Bientot® |
| Anglais | Rien :/ | Rien :/ |

## Bloc 2
| Cours | Par partie | Combine | 
| ----- | --------- | --- | 
| 🌟 Réseau | [#réseau](/tags/reseau) | [PDF](/reseau-synthese.pdf), [Typst](https://snowcode.ovh/reseau-synthese.typ) | 
| 🌟 Systèmes d'exploitations | [Bookstack](https://books.snowcode.ovh/books/systeme-dexploitations) | [PDF et Typst](/syntheses-os-final) |
| 🌟 Développement mobile | Rien :/ | [PDF](/mobile-tuto.pdf), [Typst](/mobile-tuto.typ) |
| Droit et éthique du monde numérique | [#droit](/tags/droit) | [PDF et Typst](/droit-synthese-longue) |
| Mathématiques appliquées à l'informatique2 | [#math](/tags/math) | [PDF](/synthese-math.pdf) et [Typst](/synthese-math.typ) |
| Développement web avancé | [#aspNET](/tags/aspnet) | [PDF](/questions-preparation-devweb.pdf) et [Typst](/questions-preparation-devweb.typ) |
| Structures de données | [Bookstack](https://books.snowcode.ovh/books/structure-de-donnees) | Bientot® |
| Ergonomie | [Bookstack](https://books.snowcode.ovh/books/ergonomie) | Bientot® | 
| Programmation orientée objet (Java) | [Bookstack](https://books.snowcode.ovh/books/java-les-bases-et-la-poo/chapter/programmation-orientee-objet-b2) | Bientot® |
| Conception orientée objet | [Bookstack](https://books.snowcode.ovh/books/conception-orientee-objet) | Bientot® |
| Analyse | [Bookstack](https://books.snowcode.ovh/books/analyse/chapter/bloc-2) | Bientot® |
| Activité intégrative | [Bookstack](https://books.snowcode.ovh/books/java-les-bases-et-la-poo/chapter/programmation-orientee-objet-b2) | Bientot® |
| Programmation orientée objet (C#) | Rien :/ | Rien :/ |
| Anglais | Rien :/ | Rien :/ |
| Digitalistaion et nouvelle économie | Rien :/ | Rien :/ |
| Langage de scripts dynamiques | Rien :/ | Rien :/ |
| Laboratoire pluridisciplinaire (Hackathon) | Rien :/ | Rien :/ |
