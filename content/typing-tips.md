+++
title = "Fast typing tips"
date = 2024-05-25T14:22:00+02:00
draft = true

[taxonomies]
tags = [ "typing", "english", "tips", "research" ]
+++
I've always been interested in fast typing but always frustrated when trying to practice. SO I've decided to make some research on how to improve. I haven't done it yet, but those are my notes.
<!-- more -->

- Learn to touch type if you haven't already (you can use open-source software such as Klavaro for that)
- Homerow touch typing is good but at some point you might want to spice things up and create your own fingermap which can get you really far
- Enjoy it, don't force yourself to do it. You can follow some challenges and be around with a community of other people doing the same thing to motivate you and enjoy it more if you want
- Don't compare yourself to other, it will only harm you and make you stop
- Focus on accuracy, 98% accuracy is a decent score. For doing so you can try to take a little pause before each word
- Stay focus and mindful on your pratice as much as possible
- Keep some kind of rythm, you can go faster on shorter words and take more time on longer ones
- You should analyze your practice to see what is slowing you down
- Remember that if you want to learn a new layout, you might want to start as early as possible because you'll have to re-learn everything again
- Keep track of your progress, highest scores, challenges, etc
- Experiment different things (quotes, random words, random letters, long text, short text, different modes, cometition, etc)
- Train your fingers to go fast

## Challenge ideas
I find the idea of doing challenges to be really great for enjoying the process and thus getting a lot better. So here's some idea of challenges which I found mostly on Monkeytype's Discord server (which I highly recommend you join).

- Complete one (or more) levels of Klavaro's training
- Complete a one hour test (or less/more depending on how hardcore you want to go)
- Type "miodec" one thousand time (or more, or more complex words like "antidisestablishmentarianism")
- Achieve several tests with 98% or more accuracy
- Type some text you like
- Use read ahead mode
- Complete a test without ever using backspace
- Keep a streak (1 day, 1 week, 1 month, 3 months, etc)
- Complete a short text as fast as possible (one word or one sentence)
- Complete a test without looking at the keyboard
- Use something different from homerow technique (try making your own finger map)
- Try typing with a different typing layout
- Type a test with a different keyboard (same layout)
- Don't take it too seriously

## Current score
- Highest WPM : 110WPM
- Longest test : 30 seconds
- Current streak : 7 days (started 23/05/24)
- Highest accuracy : 95%
- Highest on sentence : 133WPM
- Aciheved 60WPM on rust quotes
