+++
title = "Petite pause et futurs projets"
date = 2024-05-17T18:20:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "français", "journal", "short" ]
+++
J'ai pris une petite pause aujourd'hui après l'étude intensive de droit que j'ai fait les derniers jours.

Maintenant vient la partie compliquée, il faut faire la même chose pour [réseau](/tags/reseau) et math. Pour réseau j'ai l'avantage de déjà avoir une synthèse complete, il faut juste maintenant faire le tuto d'intégraiton et m'y entrainer.

Mais pour math j'ai pour le moment absolument rien. Je sens que ça va être un peu chaud pour tout faire.

Par contre j'ai eu une petite idée de projet à faire après tout ça. C'est de faire un bot Discord de lecture RSS (en Rust) qui notifirait les nouveaux posts, et suppporterait RSS, ATOM, OPML. Et éventuellemnet qui pourrait automatiquement trouver les URLS correspondant à certains sites (exemple: tumblr, blogs, YouTube, etc). 

J'avais déjà fait une tentative de ça dans le passé avec un bot Python mais j'ai envie de retenter de le faire mieux cette fois ci.

<!-- more -->
