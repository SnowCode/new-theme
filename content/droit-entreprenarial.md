+++
title = "Comment gérer un projet entrepreneurial"
date = 2024-05-22T19:42:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
Cette partie du cours consiste en une introduction au projet entrepreneurial de l'année prochaine mais également aux entreprises en général.
<!-- more -->

Il est important de d'abord bien comprendre ce qu'est une entreprise. Notament le fait qu'il ne faut pas confondre le terme "entreprise" et "société". Toutes les sociétés sont des entreprises mais toutes les entreprises ne sont pas des sociétés.

La notion d'entreprise a été également agrandir pour inclure toute initiative économique. La notion qui la précédait était celle de commerçant. Il faut donc maintenant utiliser le terme entreprise à la place. Il ne faut **jamais** utiliser le mot "commerçant" dans des réponses de l'examen, il faut la remplacer par le mot "entreprise".

## Personalité juridique
Une personne en droit est le fait d'avoir des droits et des obligations. 

En Belgique, il y a deux types de personnes juridiques. Les **personnes physiques** qui correspond à toute être humain né et viable. Et les **personnes morales** qui sont virtuelle et sont typiquement des groupes de personnes qui agissent ensemble dans un but commun (exemples : fondations, ASBL, institutions publics, mutuelles, SA).

Une personne morale est crée grâce à une loi qui la définit et le respect d'une formalité prescrite par cette loi pour acquérir la personalité juridique.

Une personne morale est un concept basé sur les personnes physiques. Une personne physique en droit est défninie par son nom/prénom, son adresse, sa nationalité et son patrimoine (ensemble de biens/actif et dettes/passif). Une personne morale va avoir un nom unique, une adresse de société (ou siège), une nationalité (là où se situe son siège), et un patrimoine.

## Entreprise
Une **entreprise** est définit comme tout organisme qui remplit une de ces trois conditions :

- Est une personne physique exerçant une activité professionnelle de façon indépendante (exemple : boucher, libraire, comptable, avocat, artisan, etc)
- Est une personne morale (exemple : SA, SRL, SC, ASBL, fondations, etc) **SAUF** les personnes morales de droit public qui ne sont des entreprise QUE si elle proposent des bien et des services sur le marché.
- Est une organisation sans personalité juridique (les sociétés sans personalité juridique) à condition d'avoir comme but la distribution d'argent à ses membres (exemple : les sociétés simple)




