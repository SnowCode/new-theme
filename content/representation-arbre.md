+++
title = "Représentation informatique d'un arbre"
date = 2024-05-27T09:15:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "synthèse", "français", "math" ]
+++
Nous allons ici voir comment représenter un arbre de manière non graphique (et/ou informatique).
<!-- more -->

Une première manière de le représenter est avec un tableau où les lignes représentent les enfants de chaque sommet. Et les indices représentent donc les sommets.

Une deuxième manière est de le représenter avec des parenthèses, où chaque sommet est représenté par son nom et ses enfants en parenthèse.

![Exemple de représentation par parenthèse d'un sommet](/arbre-parenthese.png)

Enfin une dernière manière de le représenter est avec des pointeurs. Où chaque élément a un pointeur vers chacun de ses enfants, et on a ensuite juste un pointeur en plus pour pointer vers la racine.

![Visualisation d'une représentation d'un arbre avec des pointeurs](/arbre-pointeurs.png)

