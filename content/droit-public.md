+++
title = "Quelques notions de droit public"
date = 2024-05-19T15:00:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++

Nous allons ici voir la structure de l'état Belge et le concept de séparations des pouvoirs.

<!-- more -->

L'État est séparé en trois pouvoir. Le pouvoir **législatif** (le parlement) qui s'occupe de rédiger les lois. L'**exécutif** (le gouvernement) met les lois en application et enfin le pouvoir **judiciaire** qui contrôle l'application de la loi.

Chaque pouvoir est donc supposé être indépendant et exercé un contrôle sur les autres, ainsi le gouvernement ne peut donc normalement pas imposer une loi ou définir comment sont gérés les procès.

Le pouvoir **législatif** va adopter les lois et contrôler le pouvoir exécutif. La consition le décrit comme deux chambres, la chambre des représentants et le sénat. La **chambre des représentants** assure l'élaboration de la majorité des lois tandis que le **sénat** s'occupe de l'élaboration des lois les plus importantes tel que la constition. Le sénat est constitué de représantants de chaque communauté et région.

Le législatif contrôle le pouvoir exécutif avec des **questions parlementaires** qui sont des séances publiques où des parlementaires pose des questions au ministre de l'intérieur pour lui demander de s'expliquer sur les actions du gouvernement.

## Pouvoir législatif : La création d'une loi
La création d'une loi se fait en plusieurs étapes.

1. Le parti politique définit des objectifs de loi et choisis un député pour porter la proposition de loi
2. Le débuté dépose une proposition de loi qui sera discuté et voté à la **commission de la chambre**. Cette commission est spécifique au sujet abordé (énonomique, intérieur, etc). 
3. Si la loi est votée à majorité à la commission, elle passe en **séance plénière** avec tous les députés de la chambre. La loi est alors discuttée et votée. 
4. Si la loi est votée à majorité à la séance pleinère, elle est alors signée par le loi et devient officiellement une loi. C'est ce que l'on appelle la *promulgation*.
5. La loi est alors publiée au moniteur belge et est appliquée à tous·te·s.

La loi indique quand elle doit être appliquée. Si elle ne prévois rien, on considère que la loi est appliquée 10 jours après sa publication au moniteur belge.

Il est aussi pour le gouvernement (pouvoir exécutif) de publier un projet de loi qui suivra le même chemin que celui du député à l'exception qu'il passera d'abord par l'avis du conseil d'État qui est un conseil de spécialistes qui va donner son avis sur la proposition.

## Pouvoir exécutif
Le rôle du pouvoir exécutif va diriger et gèrer le pays. Son rôle est donc d'appliquer les lois de manière concrète en leur allouant du budget et en assurant que les sanctions sont bien prises.

Le pouvoir exécutif est composé du gouvernement et du roi. Le gouvernement est théoriquement les conseillés du roi.

## Pouvoir judicaire : Contrôle de l'application de la loi
Le rôle des cours et tribunaux consiste à contrôler l'application de la loi, régler les litiges mais également de contrôler que les décisions du gouvernement sont conforme à la loi.

Il va également contrôler les actions du pouvoir législatif pour s'assurer que les lois ne causes pas de dommages (par exemple une loi rendant obligatoire un vaccin qui s'avèrerai être dangereux).

Il y a beaucoup de cours et de tribunaux différents pour traiter différents sujets (famille, enfance, pénal→assises, entreprise, travail, police, etc) afin de rendre le contrôle de la loi plus rapide et efficace.

## Structure de l'État et des niveaux de pouvoir en Belgique
Le pouvoir en Belgique en plus d'être divisé en trois est également divisé en regions (wallone, bruxelles et flamande) et communautés (flamande, germanophone, wallonie-bruxelles). 

Les **régions** s'occupent de tout ce qui est en lien avec l'économie, l'argiculture, l'urbanisme, transport, énergie, l'emploi, etc. 

Les **commmunautés** s'occupent de tout ce qui est en lien avec la culture, l'enseignemnet, les politiques de santé, l'aide au personnes, les langues. 

Les pouvoirs sont ensuite divisés par sous-régions au niveau provincial, puis au niveau communal.

![Schéma montrant la divisions des pouvoirs en Belgique allant du plus général au plus régional](/pouvoirs-en-belgique.png)

Les institutions **fédérales** tel que le parlement, le roi ou le gouvernement fédéral s'occupe donc des choses communes à tout le pays tel que l'armée, ou des politiques générales.

