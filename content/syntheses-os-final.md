+++
title = "Synthèses du cours de systèmes d'exploitation"
date = 2024-05-19T21:28:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "os", "synthèse", "français", "pdf" ]
+++
Petite pause sur les synthèses de droit pour poster les synthèses du cours de systèmes d'exploitation. 
<!-- more -->

Je n'ai pas simplement mis le lien sur la page d'[index des synthèses](/index-syntheses) par ce que j'ai fait ma synthèse d'OS en plusieurs version. La version longue (qui est globalement un syllabus), la version courte qui est vraiment une synthèse et la version ultra-courte qui est un aide-mémoire.

- Synthèse version longue : [PDF](/os/long-os.pdf), [Typst](/os/long-os.typ)
- Synthèse version courte : [PDF](/os/short-os.pdf), [Typst](/os/short-os.typ)
- Synthèse version ultra courte : [PDF](/os/ultra-short-os.pdf), [Typst](/os/ultra-short-os.typ)

Amusez-vous bien si vous avez ce cours (en vrai je le trouve fun) en deuxième sess ou que vous êtes des étudiant·e·s d'après 2024. Je vous souhaite tout le courage, si vous avez des questions n'hésitez pas à me les poser par Discord.