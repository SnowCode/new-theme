+++
title = "Synthèse/syllabus de droit en PDF"
date = 2024-05-23T09:25:15+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français", "pdf", "short" ]
+++
Voilà, après le marathon de droit. Normalement la synthèse/syllabus est complète.

A savoir que j'ai skip certaines partie qui ont peu de chance d'être à l'examen comme les cas d'inexécution de contrats, le RGPD, la propriété intellectuelle et les détails administratifs sur la création d'une société.

**Disclaimer important** : Les synthèses de droit ont été faites dans l'urgence il est donc possible qu'il y a ai certaines erreurs, principalement niveau orthographe, grammaire et sens de certaines phrases (et peut-être au niveau du contenu lui-même).

- [Synthèse longue en PDF](/droit/long-droit.pdf)
- [Synthèse longue en Typst](/droit/long-droit.typ)
- [Synthèse longue sur ce blog](/tags/droit)

**Update 12h34 :** Voici la synthèse courte de la synthèse syllabus :

- [Synthèse courte en PDF](/droit/short-droit.pdf)
- [Synthèse courte en Typst](/droit/short-droit.typ)
- [Synthèse courte sur ce blog](/droit-synthese-courte)

**Update d'après examen** : Pour les suivant·e·s, je recommande de plus se baser sur la synthèse longue que sur la courte car beaucoup de détails sur les cours et tribunaux et sur l'organisation générales de la belgique n'était pas présent. Je m'excuse également car j'ai dit dans le post précédent que j'allais mettre en ligne des réponses pour les QCM formatifs mais j'ai pas eu le temps et il faut ici que je me concentre sur réseau et math.

<!-- more -->
