+++
title = "WIP : Cryptographie à clé publique"
date = 2024-05-30T18:48:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "synthèse", "français", "math" ]
+++
Cette partie des synthèse va traiter de la cryptographie à clé publique-privée (cryptographie asymétrique ou cryptographie à clé révélée)
<!-- more -->

Le problème principal des cryptosystèmes à clé secrète est la clé car il faut avoir une clé par couple émetteur-récepteur ce qui dans certaines situation peut être très embétant, et il faut préalablement communiquer ces clés de manière sécurisée.

De plus elles ne permettent pas de vérifier l'identité d'une personne ayant écrit le message, ni l'intégrité du message transmis.

Aujourd'hui les cryptosystèmes doivent être confidentiels, authentifiés (on doit pouvoir être sur de qui a écrit un message), intègre (un message ne peut pas être modifié durant la transmission), et non-répudiable (un auteur ne peut pas nier avoir écrit un message).

Les **cryptosystèmes à clé révélée** permettent de résoudre ce problème en ayant deux clés, une clé privée/secrète utilisée pour déchiffrer ou signer des messages et une clé publique/révèlée utilisée pour chiffrer ou vérifier des messages.

## Propriétés des fonctions
Les fonctions de chiffrement et de déchiffrement doivent donc avoir certaines propriétés pour supporter deux clés.

1. Pour tout message, le déchiffrement d'un message chiffré doit retourner l'exact message encodé
2. La connaissance de la clé de chiffrement ne doit pas permettre de calculer le message dans un temps raisonnable
3. Ce point est plus optionel et utilisé pour les signatures : si on applique la fonction de chiffrement sur le résultat de la fonction de déchiffrement d'un message, on retrouve le message initial.

La fonction de chiffrement est ici appellée une **fonction trappe**, c'est une fonction à sens unique qui a besoin d'une autre fonction pour pouvoir inverser son opération. 

Un fonction **permutation trappe** est une fonction qui vérifie les 3 conditions.

## RSA
RSA est probablement le chiffrement à cryptosystème à clé publique le plus connu et utilisé partout sur internet et ailleurs.

Il se base sur le fait qu'il est très facile et rapide de calculer une puissance élevée de nombre mais qu'il est très compliquée sur base du nombre en question de retrouver la base utilisée. 

Vous pouvez trouver plus de détails sur le fonctionnement de RSA vers la fin de ma synthèse d'OS où j'ai également mis diverses ressources, y compris Khan Academy pour pouvoir en apprendre plus sur la cyrptographie asymétrique ou la cryptographie en général.

## Système de cryptographie asymétrique de carte bancaire
Pour vérifier payer avec une carte bancaire, le terminal de paiement n'a pas forcément besoin d'être connecté à Internet pour vérifier les transactions. Cela peut se faire via divers mécanisme mais en voici quelques exemples: 


Le chiffrement asymétrique utilisé par la carte est du RSA 1024 bits.

### Vérification avec les informations d'identification
Tout d'abord le terminal va demander de saisir le code confidentiel, une fois entré, celui-ci est communiqué à la carte. 

La carte va vérifier la validité du code confidentiel, si celui-ci est correct, le terminal va lire la clé publique signée par une autorité de certification. Cette dernière est stoquée sur la carte.

A l'aide de la clé publique de l'autorité de certification, le terminal va vérifier et récupérer la clé publique.

Le terminal va alors récupérer l'empreinte signée de la carte, cette empreinte comporte les informations d'identification de la carte et est signée avec la clé secrète de la carte.

Le terminal va alors vérifier l'empreinte et récupérer les informations d'identifications en utilisant la clé publique de la carte réupérée plus tot.

Si les informations sont bonnes, tout est bon et la carte continue.

### Vérification par challenge
Une autre manière de faire pour vérifier la validité d'une carte est d'envoyer un message aléatoire à signer à la carte. La carte va alors la signer/chiffrer avec sa clé secrète et le terminal va alors vérifier que l'information déchiffrée/vérifiée correspond bien à celle envoyée au départ.

Cela a l'avantage que contrairement aux informations de la carte qui ne change pas, celui-ci est dynamique, ainsi si quelqu'un intercepte les données lors de la transaction, il ne pourra pas simuler une nouvelle transaction.

### Vérification par clé secrète par une banque
Dans certain cas on peut également utiliser la cryptographie symmétrique, notament pour les paiements conséquents ou fait en ligne.

Pour ceux-ci la banque envois un message aléatoire à la carte, la carte va chiffrer ce message avec une clé secrète stockée sur la carte et renvoyer cette information à la banque.

La banque va alors déchiffrer l'information avec la même clé secrète (qui était également connue par la banque) et vérifier que le résultat est bien le même que le message envoyé au départ.

Le chiffrement est ici fait en triple-DES ou AES.

## ElGamal
Pour trouver la clé partagée dans ElGamal, on procède comme suis : 

1. Sur base d'un ordre de groupe cyclique et d'un élément générateur. Imaginons, un ordre `n = 30` et un générateur `g = 3`
2. La clé privée de chaque partie est n'importe quel élément entre 1 et `n`. Cette clé privée sera appellée `Ka`
3. La clé publique correspond à `Ha = g^Ka mod (n + 1)`
4. Répeter le processus pour l'autre partie
5. Trouver la clé secrète partagée en faisant `K = Ha^Ka mod (n + 1)`, soit la clé publique exposant la clé privée dans le groupe cyclique.

Ensuite pour chiffrer un message faisant partie du groupe cyclique, on fait simplement `m' = m * K mod (n + 1)`, soit multiplier le message par la clé partagée dans le groupe cyclique.

Pour la déchiffrer on peut alors faire un inverse modulaire de la clé publique exposant la clé privée. Soit `m = m' * K^-1`.

L'inverse modulaire peut être trouvé en résolvant l'identité de Bachet-Bézout pour `1 = ax + by = Ka + nb` pour y trouver a.

```
         b   a                           
     23  1   0                              
(-5)  4  0   1                           
(-1)  3  1  -5                      
      1  -1  6 <-- Valeur de a = 6
      
=> 4^-1 mod 23 = 6
```



