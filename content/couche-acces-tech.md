+++
title = "Couche d'accès réseau : Différentes technologies"
date = 2024-05-16T08:17:00+02:00
draft = false

[taxonomies]
tags = [ "réseau", "cours", "français", "synthèse" ]
+++

Dans la section précédente, nous avons vu la base de ce qu'est la couche d'accès réseau, ce qu'est une trame et comment les trames sont gérées. Maintenant nous allons voir comment cela s'intègre avec différentes technologies, dont Ethernet.

<!-- more -->

Les réseaux locaux ou **LAN (Local Area Network)** sont des réseaux qui interconnecte tous les équipements d'un campus, d'une entreprise, d'une maison, ou autre établissement local. Certains réseaux comportent un ou plusieurs routerus qui sont connectés à Internet. Il existe plusieurs norme de transfert, Ethernet, Wifi ou Tokenring.

Dans les chapitres précédents on a vu que l'on identifie des machines par adresse IP, cependant comment faire au niveau de la couche d'accès ? Cela se fait avec des **adresses physiques (MAC)**. Les addresses MAC sont unique et sont codées sur 48 bits dont une partie fixée par le vendeur et une autre fixée par l'IEEE.

Par exemple on peut voir sur le site [oui.is](https://oui.is/10c37b69d7bb) que le préfixe `10:c3:7b` correspond à ASUS et les derniers nombres sont un identifiant unique donné par le vendeur.

![](/2024-05-15-15-40-32-image.png)

Ainsi lorsqu'un noeud reçoit des informations, il vérifie que les trames correspondent à son adresse MAC, sinon il les ignores.

## Protocole ARP (IPv4)

Il y a cependant un petit soucis, c'est que sur Internet, on utilise des adresses IP (et le port pour la couche de transport) et non pas des trames pour identifier des services. Donc comment faire pour savoir quel est l'adresse MAC (adresse physique) destination d'une certaine adresse ? Par exemple si une machine 1 souhaite connaître l'adresse physique de `192.168.0.5`.

### Au sein d'un même sous réseau

Tout d'abord la machine va envoyer un message en broadcast (c'est-à-dire à tout le monde) demandant "qui est 192.168.0.5 ?"

![arp-operation-how-arp-works-2-ipcisco](https://ipcisco.com/wp-content/uploads/2018/10/arp-operation-how-arp-works-2-ipcisco.jpg)

Ensuite la machine qui se reconnait va répondre "Je suis 192.168.0.5 et mon adresse physique est `AA:BB:AA:55:55:55`"

![arp-operation-how-arp-works-3](https://ipcisco.com/wp-content/uploads/2018/10/arp-operation-how-arp-works-3.jpg)

Ensuite le PC 1 peut retenir l'information (dans sa table ARP, disponible en faisant `arp -a` sous Linux ou Windows) afin de ne pas avoir à le redemander plus tard. Elle peut ensuite tranquillement communiquer avec le PC 5 en encapsulant son paquet IP dans sa trame en précisant l'adresse MAC du PC 5.

Ce système est appellé **ARP (Address Resolution Protocol)** et est exclusivement utilisé au sein d'un sous-réseau.

### Et lorsqu'il y a plusieurs sous-réseaux ?

Lorsqu'il y a plusieurs sous-réseau avec un routeur entre les deux, le système fonctionne de manière similaire. Si la machine E souhaite communiquer avec la machine R par exemple :

![](/2024-05-15-19-33-31-image.png)

Elle va d'abord demander avec ARP l'adresse physique de R1 sur base de son adresse IP. Ensuite il pourra lui communiquer les données à envoyer à R. 

R1 va alors récupérer l'addresse IP précisée dans le paquet qui est dans sa trame. Ensuite il va demander en broadcast l'adresse physique de R et sur base de cette adresse, il va pouvoir lui transmettre les données qui lui sont destinées.

En somme c'est la même chose qu'avant, sauf qu'on le fait deux fois, une première fois entre l'émetteur et le routeur, et une deuxième fois entre le routeur et la destination.

## Le protocole NDP (IPv6)

L'ARP que nous avons vu plus tot est un protocole qui a été créé pour l'IPv4, pour l'IPv6 quelque chose d'autre a été imaginé. En effet le système d'ARP bien que fonctionnel est assez complexe à maintenir car il faut à chaque fois émettre la demande à tout le monde pour savoir qui correspond. En IPv6 on utilise le **Neighbor Discovery Protocol (NDP)**.

Lorsqu'une machine se connecte sur le réseau elle va envoyer un message de **Router Discovery** en multicast à destination des routeurs. Le routeur va ensuite répondre avec un message de **Router Advertisement** indiquant son adresse physique (qui est également envoyé périodiquement sur le réseau).

Ensuite pour connaître un autre noeud sur le réseau, on envois un message de **Neighbor Discovery** de manière similaire à ARP sauf que c'est du multicast et non du broadcast. La machine va ensuite répondre avec un message de **Neighbor Advertisement**. Il existe encore un message supplémentaire qui est celui du **Redirect** qui est envoyé par un routeur pour indiquer qu'il faut préférer un autre routeur.

Ce système est plus performant qu'ARP car il utilise du multicast et non du broadcast. Cela signifie par exemple que seul les routeurs recevront les messages de *Router Discovery* et que seul les autres machines reçevront les messages de *Neighbor Discovery*

> Pour en savoir plus sur le NDP n'hésitez pas à voir [cette vidéo](https://www.youtube.com/watch?v=A3LFt7CHpgs).

## Ethernet

Ethernet est une technologie qui a plus de 50 ans mais qui est l'une des plus populaire dans le monde nottament grâce à sa grande simplicité et au fait qu'il est bon marché.

Ethernet est un service sans connexion (pas de séquence de connexion à faire pour l'utiliser) et non fiable (des données peuvent être perdues).

Ethernet permet également de faire des réseaux en étoile, il s'agit de connecter les appareils à un switch qui s'occupe alors de retransmettre les informations (chaque appareil a donc une connexion dédiée vers le switch).

![https://www.boardinfinity.com/blog/content/images/2022/11/Your-paragraph-text--64-.jpg](https://www.boardinfinity.com/blog/content/images/2022/11/Your-paragraph-text--64-.jpg)

### La trame Ethernet

Une trame Ethernet est composée principalement de :

- Un **préambule** qui indique des informations relatives à la synchronisation de l'heure
- Un **délimiteur de début de trame** et un **délimiteur de fin de trame** (comme on a vu précédemment pour délimiter une trame)
- L'adresse MAC **source** et **destination**
- Le **type** (indiquant le type de donnée contenue, par exemple : IPv4, IPv6, etc)
- Les **données** (c'est-à-dire le paquet IP)
- Un **code CRC** (ne portant pas sur le préambule) permettant de vérifier l'intégrité de la trame.

### CSMA/CD

Ethernet est une ligne broadcast, cela signifie qu'il faut un système pour contrôler l'accès. Dans le cas d'Ethernet, ce système est le CSMA/CD. Avec ce système avant de transmettre une information on écoute le canal et si une transmission est en cours on attent. 

Une fois que le canal est libre on transmet les informations tout en écoutant pour voir si on détecte des collisions, si oui, on envois un message de colllision et on attends une durée aléatoire. Et au bout d'un certain nombre de tentative, la transmission est abandonnée.

![](/2024-05-15-20-30-30-image.png)

Ce mécanisme peut également utiliser l'**exponential back-off** qui consiste à attendre plus longtemps en fonction du nombre de collisions.

### Topologie

Un réseau Ethernet se fait généralement avec des connexions point à point entre le relai (switch) et les hôtes (les machines) via des connecteurs RJ45.

Le switch peut être intelligent ou *manageable* ce qui signifie que l'on peut le configurer plus finement. Il peut également être compatible SDN (Software Defined Network). 

Un **SDN** est un moyen de découpler l'équipement de la configuration et ainsi avoir une configuration générique qui fonctionne quelque soit l'équipement et permet de faire tout un tas de configuration (équilibrage des charges, optimisation, qualité de service, routage intelligent, etc)

### Multicast Ethernet IPv4

Pour pouvoir transmettre en multicast via Ethernet, il faut pouvoir créer une adresse MAC multicast. Celle-ci se compose des 23 derniers bits de l'adresse IP, un identificateur fixe indiquant qu'il s'agit d'une adresse multicast dans les 24 premiers bits (`01-00-5E`). 

Par exemple pour transformer `224.0.9.45` en adresse MAC multicast, on prends les 23 derniers bits (ce qui donne en hexadécimal `00-09-2D`) et on ajoute l'identifiant devant, ce qui donne alors l'addresse multicast `01:00:5E:00:09:2D`.

![https://image2.slideserve.com/3776048/multicast-mac-address-structure7-l.jpg](https://image2.slideserve.com/3776048/multicast-mac-address-structure7-l.jpg)

Le problème avec l'IPv4 ici c'est que l'on perds les premiers bits de l'IP, ce qui signifie qu'il peut y avoir une même adresse MAC pour plusieurs adresses IP multicast.

### Multicast Ethernet IPv6

Le mécanisme est semblable à celui pour l'IPv4, on copie ici les 32 derniers bits préfixé ensuite par l'identificateur `33-33`. 

Ainsi l'adresse `ff02:0:0:0:1:fff9:2973` devient `33:33:ff:f9:29:73`. 

### Interconnexions (switchs et hub)

Un hub est un relai au niveau physique, c'est un modèle assez rare, risqué et peu pratique car il ne fait simplement que simplement reproduire tous les messages sur tous ses ports sans vérification, ce qui peut donc mener à des collisions.

Aujourd'hui on utilise plus tôt des **switch** qui va analyser une trame et la propager uniquement à la destination. Le switch maintient une table indiquant pour chaque port, les adresses MAC qui y sont accessible.

Tout comme n'importe quel appareil sur le réseau il implémente CSMA/CD pour contrer les collisions. Puis ce qu'il peut faire plus de traitement il peut même être configuré ou interconnecter des technologies différentes en traduisant les différents protocoles.

Un type de configuration particulier qui peut être fait sur un switch sont des **VLAN (Virual Local Area Network)** qui sert à découper le switch pour créer des réseaux différents sans avoir besoin d'acheter plusieurs switch. Deplus dans un VLAN un même port peut être dans plusieurs VLAN simultanément en étiquettant les trames.

<img title="" src="file:///2024-05-15-22-10-51-image.png" alt="" data-align="center">

### Spanning Tree Protocol (STP)

Les switch peuvent être connecté entre eux pour former des réseaux plus grand, cela pose cependant un problème car comment faire pour éviter que des paquets ne bouclent entre plusieurs switch ?

Pour ce faire on met en place un système appellé "**Spanning Tree Protocol (ou STP)**", ou en français "arbre de recouvrement minimum". 

Ce système va utiliser des appareils appeller des **ponts (network bridges)** qui vont permettre de lier plusieurs switch entre eux. Ainsi chaque pont va être identifié par un numéro, le numéro le plus petit sera considéré comme la **racine** et chaque pont va émettre des **BPDU (Bridge Protocol Data Unit)** vers les autres ponts. Chaque pont va donc envoyer des BPDU contenant leur identifiant, l'identifiant de la racine et leur cout pour atteindre la racine.

Par exemple :

1. Lorsque l'on connecte un switch (identifiant 3), il commence par se considérer par la racine, il va donc envoyer un message indiquant "Je suis 3, mon cout pour atteindre la racine est 0 et la racine est 3"
2. Ensuite lorsqu'il reçoit un autre message, par exemple un message disant "Je suis 2, mon cout pour atteindre la racine est 1, la racine est 0", il se rend compte qu'il n'est pas la racine, il va donc se mettre à jour en conséquence et va envoyer "Je suis 3, le cout pour atteindre la racine est 2 et la racine est 0"
3. Le switch va à chaque fois se souvenir de quel port lui donne le meilleur BPDU et va adapter le cout indiquer de son BPDU en conséquence.
4. La racine envois des BPDU quoi qu'il arrive, les autres switch eux n'en envois que pour mettre à jour les informations. Une fois que la racine est le seul émetteur, cela veut dire que la phase de synchronisation est finie
5. Chaque switch va alors définir un port racine (qui est le port qui reçoit les BPDU de la racine), des ports désignés (si les messages sont moins bon, cout supérieur) à ceux envoyés par le switch afin de propager l'info de ceux en bas de l'arbre et des ports bloqués si le message est melilleur que celui du switch actuel (les informations seront donc ignorées).

En bloquant ainsi des ports, on peut donc éviter de créer des boucles dans le réseau. En cas de panne, le pont qui détecte la panne peut alors émettre un BPDU mettant à jour le cout et ainsi recalculer un nouveau chemin.

A la fin d'une séquence STP, on se retrouve donc avec un réseau tel que celui-ci :

![](/2024-05-16-08-48-33-image.png)

Les lettres dans des nuages sont des switch tandis que les numéros dans des carrés sont des ponts. `DP` signifie "Designated Port", `BP` signifie "Blocked Port" et `RP` signifie "Root Port".

## PPP - Point to Point Protocol

Le PPP est un protocole utilisé entre un émetteur et un destinataire, il est donc une sorte d'alternative à Ethernet à utiliser lorsqu'il n'y a que deux noeuds. Par exemple entre un client d'un ISP et l'ISP ou entre deux routeurs de haut niveau. 

C'est nottament très utilisé pour les connection xDSL via PPPoE (PPP over Ethernet) ou PPPoA (PPP over Asynchronous Transfer Mode). 

Une trame PPP se compose d'un **flag** au début et à la fin (pour délimiter la trame via du character stuffing comme vu dans la section précédente), un champ "**address**" et un champ "**control**" qui peuvent être utilisés pour signifier la source et la destination mais puis ce qu'il n'y a que deux noeuds la destination (address) est toujours mise à l'adresse broadcast `11111111` et `00000011` pour l'adressed de contrôle. La trame comprends également le **protocole** (IP, etc), les données contenue dans la trame et un **checksum CRC** pour détecter les erreurs.

Donc par exemple dans une connexion entre un PC chez un particulier et le routeur de l'ISP via une connection xDSL. La connexion du routeur du particulier va être passée par un modem pour pouvoir fonctionner sur la ligne téléphonique. Les données vont arrivée à un point "DSALM" qui est un multiplexeur, c'est-à-dire qu'il va combiner tous les signaux pour les mettres sur une seule ligne qui va ensuite être dirigée soit vers un autre multiplexeur, soit vers le routeur.

Les trames qui sont envoyée depuis le routeur à l'autre routeur, elles sont en PPP et ont donc toute pour destination "l'autre bout de la ligne" ce qui fait qu'il est impossible de se connecter à votre voisin de rue sans passer par le routeur par exemple. C'est ce qui fait qu'une rue ne représente pas un LAN.

Par ailleurs, petit détail, dans le cas du DSL il y a également un filtre qui permet d'éviter les collisions entre les données réseau et les données téléphoniques analogiques.

![](/2024-05-16-10-54-48-image.png)

### Sous-protocoles de PPP

Un premier sous-protocole de PPP est le **LCP (Link-Control Protocol)** qui s'occupe d'établir la liaison et négocier les options, authentifier les utilisateurs si nécessaire, maintenir l'activité et fermer la connexion.

Afin d'authentifier les utilisateur·ice·s on utilise MS-CHAPv2, cela permet ainsi que seul les clients de l'ISP puissent se connecter sur la ligne.

Un autre protocole est le **NCP (Network Control Protocol)** qui est une famille de protocoles qui permettent de configurer les couches supérieures (configuration IP, DNS, etc)
