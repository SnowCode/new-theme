+++
title = "Le routage à grande échelle sur Internet"
date = 2024-05-14T19:32:00+02:00
draft = false

[taxonomies]
tags = [ "réseau", "cours", "français", "synthèse" ]
+++

Internet est un réseau de réseau. Les routeurs permettent de connecter
des réseaux ensemble. Evidemment à une échelle telle qu'internet, chaque
routeur ne peux pas connaitre tous les autres car il y en aurait
beaucoup trop. Surtout qu'en plus il faudrait que chaque routeur sache
l'état de tous les autres routeurs du réseau.

![](/Les_modèles/2024-03-03_13-29-09_screenshot.png)

<!-- more -->

Pour résoudre ce problème, on va utiliser plusieurs niveaux différents
de routeurs. Les routeurs domestiques font tout transiter vers un
routeur plus global du fournisseur d'accès internet.

Ce dernier peut alors communiquer avec les autres routeurs du réseau de
l'opérateur, un routeur de l'opérateur connait donc tous les autres
routeurs de l'opérateurs, et lorsqu'il doit passer sur un réseau
n'appartenant pas à l'opérateur, il doit alors passer par un routeur
global. Les relations entre les routeurs d'un même opérateur (**routage
intra-domaine**) utilisent les protocoles RIP, IGP ou OSFP (par état
lien ou vecteur).

Il y a généralement un routeur global par opérateur (**Autonomous
System**), et communiquent avec les autres routeurs de haut-niveau en
utilisant le protocole BGP (Border Gateway Protocol). Les autres
routeurs de haut niveau ne conaissent pas les routeurs de chaque
fournisseur d'accès, et connaissent uniquement les autres routeurs de
haut niveau. Les relations entre les routeurs d'opérateurs différents se
font exclusivement via le protocole **BGP (Border Gateway Protocol)**,
on parles alors de **routage inter-domaine**. 

Chaque opérateur (Autonomous System) est assigné un numéro unique attribué 
par l'**IANA** ou un de ses délégué (**RIPE** pour l'Europe et l'Asie) et un 
ensemble de sous-réseaux qu'il peut utiliser. Voici par exemple les informations
sur l'AS "BELNET" (AS2611) qui est le FAI d'Helmo. Nous nous trouvons actuellement
dans le sous réseau "193.190.0.0/15". Ces informations viennent du site 
[AsnLookup](https://asnlookup.com/ipv4/193.190.77.129/).

![](/asnlookupdemo.png)

Chaque routeur est donc associé à un ensemble de préfixes d'addresses
(IP d'un réseau + masque), ainsi lorsqu'un paquet arrive à un routeur,
ce dernier regarde d'abord si l'addresse de destination fait partie de
son réseau, si ce n'est pas le cas le routeur regarde de quel réseau il
fait partie et l'envois vers le routeur correspondant au réseau dans sa
table de routage. Si aucun réseau ne correspond, le paquet est perdu.

> Vous pouvez en savoir plus sur le routage en général sur internet en
> regardant [cette vidéo de Computerphile](https://www.youtube.com/watch?v=AkxqkoxErRk).

## Routage intra-domaine

Le **protocole RIP** fonctionne avec des vecteurs de distance qui sont
envoyés toutes les 30 secondes avec un TTL de 1 en multicast (donc ne peut
uniquement passer aux routeurs voisins direct). Un vecteur est également
envoyé lors d'un changement mais toujours avec un délais de 5 secondes
avant la dernier message afin d'éviter de surcharger le réseau
(**flapping**). RIP a deux types de messages : les requêtes (pour demander
les informations de routage) et les réponses (pour envoyer des
informations). Une destination définie dans le protocole RIP est une IP ainsi
que le cout pour y accéder.

Le **protocole OSPF** est une implémentation de l'État-Lien avec
l'algorithme de Dijkstra vu plus tot. Le routeur envois un message `HELLO`
à ses voisins qui échangent ensuite des LSP toutes les 30 minutes et en
cas de modification. Un routeur peut intéroger un voisin en envoyant un
Link-State Request (LSR).

### Comment faire sur de grands réseaux

On peut voir qu'Internet est déjà divisé en plusieurs "niveaux", par
exemple celui de l'inter-domaine, de l'intra-domaine, etc. Mais que
faire lorsque l'intra-domaine est très grand ?

La solution est de diviser le réseau en "zones" et avoir une zone
appellée **backbone** qui va reprendre au minimum un routeur de chaque zone
pour les lier tous. Ainsi les routeurs ont juste besoin de connaitre les
autres routeurs de leur zone, et les routeurs de backbone ont juste
besoin de connaitre les routeurs de leur zone + les autres routeurs
backbone.

Voici un exemple de réseau backbone de haut niveau de 1991 aux États Unis.

![https://upload.wikimedia.org/wikipedia/commons/a/ad/NSFNET-backbone-T1.png](https://upload.wikimedia.org/wikipedia/commons/a/ad/NSFNET-backbone-T1.png)

## Routage inter-domaine

Le routage inter-domaine, comme dit précédemment est le routage qui est fait
entre plusieurs AS. Chaque AS ne connait pas la carte du réseau d'autres AS
pour des raisons politiques.

Le routage inter-domaine utilise le protocole **BGP (Border Gateway Protocol)** qui est un protocole utilisant le port TCP 179 et est basé sur les vecteurs de distance que nous avons vu dans le chapitre précédent.

Avec BGP le problème de **comptage à l'infini** (c'est à dire l'appartition de boucles de paquets, principalement lorsqu'un routeur tombe en panne) est réglé car **BGP liste tous les AS traversés, il est donc possible de détecter des boucles**.

Un routeur BGP communique **uniquement avec ses voisins** (c'est ce que l'on appelle de la connexion point-à-point). Cette connexion avec ses voisin est appellé une **session BGP**. Les routeurs s'échangent ainsi des informatoins concernant des destinations (les préfixes IP) en fonction de leur configuration.

Sur base de leur connaissance du réseau à l'aide des vecteurs de distance, les routeurs établissent quelle zone utiliser pour atteindre chaque destination.

Les routes annoncées aux autres routeurs se font sur base d'une sélection, toutes les routes n'ont pas forcément besoin d'être annoncée comme nous l'avons vu dans la section sur les politiques de peering.

Lorsqu'un routeur BGP reçoit une route, il la prends comme une promesse. Si AS1 partage une route dont la destination se trouve dans l'AS2, il indique donc à tout le monde qu'il promet de contacter l'AS2 pour contacter la/les destination(s) en question.

Il est parfois possible que plusieurs routent mènent à la même destination, BGP ne spécifie pas quoi faire dans ce cas, il est donc du ressort de l'administrateur·ice de s'occuper de configurer le routeur pour résoudre ce problème.

Les routeurs BGP vont ainsi s'envoyer des messages `OPEN` pour établir une connection entre eux, des messages `KEEPALIVE` pour maintenir la connection (qui sont répondu aux messages `OPEN`), des messages `UPDATE` pour annoncer une route/destination ajoutée ou supprimée, et des messages `NOTIFICATION` pour annoncer des problèmes (par exemple une session interrompue).

### Politiques de peering

Il existe deux politiques de routage inter-domaine principales :

![](/Les_modèles/2024-03-03_14-24-23_screenshot.png)

- Le **customer-provider peering** consiste à simplement payer pour être
  connecté à un AS. Par exemple dans le schéma AS3 et AS4 payent pour
  les services d'AS1.

- Le **shared-cost peering** consiste à partager les couts. Par exemple
  AS3 et AS4 peuvent faire un shared-cost peering pour pouvoir payer
  moins cher les services d'AS1 en se répartissant les paquets.

Dans le shared-cost peering les AS vont accepter de se partager les informations, ainsi AS1 et AS2 vont se partager leur routes ainsi que celles de leur client.

Alors que dans une relation customer-provider, l'AS ne partage que ses propres routes et pas celle de son fournisseur.
