+++
title = "J'ai fini Celeste !"
date = 2024-06-24T12:56:00+02:00
draft = false

[taxonomies]
tags = [ "gaming", "français", "journal", "short", "celeste" ]
+++
Je viens de finir le jeu Celeste et j'ai adoré. C'était aussi mon premier jeu de plateforme. Si vous n'y avez jamais joué, jouez-y l'histoire est trop bien et le gameplay aussi.
<!-- more -->

C'est l'histoire de Madeline qui se donne comme défi de gravir le mont Celeste pour se prouver à elle-même qu'elle est capable de surmonter sa dépression et son anxiété. Et le jeu est dur, donc ça sera autant un challenge pour Madeline que pour vous qui allez y jouer.

Le jeu est à la fois frustrant par ce qu'il est dur et à la fois hyper satisfaisant car très bien foutu. Une chose que j'ai beaucoup apprécié c'est le prochain checkpoint est presque toujours visible.

Si le jeu devient trop dur, il existe également l'"Assist Mode" qui permet de changer un peu les règles de jeu par exemple en changeant la vitesse, en donnant une assistance au dash, en activant de l'invincibilité ou en se donnant des dash aériens infinis. 

Ma recommendation est de jouer Celeste en faisant des pauses régulières pour ne pas devenir cinglé à cause de la difficulté et de la frustration. Si après plusieurs sessions de jeu vous êtes toujours frustré et/ou bloqué, activez le mode d'assistance pour quand même prendre du plaisir au jeu.

Prenez soin de vous et amusez vous sur Celeste ^^ 