+++
title = "Qu'est ce que le droit ?"
date = 2024-05-18T19:56:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
Cette synthèse est la synthèse qui reprends la première partie du cours de droit qui consiste à expliquer ce qu'est le droit, ce qu'est l'éthique, les différentes façon de les voir et leur catégories...
<!-- more -->

Ce que l'on apelle généralement "le droit", s'appelle plus précisément le **droit positif**. Le droit positif est un ensemble de règles qui régissent les relations sociales à un moment donné, au regard d'un groupe de personnes donné sur un territoire déterminé. Par exemple la loi belge actuelle à un moment donné (maintenant et jusqu'au prochain changement), à l'égard d'un groupe de personnes donné (les citoyen·ne·e belges), sur un territoire déterminé (la Belgique). 

La règle doit toujours être accompagnée d'une sanction appliquée par la force publique.

On parle de **droit objectif** lorsque l'on parle de la règle en générale, le droit de tout le monde (par exemple "le droit du mariage"), et on parle de **droit subjectif** lorsque l'on parle d'une situation particulière, individuelle ("le droit que *je* me marie").

Les droits objectifs et subjectifs sont donc deux manières de voir les règles faisant partie du droit positif.

Pour faire un recours contre une règle de droit objectif (par exemple contre une nouvelle règle), il faut pouvoir se baser sur une règle de plus haut niveau (par exemple, la constitution).

La **morale** est un ensemble de valeurs qui se décline en principes et comportements. La morale, contrairement au droit est personelle. **L'éthique** est l'étude de la morale.

## Le droit public et le droit privé
Il existe deux sortes de droit. Le droit dit **privé** qui est celui qui régit les rapportes entre les personnes physiques ou morales (entreprises, associations, organisations, etc) et le droit **public** qui régit les rapports avec l'État (c'est-à-dire avec les instituations, l'administration, la police, etc).


Dans le droit **privé** on va retrouver notament, le **droit civil** (régit les relations entre les personnes privées), le **droit des affaires** (régit les rapports entre les entreprises), le **droit social** (régit les rapports employeur-employé), le **droit des assurances**.

Dans le droit **civil**, il y a un coté plaignant et un coté défenseur. C'est par exemple le cas de conflits de famille, de contrats ou de successions. Pour réparer un dommage, cela se fait généralement par l'exécution d'une obligation ou des dommages et interêts versé à la partie adverse.

Parmi le droit public, on va retrouver le **droit consitutionnel** qui définit les libertés fondamentales et la structure de l'État, le **droit administratif** (définit les droits et obligations des administrations), le **droit fiscal** (droit relatif aux impots et à l'organisation financière de l'État) et le **droit pénal** (droit qui réprime les comportements dit comme atteinte à l'ordre de la société elle-même).

Dans le droit **pénal** cependant, il y a un accusé et un représentant de "la société", qui est le ministre public (ou "parquet"). Il s'agit de punir les "infractions", tel que les contraventions, les délits ou les crimes. Pour sanctionner les infractions il s'agit soit d'une ammende versée au trésor public, ou alors d'une peine d'emprisonnement (ou dit "réclusion" pour les crimes).

Il a également une sorte de droit qui tombe un peu dans les deux catégories qui est le **droit judiciaire** qui est une partie du droit qui traite l'organisation des tribunaux et de la procédure judiciaire. Elle est du droit public car elle concerne une institution publique, mais aussi du droit privé car elle concerne aussi les droits des individus et entreprises.

