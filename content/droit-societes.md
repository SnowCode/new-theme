+++
title = "Les sociétés"
date = 2024-05-22T22:47:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
Ceci est la synthèse du dernier chapitre vu en cours de droit. Il s'agit de la description sur la création d'une société et d'une personalité juridique.
<!-- more -->

Comme dit plus tot une entreprise et une société, ce n'est pas la même chose. Toutes les sociétés sont eds entreprises mais pas l'inverse. Il n'est donc pas obligatoire de créer une société pour fonder un projet entrepreneurial.

Il y a plusieurs raison de vouloir constituer une société :

- La société *peut* disposer d'une personnalité juridique
- La société permet de mettre en commun des biens ou des fonds entre plusieurs personnes
- La société peut également garantir une responsabilité limitée en raison du fait que si on crée une personalité juridique, la société aura son patrimoine propre et sera séparé du patrimoine des personnes qui la constitue
- Le fait que les biens et les fonds reste dans la société permet de rendre la société plus pérenne car elle peut vivre plus longtemps que les personnes qui sont dedans
- La société bénéficie d'un statut fiscal différent d'une personne physique. Cependant l'avantage de ce statut diminue avec les nombreuses réformes successives.
- La société offre un statut social différent que celui d'une personne physique. C'est-à-dire que le calcul des charges sociales (la différence entre le salaire brut et le salaire net), est différent pour une société que pour une personne physique indépendante.

Une **société** est constituée par un acte juridique par lequel une ou plusieurs personnes (associés) font un apport de patrimoine pour exercer une ou plusieurs activités déterminées. Mais un des buts de la société est de redistribuer l'argent à ses associés.

Il existe plusieurs formes de sociétés en fonction de certains critères (personnalité juridique, responsabilité, nombre d'associés, etc).

Une société va être divisées en "parts" qui seront reversée aux différents associés. 

Il existe 4 formes légales de sociétés, les sociétés **simple**, les sociétés à **responsabilité limitée (SRL)**, les sociétés **coopératives (SC)** et les sociétés **anonymes (SA)**.

## Société responsabilité limitée
Dans une SRL, le patrimoine est le patrimoine *de la société*. Les créanciers ne peuvent donc pas aller demander des dettes à n'importe quel associé de la société.

Le risque financier des associés est donc limité à leurs apports dans la sociétés. Et une fois leurs  apports libérés, leurs patrimoines privés ne sont plus exposés aux risques financiers de la société, à l'exception des fondateurs, des gérants et **administrateurs** (pour faute de gestion) et des titulaires d'une profession libérale (liée aux actes de leur profession). Mais très souvent les assoicés vont mettre leur patrimoine personnel en jeu par eux même (par exemple pour rassurer une banque par rapport à la capacité de paiement d'un prêt).

Les **administrateurs** sont des personnes nommées par les associés et chargé de la gestoin de la société (conseil d'administration). 

## Société à responsabilité illimitée
Dans une société à responsabilité illimitée, tout le monde est responsable de tout. Les créanciers peuvnet donc demander le paiement d'une dette à n'importe quel associé de la société.

C'est donc une forme bien moins avantageuse que celle de la SRL. 

## Choisir la forme de société: 
![Diagramme de choix de type de société](/choix-structure-societe.png)

## Personnes morales autre que les sociétés
L'**association** est une convention entre plusieurs personnes (membres) qui poursuit un but désintéressé dans le cadre d'une ou plusieurs activités déterminées. Elle n'a donc pas pour but (contrairement aux sociétés) de redistribuer l'argent à ses membres.

Une **fondation** est une personne morale dépourvue de membres qui est constituée par des fondateurs. Le patrimoine est utilisé pour un but désintéressé dans le cadre d'une ou plusieurs activités déterminées. Elle ne peut donc pas redistribuer l'argent à ses fondateurs.
