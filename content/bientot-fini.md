+++
title = "Bientot fini !"
date = 2024-06-03T11:19:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "français", "journal", "short" ]
+++
Les examens sont bientot fini pour moi (enfin), il ne me manque plus que quelques défenses de projet et un examen de Javascript. Mon plan est de partager des exemples de solutions commentées pour les badge de JS, et de faire une synthèse. 

Je compte aussi faire une synthèse pour la théorie à connaître en développement web avancé. 
<!-- more -->