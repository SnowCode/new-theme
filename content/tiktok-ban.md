+++
title = "Firefox collects data, AI is a disaster, France bans TikTok (sort of): Linux & Open Source News"
date = 2024-05-18T18:56:00+02:00
draft = false

[taxonomies]
tags = [ "vidéo", "links", "recommendations", "français", "english" ]
+++
[Une plus tôt bonne vidéo](https://www.youtube.com/watch?v=iinwIYt1IzM) qui parle nottament du ban de TikTok par le gouvernement français en Nouvelle-Calédonie. La Quadrature du Net va également attaquer en justice le gouvernement français pour cette décision.

<!-- more -->

<iframe width="560" height="315" src="https://yewtu.be/embed/iinwIYt1IzM" title="Invidious video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Vous pouvez également en savoir plus sur le ban de TikTok, vous pouvez également aller voir l'article de la Quadrature du Net (une association qui a pour but de signaler les dérives autoritaires en lien avec Internet en françe et également un peu en Belgique). 

- [La Quadrature du Net attaque en justice le blocage de TikTok en Nouvelle-Calédonie](https://www.laquadrature.net/2024/05/17/la-quadrature-du-net-attaque-en-justice-le-blocage-de-tiktok-en-nouvelle-caledonie/)

