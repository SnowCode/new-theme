+++
title = "Droit, les entreprises"
date = 2024-05-14T10:44:00+02:00
draft = true

[taxonomies]
tags = [ "cours", "droit", "français", "synthèse", "français" ]
+++


<!-- more -->

Il existe des sociétés sans personalité juridique. 

## Catégories d'entreprises
Toute personne physique qui exerce une activité professionnelle à titre indépendant est une entreprise de **première catégorie**.

Toute personne morale (ASBL, société, fondation, mutuelle) à l'exception des personnes morales de droit publique si elle ne proposent pas de biens ou services sur le marché, est une entreprise de **deuxième catégorie**.

Une organisation sans personalité juridique ayant pour but d'octroyer un avantage patrimonial à ses membres (par exemple pour des buts de successions, ou pour partager certains frais). Il est cependant très fort déconseillé d'utiliser cette catégorie si il y a des risques ou pour une activité entière car tous les membres sont responsables en cas de problème. Tout ce qui est une organisation telle que celle-ci est une entreprise de **troisième catégorie**.  

Une entreprise représente toute entité tombant dans l'une de ces catégories.

## Contrats de collaborations
Il existe différents types de contrats de collaboration :

Le **contrat d'entreprise** est le contrat par lequel une personne s'engage à effectuer d'une manière indépendante au profit d'un co-contractant sans le représenter.

## Les sociétés
Une société a pour objectif de fournir des bénéfices à ses associés.

### Formes légales des sociétés
La **société simple (à responsabilité illimitée)** 


