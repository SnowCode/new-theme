+++
title = "How to get a free domain name"
date = 2021-03-12T21:31:29+01:00
[taxonomies]
tags = ["english", "self-hosted"]
+++
Domain names are required for a lot of actions on your server, like the SSL certificate generation for example.

But you may want to get one for free to avoid the need of paying anything for your server (in the case when you self-host).<!-- more -->

In this tutorial, I'm gonna show you how to get a domain name for free using the popular DynDNS solution called No-IP.

## Step 1 : Create an account
![signup](/signup.png)

You can now go on [this website](https://www.noip.com/sign-up) and sign yourself up. By the way, make sure you're not using Tor or a VPN when accessing this website, it uses your public IP to autoamtically setup the DNS options.

You may have to re-login after signing up in some cases.

## Step 2 : Open your modem ports
![login](/login.png)

Go on `192.168.1.1`, or what ever your modem's portal is and login with the credidentials on the back of your modem.

## Step 3 : Create some new port maps
![portmapping](/portmapping.png)

## Conclusion
Your new domain name is now usable. I highly recommend you to follow my [server security guide](/secure) to know how to setup your firewall, https, nginx, ssh and fail2ban configuration.
