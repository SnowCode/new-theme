+++
title = "Chaya Grossberg - What is a Warm Line and What Should I Expect When I Call One?"
date = 2022-06-06T11:46:58+02:00
[taxonomies]
tags = [ "english", "mental_health", "anarchy", "praxis" ]
+++
This is a text from the website "Mad in America" and has been written by Chaya Grossberg.<!-- more -->

For the first time ever, I called a warm line-and got through. At first I felt unsure of what to do – do I just start talking about my problems with an anonymous stranger on the other end of the line?

She asked me my name and I said I don’t want to give my real name. “You can call me Felicity.” I told her it was my first time calling a warm line, except one other time when I didn’t get through.

A warm line is an alternative to a crisis line that is run by “peers,” generally those who have had their own experiences of trauma that they are willing to speak of and acknowledge. Unlike a crisis line, a warm line operator is unlikely to call the police or have someone locked up if they talk about suicidal or self-harming thoughts or behaviors. Most warm line operators have been through extreme challenges themselves and are there primarily to listen. A warm line has the purpose of reducing hospitalization and forced treatment, being a cost effective and non-intrusive, voluntary intervention.

For a number of reasons warm lines have been on my radar lately and I needed to see what it was like to call one. My call was out of curiosity as much as need, but I certainly have as many problems to talk about as anyone so I started in about some of them. She inserted an understanding “Yeah” in now and then, but mostly she just let me talk. After I talked for about 5-10 minutes, I paused. She commented on her own preferences, how she would feel about a certain thing I’d told her, if she were in my situation. I shared some of my mixed feelings and thanked her. “It felt good to get all of that off my chest.” She quickly asked me if she could ask me a few survey questions. I asked her for a minute to take a breath before answering questions (would love to have that moment incorporated as an option in all support settings). After my breath I asked her name again. She told me. I answered her questions about whether I would have used more expensive services if this warm line had not been available. No, not this time. We hung up. That was it.

I felt like I’d accomplished something huge just by trying this thing I’d never done before and realizing it was in one way no big deal and in another way a large gift. There was palpable relief from a lot of stress buildup in recent hours, days, months, years, decades. It had been a very long time since someone had just listened to me, and asked for nothing in return. My nervous system relaxed.

It reminded me of how I have felt after therapy (which I haven’t done in 10 years) but it was different. Simpler. Basically just listening; no diagnosis, no expert, no degree necessary on their part, no appointment to keep (one of my resistances to therapy-I hate having things like that on my calendar and often end up canceling) and total anonymity. No pressure from either person to find a solution, solve a problem, prove them self as skillful, or even ever interact again. This isn’t to say a warm line is “better” than therapy or friends or anything else, but there are moments where it can be.

My first thoughts upon hanging up the phone were, “Wow, I want to do that everyday” and “I’d like to call that line again very soon. That really helped me. In less than 20 minutes.”

Despite the obvious value of warm lines many people, even those heavily involved with therapy and other “mental health” services, have no idea what a warm line is or that they even exist.

I trusted the David Romprey Warm Line more than others for my first experience because I know some of the people that run it. All of their operators go through the Intentional Peer Support training, which I took last year and loved the content of. So there was a trust factor that probably made the difference between me calling and not calling. It takes courage to pick up the phone and call someone to share your feelings, fears, worries, even if you are anonymous. Anything could go wrong – at least that’s how my traumatized mind works. The person could say the wrong thing or somehow not get it and I could be left feeling even more alone and frustrated, so it is important to know the philosophy behind a particular warm line and how the warm line operators are trained.

The good news is they are all “peer” run, which means you are unlikely to be labeled or treated/seen as “other.” Still, anything is possible when calling an anonymous person and it’s taken me some time to warm up to the idea of calling a warm line. I’m excited to do it again though, as both a research project of sorts – based on my curiosity – as well as a desperate need to be simply listened to that is often near impossible to come by in our world. It is important that those of us who are making ourselves available for others are reaching out and being heard for our own difficulties in a non-judgmental setting.

Please share with me any experiences you have had with warm lines and why you have or haven’t tried them.

> List of warm lines by state: www.warmline.org
