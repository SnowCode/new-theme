+++
title = "I found some cool new blogs today and some cool ideas"
date = 2024-05-16T21:40:00+02:00
draft = false

[taxonomies]
tags = [ "links", "recommendations", "blogs", "english", "français", "blogging", "about" ]
+++

Some recommendations of cool blogs I recently started to follow, in English and in French... 

I also discovered a concept I find pretty cool called "digital gardens" on those as well as weekly notes.

<!-- more -->

## English
- [Kottke](https://kottke.org)
- [Cassidoo Williams](https://cassidoo.co/blog/)
- [Maggie Appleton](https://maggieappleton.com)

## Français
- [Framablog](https://framablog.org/)
- [Alexis Métaireau](https://blog.notmyidea.org/)
- [Benjamin Bouvier](https://blog.benj.me)
- [Julie Brillet](https://juliebrillet.fr)
- [Fabien Rica](https://frica.github.io)

## Cool ideas I found on those blogs
There's a bunch of things I find cool about those blogs, and some that I already agreed with but affirmed my ideas on it.

- The concept of [digital garden](https://maggieappleton.com/garden-history), that means a website that evolve where ideas are not just
ordered by their creation date but by their context. Websites where ideas can be half-finished and modified later without a problem, where 
it's OK things are not perfect. This is a cool personal blog, not a magazine or an encyclopedia.

![A graph showing that doing complete, long and complex posts take more energy than making updates along the way](https://res.cloudinary.com/dxj9qr5gj/image/upload/c_scale,f_auto,q_auto:best,w_1000/v1621773885/maggieappleton.com/notes/garden-history/gardentime_rrsecv.png)

- The concept of weekly notes, all the french blogs I cited earlier do it (except Framablog because it's not a personal blog), the idea is simply every week to write a summary of the week. For instance what was hard, what was good, what was discovered, etc. 
And many of them finish with links to weekly notes of other blogs, so there is a log more blogs I could have cited there but I'll let you explore them on your own.
- The concept of double-links for posts. This idea was cited by Maggie Appleton, the idea is that, instead of links going one-way (you reference someone else, and that someone doesn't know you linked them), links would be going two-way (you reference someone and you can find everyone that referenced that someone).
- The concept of sharing other blogs in general, that's why I'm doing it right now :)
