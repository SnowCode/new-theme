+++
title = "Galshi - Spring"
date = 2024-05-18T17:02:00+02:00
draft = false

[taxonomies]
tags = [ "music", "links", "recommendations", "français" ]
+++
[Spring](https://youtube.com/watch?v=rVYxuizlkXs) est une musique plus tot cool de Galshi (autre fois Galshi Revolution). Ca me rappelle la vibe de leur ancienne musiques en électroswing que je trouve assez sympa. 

Vous pouvez aussi aller voir les ancienne dans le même genre comme [Chroma](https://youtube.com/watch?v=LGGDOxYLYZI) qui doit être l'une des première que j'ai entendu et que j'aime toujours autant.

Dans le même genre de style électroswing il y a [Caravan Palace](https://www.youtube.com/@CaravanPalace) qui est l'un de mes groupes de musique préféré.

<!-- more -->