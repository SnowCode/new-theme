+++
title = "Sources du droit positif belge"
date = 2024-05-19T15:10:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
Cette partie du cours vois les différents type de droit et les sources du droit belge (notament du droit international tel que le RGPD ou des contrats).
<!-- more -->

Il y a plusieurs sources pour le droit civil tel que le droit **international**, la loi au sens large, la coutume, la doctrine et la **jurisprudence**.

Le droit **international public** est l'ensemble de règles de droit qui régissent les rapports entre les États et les organisations internationales. Elle s'occupe nottament de définir ce qui constitute un État et sa souvraineté, les compétences de tout État, les relations internationales et la coopératoin entre les États (droit international de la mer, droit humains, etc).

Le droit **international privé** régit les relations de droit privé présentant un élément externe. Par exemple un contrat dont les parties sont étalies dans des États différents, ou une faillite d'un groupe d'entreprise international, ou encore la sucesssions des biens à l'étranger. Ce droit détermine donc la juridiction compétente pour trancher le litige (conflit de juridiction) et la loi à appliquer pour trancher le litige (conflit de loi).

## Droit européen
La différence principale entre le droit internationale et le droit **européen** est que l'union européenne est plus intégrée et plus uniformisée. Il se compose principalement de traités, règlements et directives.

Les **traités européens** définissent les objectifs poursuivis par l'UE, les règles de fonctionnement des institutions, les processus décisionnels, les relations entre l'UE et ses états membres, etc. Ces tratiés sont négociés et adopés par l'ensemble des états membres avant d'être ratifier par leurs parlements.

Les **règlements** sont des actes législatifs qui s'appliquent dès leur entrée en vigueur de manière automatique et uniforme dans tous les pays de l'UE sans devoir être transposés dans la législation nationale. Ils sont donc obligatoires dans tous les pays de l'UE. C'est notament le cas de la loi sur la protection des données privées (RGPD).

Les **directives** instaurent une obligation de résultats mais sont plus permissifs pour les États que les règlements. Chaque pays doit adopter des mesures dans leur législation nationale afin d'arriver aux objectifs de la directive. Les autorités nationales doivent ainsi communiquer ces mesures à la commission européenne.

Les directives doivent être tranposé dans les législations nationales dans un délai fixé lors de leur adoption, qui est généralement 2 ans. Si un pays ne transpose pas la directive la Commission européenne peut engager une procédure d'infraction.

## Organisation de la loi
Généralement le droit international prime sur le droit national. A l'exception de la constitution qui prime tout de même quoi qu'il arrive. Il arrive donc sur des règles qui impactent les citoyens qu'il fasse valoir son droit international ou européen au delà de celui national car il peut être plus protecteur et prime sur le droit national.

Généralement la **constitution** est un texte fondateur relativement simple et qui ne change pas beaucoup. Cependant en Belgique, la constitution est un texte plus complexe qui est régulièrement réformée.

Dans ce qui dérive de la constitution, on trouve la **loi fédérale** qui consiste de lois adoptées par la chambre de représentats et/ou le sénat.

Les **décrets** sont du même principes que les lois fédérales mais au niveaux des régions et des communautés seulement.

Les **ordonnances** sont très similaires au décrets mais pour la région de Bruxelles-capitale avec la différence que les ordonnances ont moins de pouvoir que les décrets.

Les décrets et les loi fédérales sont au même niveau d'importance. Si il y a un conflit entre une loi et un décret, ce sera la règle la plus récente qui prime. Il arrive donc parfois que des décrets modifient des lois fédérales et priment donc sur la loi fédérale.

Enfin il y a les actes du pouvoir exécutif qui vont mettre en place les lois du pouvoir législatif (loi fédérale, décrets et ordonannces), ces actes du pouvoir exécutifs sont appellés **arrêtés d'exécutions**. Les arrétés d'exécutions sont donc beaucoup plus concrets que les lois. Les lois précise les concepts généraux tandis que les arrétés précise comment les lois sont appliquées dans les détails.

Les arrétés d'exécutions au niveau fédéral sont appellé **arrétés royaux**, au niveau des communautés ou des régions on parle d'**arrété du gouvernement**.

## Loi vs contrat
L'article 1.3C du code civil dit : 

> Art. 1.3. Acte juridique
> 
> L'acte juridique est la manifestation de volonté par laquelle une ou plusieurs personnes ont l'intention de faire naître des effets de droit.
> 
> Sauf disposition légale contraire, toute personne, physique ou morale, possède la capacité de jouissance et la capacité d'exercice.
> 
> On ne peut déroger à l'ordre public ni aux règles impératives.
> 
> Est d'ordre public la règle de droit qui touche aux intérêts essentiels de l'Etat ou de la collectivité, ou qui fixe, dans le droit privé, les bases juridiques sur lesquelles repose la société, telles que l'ordre économique, moral, social ou environnemental.
> 
> Est impérative la règle de droit édictée pour la protection d'une partie réputée plus faible par la loi.

Le code civil dit donc ici que l'on peut déroger (par exemple avec un contrat) à toute loi qui n'est pas d'ordre public ou de règles impératives.

Il y a donc trois types de lois :

- Les lois d'ordre public
- Les lois impératives
- Les lois supplétives

Les lois d'**ordre public** qui définit un ordre moral, politique, économique et social de la société ou des principes essentiels de fonctionnement de l'État. C'est par exemple la legislation sur les faillites, la législation fiscale, le code pénal, la sécurité sociale, etc.

On ne peut pas déroger aux dispositions qui relèvent de l'ordre public. On ne peux donc pas autoriser quelqu'un à tuer quelqu'un d'autre dans un contrat par exemple, car cela reviendrait à déroger à une loi d'ordre public (le code pénal).

Il y ensuite également les lois **impératives** qui ont pour but de protéger une des parties contre des abus de l'autre en octroyant d'office certains droits ou en interdisant à l'autre partie de lui imposer certaines obligations.

C'est par exemple le cas des lois sur la protection du consommateur en ajoutant une interdiction formelle de clauses dites abusives dans les contrats. Il est donc impossible de déroger une loi impérative ou d'ordre public par un contrat.

Ces règles peuvent souvent être reconnus par l'utilisation d'une terminologie particulière tel que ***nonobstant toutes dispositions contraires*** ou ***tout acte juridique contraire sera déclaré nul***. 

Enfin toute loi non impérative et qui n'est pas d'ordre public est appellée **loi supplétive**. Si les parties n'ont rien prévu de différents dans leur contrat, les règles supplétives s'appliqueront. Ce sont en quelques sorte des règles "par défault" qui peuvent être changée explicitement dans un contrat si besoin.

Ces règles utilisent souvent des terminologies tel que ***sauf disposition contraire*** ou ***sauf si les statuts ou le contrat en dispose autrement*** ou ***sauf si les parIes en ont convenu autrement***.

## La coutume
La coutume est une source de règles de droit non-écrite qui est une pratique généralisée par la population sur un certain territoire. 

Il s'agit donc de toutes les règles de droit qui se dégagent des faits sans intervention d'un législateur. La loi est cependant toujours dominante sur la coutume.

Par exemple, si deux personnes sont fiancées (qui ont donc promis de se marier) et que lors de la planification d'un marriage, une des deux personnes paye tous les frais, et que l'autre rétracte sa volonté de se marier et annule le marriage et refuse de payer les frais (traiteur, salle, etc).

Si la personne qui a payé tous ces frais demande un procès, l'autre peut se voir demander de payer des dommages et interets car la coutume de la fiancaille est tellement répandue que le juge considèrera qu'il y a eu un engagement et que l'autre personne a rompu l'engagement.

Et ce même si le concept de fiancaille n'existe nulle part dans la loi.

## La jurisprudence
La jurisprudence est l'ensemble des décisions judiciaires rendues par les cours et tribunaux.

En Belgique, chaque juge est indépendant et peut trancher un litige sans tenir compte de la jurisprudence à l'exception d'un cas où un litige se trouve en double cassation (il a été retrouvé en cours de cassation deux fois).

Le juge ne peut cependant pas faire n'importe quoi non plus, il y a tout de même une influence morale qui pousse à s'inspirer de la jurisprudence.

Le juge va donc décider au cas par cas ce qu'est le comportement d'une personne "prudente et raisonnable" dans une situation donnée.

Le juge peut également définir certaines situations comme des **abus de droit**, c'est-à-dire que quelqu'un utilise son droit uniquement dans le but de nuire et dépasse donc les limites de l'exercice normal de ce droit.

## La doctrine
La doctrine est l'ensemble de travaux académiques spécialisés en droit qui peuvent par exemple expliquer des façons de traiter des litiges, etc.

La doctrine n'a pas force de droit mais elle va avoir une influence car les juges vont s'en inspirer pour leur pratique. La doctrine est particulièrement pratique lorsque des juges sont en difficulté par rapport à l'application d'une loi, la documentation de la doctrine peut alors les aider à y voir plus clair.

La doctrine n'a donc aucun caractère obligatoire, c'est simplement une aide pour le juge.