+++
title = "How to connect a midi keyboard to Musescore on Linux"
date = 2022-06-11T23:03:45+02:00
[taxonomies]
tags = [ "linux" ]
+++
I've tried to connect my Yamaha YDP-162B to Musescore but I've ran into a few trouble so I've tried several methods and figured out the best method is to use PipeWire.

So first, let's install pipewire, I'm on Arch Linux so I'm gonna install it using pacman. 

```bash
sudo pacman -Sy pipewire-pulse pipewire-jack pipewire-alsa qpwgraph wireplumber
```

Then reboot your system for the changes to apply. 


Once you've done that, open the app "qpwgraph" and plug your MIDI keyboard into your laptop. You should see a new element appearing. In my case it's a purple box called "24:Arius".

Then, open Musescore, then in Edition > Preferences > I/O and choose "ALSA Audio". Then hit "Apply" and "OK". You might also need to restart Musescore for the changes to apply.

Now in "qpwgraph" you should see something like this in it (by moving the elements):

![qpwgraph](/qpwgraph.png)

And now if you press a note on the keyboard you should hear the sound coming from Musescore. You can press "P" in musescore to see the keyboard. And you can try to add notes.

