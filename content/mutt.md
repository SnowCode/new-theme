+++
title = "Confugration de mutt"
date = 2021-02-05T23:23:45+01:00
[taxonomies]
tags = ["français", "linux"]
+++
Salut ! Voici un petit tutoriel de confgiuration de mutt sous Linux. Je tient à préciser que j'utilise Gmail (malheureusement).<!-- more -->

## Configuration de Gmail
Par défault gmail bloque l'accès aux applications tierces non authorisée. Mais il y a deux moyens de bypasser cette restriction.

### Moyen recommendé par Google
Cette méthode demande d'activer la double authentification de son compte, mais est beaucoup plus sécurisée que l'autre méthode.

Tout d'abord on va aller dans le [portail de sécurité de son compte Google](https://myaccount.google.com/security). Et ensuite activer la double authentification.

Une fois celle-ci activée, on peut revenir sur la page d'avant et aller dans `Mots de passse des applications`. 

Une fois sur cette page, on peut ajouer une nouvelle application de type `Autre` et lui donner un nom (`mutt` par exemple). Un mot de passe aléatoire sera créé et pourra être donné dans la configuration de `mutt`. Il est donc important de le noter quelque pars en attendant, car il ne sera plus visible après.

### Moyen peu sécurisé
Cette méthode n'est pas recommendée mais dans un cas où la double authentificaion de Google ne peut pas fonctionner, on peut toujours utiliser cette méthode.

Tout d'abord il faut aller sur le [portail de sécurité de son compte Google](https://myaccount.google.com/security). Ensuite authoriser les applications moins sécurisées.

## Installation
Pour commencer, on va installer `mutt`, ça parrait être la base :p

```bash
sudo apt install mutt
```

Maintenant, on peut créer un nouveau fichier de configuration.

```bash
nano ~/.muttrc
```

Et ensuite y copier le code suivant:

```bash
set from = "<préfixe de l'email>@gmail.com"
set realname = "<nom>"

# Imap settings
set imap_user = "<préfixe de l'email>@gmail.com"
set imap_pass = "<mot de passe de l'application>"

# Smtp settings
set smtp_url = "smtps://<préfixe de l'email>@smtp.gmail.com"
set smtp_pass = "<mot de passe de l'application>"

# Remote gmail folders
set folder = "imaps://imap.gmail.com/"
set spoolfile = "+INBOX"
set postponed = "+[Gmail]/Drafts"
set record = "+[Gmail]/Sent Mail"
set trash = "+[Gmail]/Trash"
```

Une fois cela fait, mutt devrait être configurer correctement et être disponible en lançant: 

```bash
mutt
```

A partir de cet écran, les raccourcis les plus utiles sont montrés au dessus et on peut accéder à la liste de tous les raccourcis en utilisant la touche `?`.

## Conclusion
Je ne sais pas pourquoi je fais une conclusion, mais pourquoi pas :p Autant ajouter un peu du texte à ce billet très court. Même si Gmail n'est pas idéal, configurer mutt pour gmail (et inversément) est très simple et permet de gérer ses mails sans quitter le comfort de son terminal :)

Bonne nuit !

