+++
title = "Synthèse complète de réseau"
date = 2024-05-17T17:53:06+02:00
draft = false

[taxonomies]
tags = [ "cours", "réseau", "synthèse", "français", "pdf" ]
+++

Ca a pris moins de temps que je n'ai pensé mais j'ai fini la synthèse de réseau. Je compte juste encore ajouter un tuto de "comment faire un exercice d'intégration", donc je vais encore encore faire quelques publications et mettre à jour le document. 

<!-- more -->

- [La synthèse en format PDF](/reseau-synthese.pdf)
- [La synthèse en format Typst](/reseau-synthese.typ)
- [La synthèse sur ce blog](/tags/reseau)

