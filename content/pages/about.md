+++
title = "About"
path = "about"
template = "page.html"
+++

<!-- more -->

Hello! Bienvenu sur mon blog, ici vous trouverez [des synthèses de mes cours](/index-syntheses), des [partage de trucs que j'ai trouvé sympa](/tags/links), des [tuto minecraft](/tags/minecraft), et plein d'autres trucs random.
<!-- more -->

<!--
Hey ! Welcome to my blog, I'm a software developpement student in Belgium. I love stuff related to open source, free software, caring, anarchy, and anything going around those topics.

I've decided to re-start my blog to share my thoughts, summaries, notes, links, images and whatever I want without pressure. I was inspired by various content and ideas you can find in [here](/tags/blogs) and [here](/tags/blogging).

I also decided to migrate some French school summaries here at [#cours](/tags/cours). You can also find them combined in PDF format at [#pdf](/tag/pdf).

---

Hey ! Bonjour et bienvenu sur mon blog ! Je suis un·e étudiant·e en développement d'applications. J'aime tout ce qui est lié à l'open-source, le logiciel libre, la culture libre, l'anarchie, le partage, etc.

J'ai décidé de recommencer mon blog pour partager mes délires, mes résumés de cours, mes notes, des liens, des recommandations, des conseils, etc. J'ai été inspiré par divers autres blogs, vous pouvez en trouver plus [ici](/tags/blogs) et [ici](/tags/blogging).

J'ai aussi décidé de migrer certains résumsés de cours ici, vous pouvez les trouver directement sur ce blog dans [la catégorie "cours"](/tags/cours). Et vous pouvez les trouver combiner ensemble dans des documents PDF dans la [catégorie PDF](/tags/pdf).
-->