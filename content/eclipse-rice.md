+++
title = "How to make Eclipse suck less"
date = 2024-05-20T15:28:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "rice", "english", "tuto" ]
+++
I have been forced to use the [Eclipse IDE](https://eclipseide.org/) for some of my Java courses. The thing is that I personally find Eclipse to have a pretty horrible interface.
<!-- more -->

![A screenshot of the default look of Eclipse IDE](/default-eclipse-look.png)

So today I'm going to show you a few tricks to make Eclipse suck a little bit less, in particular by changing the theme and the fonts.

At the end of this tutorial, your Eclipse will look something like this (with your prefferred colorscheme and font if you don't like Catppuccin mocha or Geist Mono)

![A screenshot of the final rice of Eclipse IDE with catppuccin mocha and Geist Mono](/eclipse-rice-catppuccin-mocha.png)

## Installing a new theme
First go to the Eclipse Marketplace by going in "Help" then Eclipse Marketplace.

Once the window opens, search for "Darkest Dark" and install the plugin. There it will ask you to confirm you accept the ToS and shit. It might also ask you to configure the theme, you can answer anything there since we're going to change it afterward anyway.

Once that's done, you can then create a new file named something like "your-theme.xml" and paste the following content in it :

```xml
<?xml version="1.0" encoding="utf-8"?>
<colorTheme id="99999" name="Catppuccin Theme" modified="2024-05-19 10:45:00" author="Your Name">
    <searchResultIndication color="#51576d" /> <!-- surface1 -->
    <filteredSearchResultIndication color="#51576d" /> <!-- surface1 -->
    <occurrenceIndication color="#51576d" underline="true" /> <!-- surface1 -->
    <writeOccurrenceIndication color="#51576d" underline="true" /> <!-- surface1 -->
    <findScope color="#51576d" /> <!-- surface1 -->
    <deletionIndication color="#f28fad" /> <!-- peach -->
    <sourceHoverBackground color="#89b4fa" /> <!-- sky -->
    <singleLineComment color="#6c7086" italic="false" /> <!-- overlay2 -->
    <multiLineComment color="#6c7086" italic="false" /> <!-- overlay2 -->
    <commentTaskTag color="#89b4fa" italic="false" underline="true" strikethrough="false" /> <!-- sky -->
    <javadoc color="#6c7086" italic="false" /> <!-- overlay2 -->
    <javadocLink color="#89b4fa" italic="false" underline="true" strikethrough="false" /> <!-- sky -->
    <javadocTag color="#89b4fa" italic="false" /> <!-- sky -->
    <javadocKeyword color="#89b4fa" italic="false" /> <!-- sky -->
    <class color="#89b4fa" bold="false" underline="false" strikethrough="false" /> <!-- sky -->
    <interface color="#89b4fa" /> <!-- sky -->
    <method color="#a6e3a1" /> <!-- green -->
    <methodDeclaration color="#a6e3a1" /> <!-- green -->
    <bracket color="#cdd6f4" /> <!-- text -->
    <number color="#f5c2e7" /> <!-- pink -->
    <string color="#f9e2af" /> <!-- yellow -->
    <operator color="#89b4fa" /> <!-- sky -->
    <keyword color="#f5c2e7" bold="false" /> <!-- pink -->
    <annotation color="#a6e3a1" /> <!-- green -->
    <staticMethod color="#a6e3a1" /> <!-- green -->
    <localVariable color="#cdd6f4" /> <!-- text -->
    <localVariableDeclaration color="#f5c2e7" /> <!-- pink -->
    <field color="#cdd6f4" /> <!-- text -->
    <staticField color="#cdd6f4" /> <!-- text -->
    <staticFinalField color="#cba6f7" /> <!-- mauve -->
    <deprecatedMember color="#f28fad" underline="false" strikethrough="true" /> <!-- peach -->
    <enum color="#cba6f7" /> <!-- mauve -->
    <inheritedMethod color="#a6e3a1" /> <!-- green -->
    <abstractMethod color="#a6e3a1" /> <!-- green -->
    <parameterVariable color="#f5c2e7" /> <!-- pink -->
    <typeArgument color="#89b4fa" /> <!-- sky -->
    <typeParameter color="#f28fad" /> <!-- peach -->
    <constant color="#cba6f7" /> <!-- mauve -->
    <background color="#1e1e2e" /> <!-- base -->
    <currentLine color="#313244" /> <!-- surface1 -->
    <foreground color="#cdd6f4" /> <!-- text -->
    <lineNumber color="#51576d" /> <!-- surface1 -->
    <selectionBackground color="#b4befe" /> <!-- lavender -->
    <selectionForeground color="#cdd6f4" /> <!-- text -->
</colorTheme>
```

This is a custom catppuccin mocha theme I made for Eclipse based on the [Dracula Theme for Eclipse](https://draculatheme.com/eclipse).

Feel free to change the colors to make the theme fit your preferred colorscheme.

Once you've got your theme file, you can then import it by going into Window > Preferences > DevStyle > Color Themes.

![A screenshot of the devstyle settings panel](/eclipse-devstyle-settings.png)

In those settings, click on "Import" and select your custom theme. Then tick the **Theme Background** box and select "**Dark Custom**" as workbench theme.

Under the workbench theme, you'll find some H, S and L boxes. Those are for setting the background color of the IDE using HSL format. If you have an idea of precise color, you can look online how to convert it to HSL. 

For catppuccin I used values 240, 21 and 15 which corresponds to the "Base" (background) color of catppuccin mocha.

Finally, you can hit "**Apply and Close**", eventually restart the IDE and the theme should be applied.

## Change the font
Now the second thing that really sucks is the font, the default font is "monospace" which is very boring. So select a font you enjoy (you can find some at [programmingfonts.org](https://programmingfonts.org) if you want). Make sure the font is installed in your system.

Then go to the Eclipse's preferences (Window > Preferences > General > Appearance > Colors and Fonts > Basic > Text Font). Then click "Edit" and choose the font you want.

Finally, hit "**Apply and Close**" and the font should be all set.

## Better keyboard shortcuts
Finally, if you are used to do your stuff with VIM keybindings, you can also choose to install the VIM plugin from the Eclipse Marketplace. 

It will enable a little VIM icon on the top bar on which you can click to activate or deactivate VIM mode. 

It's also quite useful to get handy with the [default Eclipse keyboard shortcuts](https://www.baeldung.com/eclipse-shortcuts) too.
