+++
title = "La validité d'un contrat et les vices de consentements"
date = 2024-05-22T16:35:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
Nous allons voir ici quels sont les conditions pour qu'un contrat soit valide et quel recours juridique faire si ce n'est pas le cas...
<!-- more -->

Un contrat est considéré comme valide par la loi dans le cas où il respecte 4 conditions :

Chaque partie du contrat doit être en **capacité** de contracter. Cela signifie donc que les personnes de moins de 18 ans ne peuvent par exemple pas contracer. Certaines personnes sous protection judiciaire (où une tierce personne gère les biens par exemple) peuvent également avoir l'interdiction légale de conclure certains contrats. 

Le contrat doit avoir un **objet déterminable et licite** (doit porter sur des choses légales) et ne doivent pas créer des déséquilibres manifests entre les droits et obligations de chaque partie.

La **cause du contrat** (sa raison d'être) doit être **licite**. C'est-à-dire que le contrat ne peut pas servir à des fins illégales.

Enfin, il doit y avoir le **consentement libre et éclairé** des deux parties du contrat (donc pas d'usage de violence, d'abus de position dominante, fraude, erreur, etc). Dans le cas contraire, on dit qu'il y a **vice de consentement**.

## Types de vice de consentement
Un premier type de vice est l'**erreur** sur une qualité déterminante de la chose-objet du contrat autre que la valeur de la chose (le prix) ou la personne (sauf si le contrat a été conclu en considération de celle-ci).

Pour qu'une erreur conduise à la nullité du contrat, il faut également qu'elle soit **excusable** (c'est-à-dire qu'elle aurait pu être commise par une personne raisonnable et prudente et qui n'est pas pris à la légère) et **commune** (la qualité sur laquelle porte l'erreur doit être subtantielle par les deux parties).

Un deuxième type de vice est la **lésion** qui est une disproportion manifest entre les prestations des parties dès la conclusion du contrat. La lésion n'est *pas* un vice de consentement SAUF dans les cas où la loi le prévois (par exemple pour le partage d'une succession ou si la personne lésée est un·e mineur·e émancipé·e).

Un troisième type de vice est l'**abus de circonstances** qui est un déséquilibre manifeste entre les prestationss des deux parties (comme la lésion) où il y a une position de faiblesse de l'autre partie et où cette faiblesse a eu une influence déterminante sur le consentement de la partie victime de l'abus.

A partir de là si sans abus, il n'y aurait eu aucun contrat, le contrat est considéré nul. Si sans abus, le contrat aurait été accepté mais à d'autres conditions, l'autre partie doit reverser des dommages et intérêts.

Un quatrième type de vice est la **violence**, c'est-à-dire des menaces quant à l'intégrité physique ou morale ou aux biens d'une personne. Cela consiste à impressionner la personne afin qu'elle se sente obliger d'accepter le contrat. Comme pour l'abus de circonstance, si sans violence, le contrat n'aurait pas été accepté, alors le contrat est considéré nul. Et si sans violence, le contrat aurait été accepté à d'autres conditions, alors l'autre partie doit reverser des dommages et intérêts.

Enfin un ciquième et dernier type de vice est le **dol**, c'est-à-dire des manoeuvres intentionelles dans le but de tromper, de mentir ou de ne pas dire des informations crutiales sur le contrat. Cette tromperie peut venir du cocontractant ou d'un tiers complice du cocontractant. Pareil que pour les deux derniers, dépendant de l'absence de ces tromperies, le contrat peut être considéré nul ou le cocontractant peut être obligé de payer des dommage et intérêts. 

Un vice est dit **incident** si sans ce dernier, le contrat aurait tout de même été accepté mais sous des conditions différentes. Dans les cas de vices incidents l'autre partie doit reverser des dommages et intérêts.

Un vice est dit **principal** si sans ce dernier, le contrat n'aurait pas été accepté. Dans quel cas le contrat est considéré comme nul.

