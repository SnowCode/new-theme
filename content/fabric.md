+++
title = "How to install fabric loader for Minecraft on Linux."
date = 2021-07-02
[taxonomies]
tags = ["english", "minecraft"]
+++

Fabric is a lightweight mod loader, much lighter and leaner to forge. More and more mods are moving from Forge to Fabric for those reasons. So let's see how to install it:<!-- more -->

> Note: I personally had a problem with the fabric not showing up in the launcher versions. This guide will solve that issue.

## Step 1: Download and install fabric
Installing fabric is pretty easy, simply go on the [download page](https://fabricmc.net/use)

Then click on Universal/.JAR if you're on Linux or macOS, or click on Windows/.EXE if you're on Windows. Note you will need to have Java installed!

Then simply execute the launcher (either by running a command or double clicking on it)

```
java -jar ~/Downloads/fabric-installer-*.jar
```

Once that's done, click on Install (don't forget to check the Minecraft version is the one you want).

## Step 2: Adding the Fabric API and the mods
Many mods require you to install the Fabric API mod. Simply download the Fabric API mod corresponding to your Minecraft version [here](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files)

Move that file to ~/.minecraft/mods/ (on linux and macOS) or to  %AppData%/.minecraft/mods/ (on Windows).

Then, add the fabric mod(s) for that version into that same directory. I personaly like [Xaeros minimap](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap/files) and [ReplayMod](https://www.replaymod.com/download/).

## Step 3: Adding a new launcher entry
The "normal" usage is as such, open the launcher, check there is a "fabric" profile corresponding to the version you installed (if there's none, make sure the "Modded" box is ticked).

If you have it, great! But in my case, I already had fabric 1.16 installed, and I couldn't find fabric 1.17 in the version list. If you have that same problem, add the following to ~/.minecraft/launcher_profiles.json

```
"Fabric <minecraft version" : {
      "created" : "2021-07-02T13:06:18.358Z",
      "gameDir" : "<path to .minecraft, like /home/snowcode/.minecraft>",
      "icon" : "Furnace",
      "lastUsed" : "2021-07-02T16:22:50.562Z",
      "lastVersionId" : "fabric-loader-<fabric loader version>-<corresponding minecraft version>",
      "name" : "fabric",
      "type" : "custom"
},
```

For instance:

```
"Fabric 1.17" : {
      "created" : "2021-07-02T13:06:18.358Z",
      "gameDir" : "/home/snowcode/.minecraft/",
      "icon" : "Furnace",
      "lastUsed" : "2021-07-02T16:22:50.562Z",
      "lastVersionId" : "fabric-loader-0.11.6-1.17",
      "name" : "fabric",
      "type" : "custom"
},
```

Once you did that, restart the launcher. You should now have a new profile showing up, simply hit the PLAY button next to it. Have fun!

## Conclusion
Here is my second tutorial on this glog. I hope it was useful to you!

Good night!
