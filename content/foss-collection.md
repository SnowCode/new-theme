+++
title = "Ma liste de logiciels libre préféré"
date = 2024-05-14T10:00:00+02:00
draft = true

[taxonomies]
tags = [ "open source", "logiciel libre", "français", "liste", "recommandations" ]
+++

A work in progress collection of cool af open source projects.

<!-- more -->

- Firefox (en particulier Hardened Firefox)
- Krita
- Vencord
- Hyprland
- NixOS
- uBlock Origin
- Lazygit
- Helix
- Emacs
- WinDirStat / ncdu
- KeepassXC et KeepassDX
- SponsorBlock
- Tridactyl
- Language Tools
- TOR Browser
- Tails OS
- VLC
- Bookstack
- OsmAnd~ ou Organic Maps
- NewPipe
- Etar
- Calculator++
- Librera FD
- Rain
- Aurora Store
- Wikipedia
- Syncthing
- Signal
- Lichess
- Lawnchair
- KDE Connect
- Feeder
- Fennec
- F-Droid & NeoDroid
- PollyMC
- moodle-dl
- yt-dlp
- Hyprland
- Alacritty
- Briar
- Transmissions et Torrents CSV
- Anna's Archive
- Termux
- Street Complete
- Scrcpy
- Mindustry
- Typst 

