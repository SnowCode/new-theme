+++
title = "The rush is coming"
date = 2024-05-14T18:48:00+02:00
draft = false

[taxonomies]
tags = [ "course", "diary", "study", "exams", "english", "short" ]
+++

Urhh, this sucks. I have a very tight exam session this time. With only two weeks to study for three exams (in which included 1 which will take easily one to two week :/)

I'm sick of this, I feel like I haven't had a time off in ages at this point. This makes me procrastinate so much, I would just like to be with my friends (and lover) and chill without having to worry about what's coming next.

My lover recently said that she felt like she hasn't seen me in the past weeks even though we live together. That makes me sad, but it should be over soon I hope.

<!-- more -->
