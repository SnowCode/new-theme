+++
title = "Fuckkkk..."
date = 2024-05-18T15:36:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "français", "journal", "short" ]
+++
J'ai passé un certain temps à essyaer de me mettre à faire les synthèses de réseau, pour les exercices d'intégration.

Mais je viens de me rendre compte que je suis nulle part en droit, en math et en digitalisation. La dead line de digitalisation est dans 3 jours, celle de droit dans 4, celle de math et réseau dans environ une semaine.

Je ne sais vraiment pas comment faire pour m'en sortir surtout que j'arrive pas à rester concentrer sur les cours et que ma tête est comme "embrouillée".

<!-- more -->

**Edit 19/05/24** : Du coup je me rends compte qu'étudier droit est encore assez rapide mais qu'il faut que je me mette vraiment à étudier digitalisation. Du coup je vais charbonner sur digitalisation demain et après demain, puis me mettre sur droit. Puis une fois que l'exam de droit sera passé je finirais d'abord réseau et puis math.

Si tout se passe comme je le prévois j'aurais ensuite une semaine pour étudier JavaScript et il restera comme ça uniquement les petits oraux de défense de projets auquel il y a pas vraiment besoin de préparation.