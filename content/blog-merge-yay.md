+++
title = "I merged my old blog!"
date = 2024-05-18T14:35:00+02:00

[taxonomies]
tags = [ "about", "english", "blogging", "short" ]
+++

I've just found my old blog from 2 years ago so I decided to merge its content and theme with this new one. I'll try to clean things up later since there is still a large part of things that are outdated (such as links to fediverse and shit).

Also maybe I'll change the theme or something like that. But I find it really cool to be able to finally start blogging more frequently !

I'll try to write an about page soon and stop procrastinating on the insane amount of study I still have to do

<!-- more -->