+++
title = "Contrats de collaboration"
date = 2024-05-22T20:16:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
Nous allons voir ici comme la collaboration entre entreprise peut être structurée en utilisant des contrats de collaboration.
<!-- more -->

Les **contrats de collaboration** permettent de collaborer en restant des personalités juridiques distincte. On ne va donc pas créer une personne morale, mais simplement collaborer entre entreprises.

Il existe différents types de contrats. Tel que le contrat sur la propriété intellectuelle, contrat d'entreprise, l'agence commerciale, la concession de vente ou le contrat de franchise. Nous n'allons pas ici voir en détail le contrat sur la propriété intellectuelle car elle est dédiée à un chapitre à part.

## Contrat d'entreprise
Les **contrats d'entreprise** sont des contrats par lequel une personne (physique ou morale) s'engage à effectuer de manière indépendante un travail au profit d'un cocontractant sans représenter celui-ci, moyennement un engagement réciproque de lui payer un certain prix. Par exemple, si la ville de Verviers engage une entreprise de travaux, cela se fera grâce à un contrat d'entreprise.

Le contrat d'entreprise est donc un contrat de location de service. Si vous décidez de vous lancer en tant qu'indépendant·e, le contrat d'entreprise est donc le contrat de base car c'est celui que vous allez signer avec vos client·e·s et vos collaborateur·ice·s.

## Le contrat d'agence commerciale
Le **contrat d'agence commerciale** est lorsque l'on confie à un tiers la négociation de contrats. En somme cela consiste à sous-traitre la vente de biens ou services. 

Dans un contrat d'agence commerciale, l'agent commercial est chargé de façon permanente et moyennant rémunération par l'autre partie, sans être soumis à son autorité, la négociation et la conclusion d'affaires au nom du commettant.

Cela permet ainsi de ne pas avoir à employer des vendeurs en interne. Et contrairement au contrat d'entreprise, les contrats sont ici fait en votre nom plus tot qu'au nom du sous-traitant.

Ce type de contrat est particulièrement protégé par des **lois impératives** (pour la fin de contrat) qui protège les agents commerciaux qui sont vu comme la partie faible du contrat. Cela empèche donc d'engager un agent pour ammener des clients, et une fois ces clients fidéliser, virer l'agent et continuer de profiter des clients car cela metterait en danger l'agent.

La loi dit que le contrat d'agence commerciale peut être soit d'une durée **déterminée** ou **indéterminée**. 

Si le contrat est de durée **déterminée**, alors le contrat devra impérativement être exécuté **jusqu'a son terme**. Si le commetteur souhaite mettre fin au contrat pour des raisons autre qu'un très bon motif (faute grave par exemple), il devra reverser une indemnité couvrant en principe le manque à gagner de l'agent. Cependant le contrat peut définir en avance des justes motifs permettant de mettre fin au contrat prématurément.

Si le contrat est de durée **indéterminée**, alors le contrat pourra prendre fin moyennant la notification de l'autre partie d'un préavis raisonable d'au moins un mois par année commencée et pouvant aller jusqu'a maximum 6 mois. Ainsi pour renvoyer une personne engagée il y a 3 ans, il faudra la prévenir 3 mois à l'avance par exemple.

L'agent commercial est ici rémunéré via les commissions, c'est-à-dire qu'il prend une partie des ventes qu'il fait. Soit via un pourcentage (exemple, 10% de chaque vente) ou un montant fixe (10€ par vente) ou une combinaison des deux. Ce montant est donc précisé dans le contrat.

La commission peut être payée au plus tard le dernier jour du mois qui suit le trimestre au cours duquel le tiers et le commettant ont effectué leurs obligations contractuelles. Par exemple, si un contrat a été signé en janvier alors la date limite pour payer la commission correspondante est le 30 avril. 

Afin de pouvoir calculer la rémunération de l'agent il faudra que l'agent ai un relevé des commissions dues. L'agent peut ainsi exiger que ces informations lui soit fournie, notament un extrait des livres comtables afin de bien pouvoir vérifier que le montant des commissions qui lui sont due est correct.

Il y a encore une protection par loi impérative supplémentaire pour l'agent qui est qu'il peut demander une **indemnité d'éviction** à la fin de son contrat si il prouve qu'il a apporté un réel apport à l'entreprise. Le montant de cette indemnité peut monter jusqu'a un an de rémunératio. Ce montant est donc calculer sur base de l'apport de l'agent à l'entreprise.

## Contrat de concession de vente
Dans un **contrat de concession de vente** un concessionnaire va vendre les biens. A la différence de l'agent commercial, le concessionnaire vend les biens **en son propre nom**.

Cela signifie que le concessionnaire est donc bien mieux payé (car il vend comme il veut) mais c'est plus risqué car il doit gérer lui-même ses stock par exemple.

Le contrat de concession de vente peut accorder une **exclusivité** concessionnaire(s) indiquant que le fournisseur ne vendra les marchandise qu'a une certaine liste de concessionaire et à personne d'autres (offrant ainsi un fort avantage aux concessionaires). En contrepartie de cela, le concessionaire peut se voir demander de réaliser un certain chiffre d'affaires, de commander une certaine quantité de produit au concédant (fournisseur) ou de constament avoir un certain stock de marchandise disponible.

Un contrat peut également accorder une **exclusivité** inverse qui dit que le concessionaire ne peut acheter des marchandise que depuis le concédant et ne peut donc pas vendre des produits similaires venant d'autres fournisseurs.

Si le contrat exige une exclusivité pour le concessionnaire (qu'il ne peut acheter que chez le concédant), alors le contrat sera un contrat à durée indéterminée. Et lorsque lorsque le concédant souhaite terminer le contrat, il devra donner un préavis raisonable ou une indemnité du manque à gagner. En plus de cela le concessionnaire peut demander une indemnité complémentaire de la même manière que pour l'agent commercial par rapport à la plus value de clientelle pour le concédant.

## Contrat de franchise
Contrairement aux autres types de partenariat, le contrat de franchise n'est pas réglé ou défini par la loi. 

La **franchise** est un système de commercialisation de produits et/ou services basé sur la collaboration étroite et continue entre des entreprises. Il impose l'obligation d'exploiter une entreprise en conformiter avec le concept du franchiseur. 

Le franchisé va donc devoir utiliser l'enseigne, la marque, le savoir-faire et les règles du franchiseur en échange d'une contribution financière directe ou indirecte de sa part.

C'est par exemple le cas de beaucoup de supermarchés tel que Carrefour par exemple. Le groupe Carrefour est le franchiseur et chaque supermarché individuel est une franchise de Carrefour. Ce sont donc des entreprises indépendentes du groupe Carrefour mais qui sont en collaboration avec le groupe et respectent donc les règles de Carrefour en échange de leur contribution financière et de la disposition de leur produits.
