+++
title = "Le routage multicast"
date = 2024-05-15T09:49:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "réseau", "synthèse", "français" ]
+++

Ceci est la fin du chapitre du cours de réseau sur Internet. On va ici parler du fonctionnement du multicast IPv4 ainsi que du multicast sur Internet.

<!-- more -->

L'idée du multicast est simplement de partager une information avec un groupe de receveurs et non pas une seule. Cette idée n'a pas et n'est pas toujours présentes dans tout les réseaux.

Il existe donc plusieurs méthodes pour implémenter le multicast

## Types de multicast

Le **one-to-all multicast** est un type de multicast qui fonctionne lorsque le réseau ne supporte pas le multicast. Il consiste simplement à utiliser une connexion séparée pour chaque destinataire. Il y a donc autant de connexions que de destinataires. 

L'efficacité est mauvaise mais son implémentation est simple.

L'**application-level multicast** fonctionne également lorsque le réseau ne supporte pas nativement le mutlicast et consiste à transmettre l'information à un destinataire qui va ensuite se charger de la partager au suivants. 

L'efficacité est meilleur que le one-to-all mais est compliquée à mettre en place car elle demande une certaine restructuration de l'application.

L'**explicit multicast** fonctionne uniquement si la couche réseau le permet. Il consiste à n'envoyer qu'un seul paquet qui sera répliqué par le routeur et acheminé à tous les destinataires. 

Cette solution est la plus efficace, son seul désavantage est que le réseau doit supporter nativement le multicast pour fonctionner.

## Différentier les traffics multicast

Pour pouvoir différentier plusieurs émetteurs et également faire en sorte qu'uniquement les machines intéressées par le multicast ne le reçoit, on pourrait penser que l'on peut "simplement" indiquer toutes les adresses des destinataires dans le paquet. Cependant cela n'est pas pratique, surtout si il y a beaucoup de destinataire. D'autant plus que la source ne connait pas forcément les adresses de ses destinataires.

La solution utilisée est donc plus tot d'utiliser un identifiant appellée une **adresse IP multicast** 

## Le protocole IGMP

![](/2024-05-15-10-26-30-image.png)

La solution de l'explicit multicast pose cependant certain challenges, comme celui de choisir l'adresse, démarrer ou arrêter le groupe, ajouter une nouvelle machine, vérifier qui peut rejoindre le groupe, etc.

Pour cela on utilise le protocole **IGMPv2 (Internet Group Management Protocol)** qui s'opère entre l'ordinateur et le routeur qui lui est directement connecté. Il offre un moyen pour un hôte d'avertir le routeur qu'il souhaite recevoir un trafic multicast donné et rejoindre le groupe.

Il est donc uniquement utilisé pour signaler au routeur qu'un hote et ou n'est plus intéressé (AKA qu'il est ou non dans le groupe multicast).

Il utilise trois types de messages, les **membership query** envoyés par un routeur à tous les hôtes pour déterminer quels groupes doivent être joints; les **membership report** envoyés par un hôte pour indiquer qu'il s'abonne à un groupe multicast et le **membership leave** qui est envoyé par un hôte pour indiquer qu'il quitte le groupe multicast.

Un message IGMP est composé d'un type, de l'addresse multicast de groupe (qui va de 224.0.0.0 à 239.255.255.255), un champ **Max Response Time** qui indique le temps d'attente maximum pour la réponse et le champ **checksum** qui pemret de contrôler la cohérence des informations.

Dans ce modèle, n'importe qui peut s'abonner à un groupe multicast, personne ne connait donc l'ensemble des destinataire et tous les membres peuvent envoyer des informations multicast. Le contrôle d'accès et des informations est donc réalisé par l'application elle-même.

## Routage multicast

Si il n'y a qu'un seul routeur, les choses sont simple car le routeur n'a qu'a garder en mémoire la liste des membres du groupe multicast et de retrasférer les informations de cette façon. Cependant les choses deviennent plus complexe lorsqu'il y a plusieurs routeurs.

Une première solution serait que tous les routeurs possèdent une carte complète du réseau (**arbre partagé**) et donc alors de trouver un moyen de relier tous les routeurs intéressés avec un cout minimum. Cela est cependant assez complexe à mettre en place et il n'y a pas de solution dans un temps fini.

Une deuxième solution pourrait être de définir un point de rendez vous. Ce système consiste simplement à choisir un routeur central (par exemple **E** dans le schéma) et à rediriger les messages join vers ce routeur.

![](/2024-05-15-10-39-46-image.png)

Enfin une dernière solution serait d'avoir **un arbre pour chaque membre** en utilisant un système **RPF (Reverse Path Forwarding)**, c'est à dire que lorsqu'un routeur reçoit un paquet multicast qu'il n'a jamais vu, il le retransmet à tout le monde. Le routeur garde en mémoire qui lui a envoyé ce premier paquet et ne retransmettera que les paquets de ce routeur là, les autres seront ignoré. Le routeur peut également indiquer à ses voisins qu'il n'est pas intéressé par le traffic multicast (message **prune**).

L'avantage est ici que le routeur n'a pas besoin d'avoir une carte complète du réseau, simplement de retransmettre le paquet aux suivants.

## Routage multicast dans Internet

Comme vu précédemment le multicast explicite nécessite que le réseau supporte le multicast, c'est-à-dire que les routeurs soit des routeurs multicast. Donc si un routeur multicast est entouré par des routeurs non-multicast il ne servira à rien. Il est cependant possible d'établir des tunnels (un peu comme pour l'IPv6) qui permettent de transmettre du multicast via de l'unicast.

Différents algorithmes sont prévu pour faire du routage multicast sur Internet, le premier est le **DVMRP (Distance Vector Multicast Routing Protocol)** qui est un algorithme qui implémente un arbre pour chaque membre avec RPF et le pruning et peut également déterminer lorsqu'un traffic ne doit plus lui être propagé en fonction des routeurs qui dépendent de lui.

Un autre algorithme est le **PIM (Protocol Independent Multicast)** qui a deux modes différents, le mode "dense" qui est utilisé lorsqu'il y a beaucoup de routeurs multicast à un endroit, et le mode "sparse" lorsqu'il y a peu de routeurs multicast à un endroit.

Le mode "dense" utilise du RPF (donc un arbre pour chaque membre) alors que mode "sparse" utilisent un système de points de rendez-vous.
