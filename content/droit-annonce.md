+++
title = "Annonce : synthèses de droit finie pour demain"
date = 2024-05-22T23:09:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français", "short" ]
+++
J'ai passé la journée à faire mes synthèse là et il commence à faire tard. Mais puis ce que l'exam c'est quand même demain, j'annonce d'avance ce que je compte faire demain matin :

- Finir la dernière [synthèse sur les sociétés](/droit-societes)
- Combiner toutes les synthèses ensemble dans un PDF
- Faire une synthèse condensée de tout
- Donner des exemples expliqués pour les questions QCM et questions ouvertes
<!-- more -->

N'hésitez pas à me poser des questions sur Discord si vous en avez, ça m'aide aussi à améliorer mes synthèses et à mieux comprendre la matière.

De plus je mentionne quand même que je laisse tomber les parties sur la propriété intellectuelle et le RGPD par ce qu'elles n'ont pas été vue au cours. Je laisse aussi tomber la partie sur inexécutions des contrats dont je n'ai vu aucune question passer (ni dans les QCM, ni dans les cas pratiques) et qui est trop longue à faire.

A demain, et bonne chance (et bon courage) tout le monde !