+++
title = "Installing Plume (fediverse blog platform)"
date = 2021-10-21T08:48:54+02:00
[taxonomies]
tags = ["english", "self-hosted"]
+++
Hi, this is just a quick tutorial to install Plume on Debian.<!-- more -->

## Step 1: Installing dependencies
First let's install the dependencies for Plume. Actually it's just the database because we're gonna use the precompiled binary for this tutorial.

```bash
sudo apt install postgresql
```

## Step 2: Installing plume binaries
Now let's download all the binaries.

```bash
mkdir -p /usr/www/plume
cd /usr/www/plume
wget -O plume.tar.gz <installation tarball link for postgres>
tar -xvf plume.tar.gz
chmod +x bin/*
rm plume.tar.gz
```

## Step 3: Configure
Now let's setup the database:

```bash
systemctl restart postgresql
su - postgres
createuser -d -P plume
createdb -O plume plume
exit
```

Then create the `.env` file for the configuration:

```bash
nano .env
```

Paste and change the following configuration:

```bash
DATABASE_URL=postgres://plume:PASSWORD@127.0.0.1:5432/plume
MIGRATION_DIRECTORY=migrations/postgres
BASE_URL=DOMAIN
```

And set the secret key using openssl

```bash
echo "ROCKET_SECRET_KEY=$(openssl rand -base64 32)" >> .env
```

Run the commands to migrate the database and create the instance:

```bash
./bin/plm migration run
./bin/plm search init
./bin/plm instance new
./bin/plm users new --admin
```

## Step 4: Add SystemD service file
Let's open the `plume.service` file:

```bash
nano /etc/systemd/system/plume.service
```

And paste the following content:

```service
[Unit]
Description=plume

[Service]
Type=simple
WorkingDirectory=/var/www/plume
ExecStart=/var/www/plume/bin/plume
TimeoutSec=30
Restart=always

[Install]
WantedBy=multi-user.target
```

Enable and run the file:

```bash
systemctl enable plume
systemctl start plume
systemctl status plume
```

## Step 5: Configuring the reverse proxy
Create the file `plume.conf`

```bash
nano /etc/nginx/sites-available/plume.conf
```

And paste the following content:

```nginx
server {
	listen 80;
	listen [::]:80;

	server_name plume.example.tld;

	return 301 https://plume.example.tld$request_uri;
}

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	location / {
		proxy_pass http://localhost:7878;

	    proxy_set_header Host $host;
    	proxy_set_header Connection       $http_connection;
	    proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
    	proxy_set_header X-Scheme         $scheme;
	    proxy_buffering                   off;
	}

	include /etc/nginx/snippets/letsencrypt.conf;

	server_name plume.example.tld;

	ssl_certificate /etc/letsencrypt/live/plume.example.tld/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/plume.example.tld/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/plume.example.tld/fullchain.pem;

	access_log /var/log/nginx/plume.access.log;
	error_log /var/log/nginx/plume.error.log;
}
```

Then enable this file by creating a symlink:

```bash
ln -s /etc/nginx/sites-available/plume.conf /etc/nginx/sites-enabled/plume.conf
```

## Step 6: Enable HTTPS
Make sure to create a new DNS entry for your domain name like this:

| Domain | Type | IP |
| --- | --- | --- |
| plume.example.tld | A | 12.34.56.78 |
| plume.example.tld | AAAA | 2a02:a03f:a1de:4600:467a:291d:a613:c637 | 

Then stop nginx and generate a certificate:

```bash
systemctl stop nginx
certbot certonly --standalone -d plume.example.tld
systemctl start nginx
```

## Conclusion
Congratulation, you just installed plume! You can now checkout your subdomain and plume should be there. You can then connect using your admin credentials and start writing and following blogs.
