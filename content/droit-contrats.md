+++
title = "Les contrats"
date = 2024-05-22T14:27:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "droit", "synthèse", "français" ]
+++
![Diagramme montrant les différentes étapes du cycle de vie d'un contrat en cercle](/cycle-vie-contrat.png)

Dans cette section nous allons voir le chapitre sur les contrats. Nous allons principalement voir les bases, le cycle d'un contrat lorsque tout se passe comme prévu. Nous allons voir dans d'autres articles ce qu'il se passe lorsqu'un contrat est invalide ou n'est pas respecté.
<!-- more -->

Un **contrat** est un **accord de volontés** entre plusieurs personnes avec l'intention de faire naitre des **effets de droit** (intention de bénéficier d'un droit et d'assumer des obligations).

Un contrat a un cycle de vie bien défini qui reprends la négotiation, l'accord de volontés, la rédaction, la signature, l'exécution, le contrôle et la fin.

## 1. Phase de négotiation d'un contrat
Ce cycle de vie commence par la **négociation** d'un contrat qui consiste à discuter les termes du contrat. Cette phase n'engage généralement rien, mais pas toujours. Parfois, cloturer les négotiations prématurément peut être considéré comme une faute, par exemple si on fait perdre beaucoup de temps à une entreprise.

Cela est généralement pourquoi avant de commencer une négotiation on la signe pour limiter la clôture du contrat, cela s'appelle un **NDA** (Non Disclosure Agreement) ou accord de confidentialité en français. Cela permet également de protéger les informations échangées dans le contrat, notament en les étampillant les informations considérées comme confidentielles.

Un NDA est en quelque sorte un contrat à propos du contrat.

## 2. Accord de volonté
Comme vu précédemment l'accord de volonté est le coeur même d'un contrat. Le fait que le simple accord de volonté suffise à définir un contrat montre à quel point un contrat est une notion vague. 

Il existe donc par conséquent beaucoup de types de contrats, de catégories et de variations. Nous allons en voir quelques une ici.

Un contrat peut être **unilatéral** ou **bilatéral**. On dit qu'un contrat est **unilatéral** si une seule des partie n'a des obligations (exemple : donation ou caution), et on dit qu'il est **bilatéral** si les deux parties ont des obligations (exemple : prestation rémunérée). 

Un contrat peut également être **gratuit** (si une seule partie n'a des avantages) ou **onéreux** (si les deux parties reçoivent des avantages). 

Dans le cas d'un contrat onéreux, si le gain d'une des partie n'est pas certain (exemple, "vous recevrez 10% des gains potentiel de l'entreprise"), on dit que le contrat est **aléatoire**. Dans le cas où le gain des deux parties est certain (par exemple, "tu me donne 1€, je te donne un chocolat"), on dit que le contrat est **commutatif**.

Un contrat peut également être **pur**, **à terme** ou **sous condition**. Un contrat est dit **pur** si l'obligation qui s'en dégage est immédiate après la création du contrat.

Un contrat est dit **à terme** si il ne se met en oeuvre qu'a une certaine date. 

Enfin un contrat est dit **à condition** si sa mise en action dépends d'autre facteurs. La condition peut être soit **suspentive** (le contrat commence lorsque la condition arrive), ou **résolutoire** (le contrat se termine lorsque la condition arrive).

### Principe de consensualisme
Il existe différentes formes, le contrat **consensuel** (**principe de consensualisme**) qui est formé par le seul accord des volontés (pas d'écrit ou juste obligatoire, pas d'exigence de forme pour être valide). 

Un exemple de tel type de contrat est la vente. Pour vendre quelque chose il n'y a pas besoin de remplir un formulaire ou de faire quoi que ce soit de particulier. Cependant avoir une preuve écrite est utile pour pouvoir prouver un **vice de consentement** (on verra cela plus tard). La preuve écrite est donc un moyen de se protéger sur un contrat en pouvant prouver que l'autre partie n'a pas respecté ses obligations.

Le contrat **formel** est une exception au principe de consensualisme vu plus tot, ici pour être valide il doit être soumis à une exigence de forme.

Un exemple de contrat forml est le crédit hypothécaire, le contrat de mariage ou le crédit à la consommation qui ont tous des exigences de forme pour être valide. Si le formalisme n'est pas respecté, le contrat est nul (il est considéré comme inexistant).

Le contrat **réel** qui est aussi une exception au principe de consensualisme où ici le contrat est formé par la remise d'une chose à l'autre partie, en plus de l'échange de consentement.

Un exemple de contrat réel est le contrat de dépot ou de prêt (on demande à quelqu'un de garder quelque chose ou on lui prête un objet), dans ces contrats, si la chose n'est pas remise physiquement, on ne peut pas considéré que le contrat est valide. Le contrat n'est donc seulement valide qu'a partir du moment où la chose a été remise.

## 3 et 4. Rédaction et signature
Une fois que les termes du contrat a été négocié et que les deux parties ont indiqué être d'accord. Le contrat peut alors être rédigé et signé (comme dit plus tot ce n'est pas forcément nécessaire, par exemple dans le cas d'une vente).

Mais si elle est faite de manière formelle, la signature peut être de deux formes soit manuscrite (qui doit correspondre à celle de la carte d'identité), ou électronique (scan de document, cryptographie, biométrique, etc).

La personne qui signe un contrat est une personne physique ou un mandataire réprésentant une personne morale (entreprise, organisation, ASBL, etc)

Une personne **morale** est un groupement d'individus réunis dans un interet commun auquel la loi confère une existence et une personalité juridique propre. Une **personne** en droit est donc quelque chose ou quelqu'un disposant de droits et d'obligations.

### Signature électronique
Il existe plusieurs sortes de signature électronique, la signature électronique **ordinaire** qui peut simplement consister à cocher une case ou écrire son nom en bas d'un e-mail ou d'un document, la signature électronique **avancée** qui consiste à utiliser un utiliser un système permettant de lier un contenu, une identité et une signature ensemble de façon à assurer qu'elle est authentique, non falsifiable et non réutilisable. 

Enfin il a une dernière forme de signature électronique qui est la seule qui est assimilée en droit au même niveau qu'une signature manuscrite. C'est la signature électronique **qualifiée** qui utilise un certificat de sécurité officiel. C'est notament le cas des signatures faite avec une carte d'identité. Cette forme renforce ainsi l'authenticité de la signature par rapport à la signature électronique avancée.

## 5. Exécution et contrôle du contrat
A partir du moment où le contrat est signé, il est alors mis en application et son application est contrôlée mutuellement par les deux parties.

Ainsi chaque partie doit appliquer les termes du contrat. Il y adeux pratiques d'exécutions :

Le principe de la **convention-loi** qui indique que chaque partie doit exécuter ce qui a été convenu, qu'une partie ne peut pas définir *seule* la fin d'un contrat ou le modifier, à moins qu'une exception ait été explicitement définie dans le contrat lui-même.

Le deuxième principe est **l'exécution de bonne foi**. Pendant toute la vie du contrat on considère qu'il ne faut pas abuser de sa position économique, ni sanctionner/mettre fin au contrat pour un manquement sans gravité. 

Ce principe va dans les deux sens, il ne faut donc pas abuser de ses droits (voir "abus de droit"), et il est également du devoir de la victime de limiter son dommage et d'éviter qu'il ne s'aggrave. 

Vous allons voir dans un autre post, ce qu'il se passe lorsqu'une des partie ne respecte pas ses engagements.
