+++
title = "Introduction au modèle OSI"
date = 2024-05-14T14:15:00+02:00
draft = false

[taxonomies]
tags = [ "cours", "réseau", "français", "synthèse" ]
+++

![](/Les_modèles/2024-02-02_14-09-22_screenshot.png)

Le modèle OSI est une norme de communication réseau proposée par ISO.
Elle met ainsi en relation plusieurs protocoles de communications
différents (IP, HDLC, TCP, UDP, HTTP, etc).

<!-- more -->

![](/Les_modèles/2024-02-02_14-07-12_screenshot.png)

Voici par exemple une liste de différents protocoles pour chaque couche
du modèle.

![](/Les_modèles/2024-02-02_14-09-07_screenshot.png)

