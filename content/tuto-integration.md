+++
title = "WIP Tuto : Comment faire un exercice d'intégration"
date = 2024-05-17T18:20:00+02:00
draft = false

[taxonomies]
tags = [ "réseau", "tuto", "cours", "français", "synthèse" ]
+++

**ATTENTION !!!** Cette page est en train d'être construite. Donc c'est le bordel et vous y comprendrez probablement rien.
<!-- more -->

---

Dans cette synthèse on va voir une procédure type et des exemples d'exercices d'intégration de l'examen.

**Disclaimer :** Il est fortement conseillé d'aller voir les autres synthèses sur le cours de réseau afin de comprendre au mieux le cours car l'exercice d'intégration ne représente qu'une partie de l'examen oral, le reste sera des questions théoriques sur le cours. 


Un exercice d'intégration se compose généralement de trois phases : 

- La phase de la résolution de nom (sauf si explicitement indiqué dans l'énoncé qu'elle n'a pas besoin d'être couverte)
- L'envois de la requète du client au serveur et sa réception
- L'envois de la réponse du serverur au client et sa réception

Pour chacune de ces phases il faut décrire ce qui se passe de la manière la plus complète possible à toutes les couches du modèles OSI, c'est-à-dire au niveau applicatif (DNS, HTTP, IMAP, POP, etc), niveau transport (TCP, UDP), niveau réseau (IPv4 ou IPv6), au niveau de l'accès réseau (Ethernet, etc) et enfin au niveau de la sécurité (firewall, protectoins mises en place, chiffrement, VPN, etc).

Je vais ici partir d'un exemple d'énoncé, pour le décrire dans les détails et essayer de donner les explications sur comment répondre pour chaque niveau en fonction du protocole utilisé ou des sécurité mises en place.

## Étapes globales pour répondre à l'exercices
> Pour des raisons de propriété intellectuelle et des règles de l'école je ne peux pas mettre un exemple d'ancien examen ici. Je vais donc vous recommander de prendre un examen récapitulatif à coté pour l'exemple.

Premièrement, il faut identifier les informations utiles dans le texte, il faut donc identifier les informations principales suivantes :

- Quel protocole applicatif et transport ?

| Situation | Protocole applicatif | Protocole transport | 
| --------- | -------------------- | ------------------- |
| Site internet | HTTP ou HTTPS | TCP, port 80 ou 443 |
| Envois d'email | SMTP | TCP, port 25 | 
| Reception d'email | IMAP ou POP3 | TCP, port 993 |
| Resolution du nom de domaine | DNS | UDP, port 53 |

- Quel est le protocole réseau utilisé ? IPv4 ou IPv6 ?
	+ Est ce que toutes les partie du réseau supportent IPv6, et si non, quels systèmes sont mis en place pour palier à ce problème (NAT, 6to4, tunnel brokers, 6rd, dual stack, ou autre) ?
- Quel système de sécurité est mis en place
	+ VPN, si oui quel type de sécurité (PPTP, IPSec, TLS, etc)
	+ Y a-t'il un firewall et si oui comment est-il configuré
	+ Y a-t'il des VLAN configuré sur le switch ?
	+ Si le site est sécurisé (HTTPS, TCP port 443), quel type de certificat est utilisé (autosigné, autorité de certification, etc)

## Réponses types en fonction du protocole


### Si c'est le protocole HTTP

> Expliquer que le client construit un APDU HTTP en y placant le type de requète (GET, DELETE, PATCH, PUT, POST ou HEAD), une entête en y décrivant les métadonnées de l'appareil, la requète et le navigateur, l'hôte, le chemin de l'url, et que le corps de la requète est laissé vide (dans le cas d'un GET, ce n'est pas le cas pour un POST par exemple)

Pour accéder au site *http://www.swilabus.be/cours*, Le client va construire un APDU (Application Protocol Data Unit) HTTP avec la commande **GET** qui va contenir en entête le chemin de l'URL demandé, soit */cours*, ainsi que l'hôte demandé; soit *www.swilabus.com*, des métadonnées sur le navigateur et sur la machine tel que le système d'exploitation, le navigateur, la version, la langue, etc. Dans le cas où l'utilisateur·ice est authentifié·e, l'entête contiendra aussi un token d'authentification dans les cookies. Enfin, le corps de la requète sera laissé vide.

> Voir la section sur TCP

```
GET /cours HTTP/2
Host: www.swilabus.com
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:124.0) Gecko/20100101 Firefox/124.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate, br
DNT: 1
Sec-GPC: 1
Connection: keep-alive
Referer: https://www.swilabus.com/
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Pragma: no-cache
Cache-Control: no-cache
```

### Si le protocole de transport est TCP
Une fois l'APDU créé, ce dernier est placé dans un TPDU (Transport Protocol Data Unit) TCP dont le port source sera définit comme le premier port disponible au delà de 1024, et le port de destination comme le port 443 dans le cas de l'HTTPS (ce qui est notre cas ici), ou port 80 dans le cas de l'HTTP.

Le TPDU va aussi contenir d'autres informations tel que la taille de l'entête, le numéro de séquence, le numéro d'aquittement, les indicateurs (ACK pour acquis, SYN pour demadner une connexion, FIN pour la terminer, etc), la taille de la fenêtre glissante qui indique le nombre de TPDU qui peuvent être envoyé sans devoir attendre un acquis, un checksum qui permet de vérifier l'intégrité des donneés.

> Les informations qui suivent sont des informations bonus à propos de couche de transport mais il est possible que vous n'ayez pas le temps de tout caser, et c'est pas grave.

### En cas de perte de paquet
Lorsque le TPDU sera envoyé, TCP s'assurera que les données sont intactes et qu'il n'y pas de données perdues en aquittant chaque paquet reçu au bout de l'épuisement de la fenêtre glissante. 

Ainsi lorsqu'un paquet est perdu, l'émetteur pourra le remarqué par l'absence de l'aquis correspondant. 

Ensuite le client pourra aquitter de nouveau les paquets maintenant reçu, soit en aquittant uniquement le nouveau paquet et en demandant ainsi à l'émetteur de les ré-émettre (go-back-n), soit en acquittant le dernier paquet reçu en séquence (si il a reçu 2, 3, 4, 5 au total, il pourra acquitter 5, ce que le serveur comprendera comme ayant aquitter les 4 TPDU).

Si des paquets sont perdu et que le reçeveur le remarque avant la fin du temporisateur, il peut envoyer 3 acquis identique pour le TPDU perdu, ce que l'émetteur comprendra comme un message qu'il faut renvoyer le TPDU. Cela permet ainsi d'accélérer le transfert.

### Si le protocole est IPv4
Le TPDU sera alors placé dans un paquet IPv4 indiquant la version (4), la longueur de l'entête, la longueur totale du paquet, l'identifiant du paquet, les flags pour indiquer si le paquet est fragmenté ou non (par défault il ne l'est pas), le fragmentation offset qui indique le déplacement par rapport au paquet initial (0 par défault), le TTL qui indique le temps de survie dans le réseau afin d'éviter les boucles (chaque routeur le décrémentera de 1, et si il arrive à 0, le paquet est perdu, cette valeur change en fonction des SE), **le protocole supérieur** (TCP ou UDP), le checksum, **l'addresse IP source**, **l'adresse IP destination**, des options et les **données**.

La fragmentation IPv4 est utilisée lorsque la taille maximum autorisée d'un paquet (MTU) change en fonction des réseaux et qu'il faut donc diviser le paquet pour qu'il entre un autre réseau.

### Si le protocole est IPv6
Le TPDU sera alors placé dans un paquet IPv6 en indiquant la version (6), le type de traffic (qui permet d'affiner la qualité de service), le flow label (qui permet de grouper des paquets comme faisant partie d'un flow de paquet, comme une vidéo par exemple), la longueur des données, le "next header" (qui indique le protocole suppérieur, soit TCP soit UDP), le hop-limit qui indique la durée de vie du paquet sur le réseau, l'adresse IP source et l'adresse IP destination.





## Requète
- **[DNS]** Pour pouvoir connaître le nom de domaine, le paquet est placé dans un APDU DNS en y indiquant le nom de domaine demandé
- **[HTTP/HTTPS]** Pour pouvoir accéder au site Internet, la machine va indiquer le chemin demandé dans une requète de type GET, en y indiquant l'hôte et d'autres informations tel que des métadonnées, les cookies, etc. Le corps de la requète sera laissé vide.
- **[HTTP]** L'APDU HTTP va ensuite être placé dans un TPDU TCP où le port source sera le premier port disponible à partir de 1024, et le port de destination sera 80
- **[HTTPS]** L'APDU HTTP va ensuite être placé dans un TPDU TCP où le port source sera le premier port disponible à partir de 1024, et le port de destination sera 443
- Le TPDU sera ensuite placé dans un paquet IP où l'IP source sera l'IP de la machine et l'IP destination sera l'IP du serveur qui a été récupérée lors de la requète DNS
- **[VPN]** Le paquet IP sera alors chiffré et passé dans un TPDU UDP avec comme port source, le premier disponible et comme port destination, celui du VPN il sera alors encapsulé dans un paquet IP classique avec comme ip source celle de la machine et comme IP destination celle du serveur VPN.
- **[Serveur externe au réseau]** Etant donné que l'adresse de destination est en dehors du sous-réseau de la machine, le paquet sera adressé au routeur qui fera office de relai vers l'extérieur
- **[Si couche liaison doit être définie && IPv4]** Cepandant pour pouvoir communiquer avec le routeur il faut connaître son adresse MAC. Etant donné que l'on est en IPv4, on va utiliser le protocole ARP. On va donc demander en broadcast (à tout le monde) sur le réseau qui est "192.168.1.1" (adresse du routeur), la trame va avoir comme destination "FF:FF:FF:FF:FF:FF" (tout le monde)
- **[Si couche liaison doit être définie && IPv4]** Le routeur va alors répondre à la requète avec "192.168.1.1 c'est moi et je suis FC:56:36:AB:66:21".
- **[Si couche liaison doit être définie && IPv6]** Cependant pour pouvoir communiquer avec le routeur il faut connaître son adresse MAC. Etant donné que l'on est en IPv6, on va utiliser le protocol NDP (Neighbor Discovery Protocol), qui va demander en multicast "qui est le routeur ?" (Router Discovery). Pour créer la trame multicast IPv6 on définit la destination comme `33:33` suivi des 32 derniers bits de l'adresse multicast. 
- **[Si couche liaison doit être définie && IPv6]** Le routeur va alors répondre à la requète avec "9c:fc:e8:e5:7e:9b c'est moi et je suis FC:56:36:AB:66:21" (Router Advertisement).
- **[Si couche liaison doit être définie]** La machine peut maintenant encapsuler les paquets IP dans des trames avec comme destination l'adresse MAC découverte du routeur.
- **[NAT]** Le paquet est ensuite envoyé au routeur, à la réception le routeur vérifie que le paquet lui est adressé et va aller le renvoyer à la machine en regardant dans sa table de NAT qui correspond au port source en question
- Le paquet sera ensuite envoyé au serveur


## Réponse
- **[IPv4 && MTU < taille du paquet]** Etant donné que la taille maximum que peux acceuillir le réseau de destination est inférieur à celle du paquet IPv4, le paquet va devoir être fragmenter pour pouvoir passer sur le réseau. Cela est fait à l'aide des champs "fragmentation offset" qui indique le nombre de paquets par rapport à l'initial et des flags du paquet qui indiquent si le paquet est fragmenté ou non. 
- **[Si couche réseau doit être définie && IPv4]** Cepandant pour pouvoir communiquer avec la machine (ou le firewall ou autre sur le réseau) il faut connaître son adresse MAC. Etant donné que l'on est en IPv4, on va utiliser le protocole ARP. On va donc demander en broadcast (à tout le monde) sur le réseau qui est "192.168.1.1" (adresse du routeur), la trame va avoir comme destination "FF:FF:FF:FF:FF:FF" (tout le monde)
- **[Si couche liaison doit être définie && IPv6]** Cependant pour pouvoir communiquer avec une machine il faut connaître son adresse MAC. Etant donné que l'on est en IPv6, on va utiliser le protocol NDP (Neighbor Discovery Protocol), qui va demander en multicast "qui est la machine 00:2b:67:99:a7:fb ?" (Neighbor Discovery). Pour créer la trame multicast IPv6 on définit la destination comme `33:33` suivi des 32 derniers bits de l'adresse multicast. 
- **[Si couche liaison doit être définie && IPv6]** La machine va alors répondre à la requète avec "9c:fc:e8:e5:7e:9b c'est moi et je suis FC:56:36:AB:66:21" (Neighbor Advertisement).
- **[Si couche liaison doit être définie && IPv4]** La machine/firewall/reverse proxy va alors répondre à la requète avec "192.168.1.1 c'est moi et je suis FC:56:36:AB:66:21".
- **[Si couche liaison doit être définie]** La machine peut maintenant encapsuler les paquets IP dans des trames avec comme destination l'adresse MAC découverte du routeur.
- **[Firewall]** Etant donné qu'un firewall est configuré, ce dernier va récupérer le paquet et l'analyser en fonction des règles qui lui ont été définie. Si tout est conforme il le laisse passer, sinon il le supprime.
- **[Reverse proxy]** Ensuite le reverse proxy va modifier le paquet pour définir le port destination comme le port destination sur la machine ainsi que l'IP destination à celle de la machine sur le réseau, et enfin il va enregistrer l'IP et le port source et va les modifier sur le paquets pour indique sa propre IP et port source à la place. Il va ensuite envoyer le paquet à la bonne machine.
- **[VPN]** Le VPN va reçevoir le paquet, il va alors vérifier qu'il lui est destiné en regardant le port UDP et l'IP destination. Il va alors l'ouvrir, le déchiffrer et traiter le paquet IP qui est à l'intérieur en ré-émettant le paquet en modifiant l'adresse IP source et le port source.
- **[DNS]** Le serveur qui reçoit la requète va ensuite demander au serveur DNS mondiaux quel est le serveur qui s'occupe du domaine .be, elle va ensuite intéroger le serveur DNS de .be, qui va rediriger vers le serveur DNS de swilbus.be, qui une fois interogé va indiquer l'adresse IP du serveur en question.
- **[DNS]** Le serveur DNS va construire un APDU DNS qui va indiquer quel adresse IP correspond à www.swilabus.be. 
- **[HTTP/HTTPS]** Le serveur va ensuite reçevoir le paquet, il va ensuite pouvoir récupérer l'APDU HTTP et trouver les données à répondre
- **[HTTP/HTTPS]** Le serveur va ensuite créer un APDU de réponse HTTP qui va contenir un code status (200 si la page a bien été trouvée et envoyée), des informations en entête (comme les cookies), et le code HTML (ou autre donnée) de la page en corps de réponse.
- **[HTTP]** Le serveur va ensuite placer son/ses APDU HTTP dans des TPDU TCP où le port source sera le port 80 et le port de destination sera le port de destination choisis par la machine cliente
- Le serveur va ensuite placer son/ses TPDU dans un/des paquets IP en précisant comme adresse IP source, sa propre adresse IP, comme adresse IP destination, celle qu'il avait reçu dans sa requète comme source
- Le serveur va ensuite envoyer ce/ces paquets vers la machine/firewall/reverse proxy
- **[Reverse proxy]** Le paquet va alors être envoyé vers le proxy inversé, qui va s'occuper de définir comme IP destination et port destination, celle qui avait été définit à la base dans le paquet reçu. Ensuite il va définir le port source et l'IP source à son IP et port source.
- **[VPN]** Le VPN va alors recevoir la réponse du serveur et va l'encapsuler dans un TPDU UDP avec comme port source le port du VPN, et comme port de destination celui qui avait été défini par le client comme port source. Ce TPDU UDP va ensuite être placé dans un paquet IP à destination du client VPN.
- **[NAT]** Le paquet est ensuite envoyé au routeur, à la réception le routeur vérifie que le paquet lui est adressé et va aller le renvoyer à la machine en regardant dans sa table de NAT qui correspond au port source en question
- **[VPN]** Le client va alors voir que le paquet lui est adressé et est adressé plus précisément au VPN (grâce au port de destination), il va alors récupérer le paquet IP interne et le déchiffrer. 
- **[HTTP/HTTPS]** La réponse sera alors reçue par la machine et pourra être affichée dans le navigateur du client
- **[DNS]** Le paquet est donc transféré vers la machine qui connait à présent l'IP du serveur demandé

### 


<!--
- Requète HTTPS
- Utilistaion de VLAN
- Précision plus complète sur le routage
- Cas de perte de paquet
- 6to4
- Protocole mail SMTP
- NAT coté serveur


- Comment les boucles sont-elle évitées ?
- 
-->

<!-- FAIT :
- Utilisation d'un NAT
- Requète DNS externe
- Requète HTTP
- Utilisation d'un reverse proxy
- MTU différent, fragmentation nécessaire
- Description des trames IPv4
- Description des trames IPv6

-->

<!-- 

Entrée dans un réseau =
- NAT → récupère l'ip destination et le port source dans sa table
- VPN → déchiffre le paquet IP
- réponse HTTP → 
- requète HTTP →
- requète DNS → 
- réponse DNS →


-->

## Entrée dans un réseau
- **[IPv4 && MTU < taille du paquet]** Etant donné que la taille maximum que peux acceuillir le réseau de destination est inférieur à celle du paquet IPv4, le paquet va devoir être fragmenter pour pouvoir passer sur le réseau. Cela est fait à l'aide des champs "fragmentation offset" qui indique le nombre de paquets par rapport à l'initial et des flags du paquet qui indiquent si le paquet est fragmenté ou non. 
- **[Si couche réseau doit être définie && IPv4]** Cepandant pour pouvoir communiquer avec la machine (ou le firewall ou autre sur le réseau) il faut connaître son adresse MAC. Etant donné que l'on est en IPv4, on va utiliser le protocole ARP. On va donc demander en broadcast (à tout le monde) sur le réseau qui est "192.168.1.1" (adresse du routeur), la trame va avoir comme destination "FF:FF:FF:FF:FF:FF" (tout le monde)
- **[Si couche liaison doit être définie && IPv6]** Cependant pour pouvoir communiquer avec une machine il faut connaître son adresse MAC. Etant donné que l'on est en IPv6, on va utiliser le protocol NDP (Neighbor Discovery Protocol), qui va demander en multicast "qui est la machine 00:2b:67:99:a7:fb ?" (Neighbor Discovery). Pour créer la trame multicast IPv6 on définit la destination comme `33:33` suivi des 32 derniers bits de l'adresse multicast. 
- **[Si couche liaison doit être définie && IPv6]** La machine va alors répondre à la requète avec "9c:fc:e8:e5:7e:9b c'est moi et je suis FC:56:36:AB:66:21" (Neighbor Advertisement).
- **[Si couche liaison doit être définie && IPv4]** La machine/firewall/reverse proxy va alors répondre à la requète avec "192.168.1.1 c'est moi et je suis FC:56:36:AB:66:21".
- **[Si couche liaison doit être définie]** La machine peut maintenant encapsuler les paquets IP dans des trames avec comme destination l'adresse MAC découverte du routeur.
- **[Firewall]** Etant donné qu'un firewall est configuré, ce dernier va récupérer le paquet et l'analyser en fonction des règles qui lui ont été définie. Si tout est conforme il le laisse passer, sinon il le supprime.
- **[Reverse proxy]** Ensuite le reverse proxy va modifier le paquet pour définir le port destination comme le port destination sur la machine ainsi que l'IP destination à celle de la machine sur le réseau, et enfin il va enregistrer l'IP et le port source et va les modifier sur le paquets pour indique sa propre IP et port source à la place. Il va ensuite envoyer le paquet à la bonne machine.
- **[VPN]** Le VPN va reçevoir le paquet, il va alors vérifier qu'il lui est destiné en regardant le port UDP et l'IP destination. Il va alors l'ouvrir, le déchiffrer et traiter le paquet IP qui est à l'intérieur en ré-émettant le paquet en modifiant l'adresse IP source et le port source.
- **[Requète DNS]** Le serveur qui reçoit la requète va ensuite demander au serveur DNS mondiaux quel est le serveur qui s'occupe du domaine .be, elle va ensuite intéroger le serveur DNS de .be, qui va rediriger vers le serveur DNS de swilbus.be, qui une fois interogé va indiquer l'adresse IP du serveur en question.
- **[Requète HTTP/HTTPS]** Le serveur va ensuite reçevoir le paquet, il va ensuite pouvoir récupérer l'APDU HTTP et trouver les données à répondre
- **[Réponse HTTP/HTTPS]** La réponse sera alors reçue par la machine et pourra être affichée dans le navigateur du client
- **[Réponse DNS]** Le paquet est donc transféré vers la machine qui connait à présent l'IP du serveur demandé et va la mettre en cache


## Sortie d'un réseau
- **[Réponse HTTP/HTTPS]** Le serveur va ensuite créer un APDU de réponse HTTP qui va contenir un code status (200 si la page a bien été trouvée et envoyée), des informations en entête (comme les cookies), et le code HTML (ou autre donnée) de la page en corps de réponse.
- **[Réponse HTTP/HTTPS]** Le serveur va ensuite placer son/ses APDU HTTP dans des TPDU TCP où le port source sera le port 80 et le port de destination sera le port de destination choisis par la machine cliente
- Le serveur va ensuite placer son/ses TPDU dans un/des paquets IP en précisant comme adresse IP source, sa propre adresse IP, comme adresse IP destination, celle qu'il avait reçu dans sa requète comme source
- Le serveur va ensuite envoyer ce/ces paquets vers la machine/firewall/reverse proxy/VPN
- **[VPN]** Le paquet IP sera alors chiffré et passé dans un TPDU UDP avec comme port source, le premier disponible et comme port destination, celui du VPN il sera alors encapsulé dans un paquet IP classique avec comme ip source celle de la machine et comme IP destination celle du serveur VPN.
- **[Reverse proxy]** Le paquet va alors être envoyé vers le proxy inversé, qui va s'occuper de définir comme IP destination et port destination, celle qui avait été définit à la base dans le paquet reçu. Ensuite il va définir le port source et l'IP source à son IP et port source.
- **[NAT]** Le paquet est ensuite envoyé au routeur, à la réception le routeur vérifie que le paquet lui est adressé et va aller le renvoyer à la machine en regardant dans sa table de NAT qui correspond au port source en question



- **[VPN]** Le client va alors voir que le paquet lui est adressé et est adressé plus précisément au VPN (grâce au port de destination), il va alors récupérer le paquet IP interne et le déchiffrer. 
- **[HTTP/HTTPS]** La réponse sera alors reçue par la machine et pourra être affichée dans le navigateur du client
- **[DNS]** Le paquet est donc transféré vers la machine qui connait à présent l'IP du serveur demandé


Pour chaque exercice il faut constuire les différentes étapes du chemin pour aller faire une requète DNS, recevoir une réponse, faire une requète au serveur et reçevoir une réponse. Ainsi cela peut donner quelque chose comme ceci :

Requète DNS :

1. Création d'une requète (APDU + TPDU + paquet IP)
2. Chiffrement des paquets via le VPN et envois au serveur
3. Vérification des paquets par le firewall
4. Déchiffrement des paquets par le VPN et ré-émission
5. Récupération par le serveur DNS et recherche de l'IP correspondant au serveur

Réponse DNS : 
1. Création d'une réponse DNS et envois au VPN
2. Chiffrement de la réponse et envois au client
3. Déchiffrement au niveau du client et mise en cache de l'IP correspondant au serveur web

Requète HTTPS :
1. Etablissement d'une connexion avec le serveur web
2. Création d'une requète HTTPS et chiffrement
3. Chiffrement des paquets par le VPN et envois
4. Vérification des paquets par le firewall
5. Déchiffrement par le VPN et ré-émission des paquets

Réponse HTTPS :
1. Réception des paquets par le serveur web, et création d'une réponse
2. Chiffrement de la réponse par le VPN et envois au client
3. Déchiffrement de la réponse par le client et affichage dans le navigateur

Ensuite, il faut détailler et exemplifier ces différentes étapes. Voici des détails pour chaque type d'étape.

### Création d'une requète HTTP
Pour pouvoir accéder au site Internet, la machine va indiquer le chemin demandé dans une requète de type GET, en y indiquant l'hôte et d'autres informations tel que des métadonnées, les cookies, etc. Le corps de la requète sera laissé vide.

L'APDU HTTP va ensuite être placé dans un TPDU TCP où le port source sera le premier port disponible à partir de 1024, et le port de destination sera 80

L'APDU HTTP va ensuite être placé dans un TPDU TCP où le port source sera le premier port disponible à partir de 1024, et le port de destination sera 443

Le TPDU sera ensuite placé dans un paquet IP où l'IP source sera l'IP de la machine et l'IP destination sera l'IP du serveur qui a été récupérée lors de la requète DNS

### TODO Création d'une requète HTTPS
Pour pouvoir accéder au site Internet, la machine va indiquer le chemin demandé dans une requète de type GET, en y indiquant l'hôte et d'autres informations tel que des métadonnées, les cookies, etc. Le corps de la requète sera laissé vide.

La requète va ensuite être chiffrée à l'aide 

L'APDU HTTP va ensuite être placé dans un TPDU TCP où le port source sera le premier port disponible à partir de 1024, et le port de destination sera 443

Le TPDU sera ensuite placé dans un paquet IP où l'IP source sera l'IP de la machine et l'IP destination sera l'IP du serveur qui a été récupérée lors de la requète DNS

### Chiffrement d'une requète HTTP
### Création d'une requète DNS
### Création d'une requète SMTP
### Chiffrement des données par le VPN
Le paquet IP sera alors chiffré et passé dans un TPDU UDP avec comme port source, le premier disponible et comme port destination, celui du VPN il sera alors encapsulé dans un paquet IP classique avec comme ip source celle de la machine et comme IP destination celle du serveur VPN.

### Entrée de paquets dans un NAT
Le paquet est ensuite envoyé au routeur, à la réception le routeur vérifie que le paquet lui est adressé et va aller le renvoyer à la machine en regardant dans sa table de NAT qui correspond au port source en question

### Sortie de paquets d'un NAT
Le paquet est ensuite envoyé au routeur car la destination n'est pas présente dans le même réseau, à la réception le routeur vérifie que le paquet lui est adressé et va aller le renvoyer à la machine en regardant dans sa table de NAT qui correspond au port source en question.

### Vérification des paquets d'entrée par le firewall
Etant donné qu'un firewall est configuré, ce dernier va récupérer le paquet et l'analyser en fonction des règles qui lui ont été définie. Si tout est conforme il le laisse passer, sinon il le supprime.

### Redirection des paquets entrant avec un proxy inversé
Ensuite le reverse proxy va modifier le paquet pour définir le port destination comme le port destination sur la machine ainsi que l'IP destination à celle de la machine sur le réseau, et enfin il va enregistrer l'IP et le port source et va les modifier sur le paquets pour indique sa propre IP et port source à la place. Il va ensuite envoyer le paquet à la bonne machine.

### Redirection des paquets sortant avec un proxy inversé
Le paquet va alors être envoyé vers le proxy inversé, qui va s'occuper de définir comme IP destination et port destination, celle qui avait été définit à la base dans le paquet reçu. Ensuite il va définir le port source et l'IP source à son IP et port source.

### Déchiffrement des donnés par le VPN
Le VPN va reçevoir le paquet, il va alors vérifier qu'il lui est destiné en regardant le port UDP et l'IP destination. Il va alors l'ouvrir, le déchiffrer et traiter le paquet IP qui est à l'intérieur en ré-émettant le paquet en modifiant l'adresse IP source et le port source.

### Recherche de l'IP et envois de la réponse DNS
Le serveur qui reçoit la requète va ensuite demander au serveur DNS mondiaux quel est le serveur qui s'occupe du domaine .be, elle va ensuite intéroger le serveur DNS de .be, qui va rediriger vers le serveur DNS de swilbus.be, qui une fois interogé va indiquer l'adresse IP du serveur en question.


### Création d'une réponse HTTP
Le serveur va ensuite créer un APDU de réponse HTTP qui va contenir un code status (200 si la page a bien été trouvée et envoyée), des informations en entête (comme les cookies), et le code HTML (ou autre donnée) de la page en corps de réponse.

### Création d'une réponse SMTP

### Recherche de l'adresse MAC d'une IPv4 et envois des trames (couche accès réseau)
Pour pouvoir communiquer avec la machine (ou le firewall ou autre sur le réseau) il faut connaître son adresse MAC. Etant donné que l'on est en IPv4, on va utiliser le protocole ARP. On va donc demander en broadcast (à tout le monde) sur le réseau qui est "192.168.1.1" (adresse du routeur), la trame va avoir comme destination "FF:FF:FF:FF:FF:FF" (tout le monde)

La machine/firewall/reverse proxy va alors répondre à la requète avec "192.168.1.1 c'est moi et je suis FC:56:36:AB:66:21".

La machine peut maintenant encapsuler les paquets IP dans des trames avec comme destination l'adresse MAC découverte du routeur.

### Recherche de l'adresse MAC d'une IPv5 et envois des trames (couche accès réseau)
Pour pouvoir communiquer avec une machine il faut connaître son adresse MAC. Etant donné que l'on est en IPv6, on va utiliser le protocol NDP (Neighbor Discovery Protocol), qui va demander en multicast "qui est la machine 00:2b:67:99:a7:fb ?" (Neighbor Discovery). Pour créer la trame multicast IPv6 on définit la destination comme `33:33` suivi des 32 derniers bits de l'adresse multicast. 

La machine va alors répondre à la requète avec "9c:fc:e8:e5:7e:9b c'est moi et je suis FC:56:36:AB:66:21" (Neighbor Advertisement).

La machine peut maintenant encapsuler les paquets IP dans des trames avec comme destination l'adresse MAC découverte du routeur.

### Fragmentation d'un paquet si le MTU est plus petit que la taille du paquet
Étant donné que la taille maximum que peux acceuillir le réseau de destination est inférieur à celle du paquet IPv4, le paquet va devoir être fragmenter pour pouvoir passer sur le réseau. Cela est fait à l'aide des champs "fragmentation offset" qui indique le nombre de paquets par rapport à l'initial et des flags du paquet qui indiquent si le paquet est fragmenté ou non. 




