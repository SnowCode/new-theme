+++
title = "How to make an ON/OFF nether portal"
date = 2021-07-03
[taxonomies]
tags = ["english", "minecraft"]
[extra]
image = "portal/2021-07-03_18.44.46.png"
+++
Let's see how to make a super compact, cheap and simple ON/OFF redstone Nether portal in Minecraft.<!-- more -->

## Step 1: Gather building blocks
* 1 portal (10 obsidian)
* 1 redstone repeater
* 1 redstone
* 2 dispensers
* 1 sticky piston
* 2 observers
* 1 lever
* 2 pressure plates (optional)
* 1 lighter (flint&steel)
* 1 bucket of water

## Step 2: Build it

Make a simple portal
![](/portal/2021-07-03_18.42.26.png)
Place a sticky piston on the left side
![](/portal/2021-07-03_18.42.43.png)
Place one observer on it, facing the outside
![](/portal/2021-07-03_18.42.54.png)
Next to that observer place a dispenser and another observer facing downward to another dispenser
![](/portal/2021-07-03_18.43.21.png)
Place 1 repeater (3 ticks) and one redstone on top
![](/portal/2021-07-03_18.43.31.png)
In the top dispenser, place a lighter. And place a water bucket in the other one
![](/portal/2021-07-03_18.44.33.png)
Place a lever and 2 optional pressure plates  (to avoid water going everywhere if there's a bug)
![](/portal/2021-07-03_18.44.46.png)
