+++
title = "Couche d'accès réseau : les trames"
date = 2024-05-15T11:30:00+02:00
draft = false

[taxonomies]
tags = [ "réseau", "cours", "français", "synthèse" ]
+++

![](/2024-05-15-11-30-58-image.png)

Nous allons maintenant voir la couche de plus bas niveau du réseau qui est celle de l'accès réseau.

<!-- more -->

La couche d'accès réseau concerne l'échange d'information entre deux machines directement connectées. L'utilité de cette couche est simplement de partager les informations sur la ligne et le format de l'information dépends de la technologie sous-jacente (ethernet, etc)

## Délimitation de l'information

Pour pouvoir transmettre les données sur une ligne il est important de délimiter l'information affin d'en connaître sa taille et ainsi déterminer quand la **trame** commence et quand elle se termine.

> Pour en savoir plus sur la délimitation de l'information, voir [cet article](https://www.geeksforgeeks.org/various-kind-of-framing-in-data-link-layer/).

### Indication de la longueur

Une première méthode serait simplement de garder en mémoire la taille de la trame. Ainsi si on sait par exemple qu'une trame fait 8 bits, on reçoit les 8 bits d'information, ensuite on sait que la trame est finie.

Le problème arrive si une erreur se produit et que la longueur est impactée. Par exemple si la longueur passe de 8 à 10, une partie de la trame suivante va être prise dans la trame actuelle.

![](/2024-05-15-12-46-28-image.png)

Une manière de résoudre cela est d'utiliser des marqueurs de début et de fin en plus. Cela permet ainsi de vérifier que l'on ne prends pas une trame suivante comme donnée.

### Character stuffing

Pour cela il y a deux méthodes, le **character stuffing** qui insère un certain charactère comme début et fin. On va ainsi ajouter un octet particulier pour signifier le début ou la fin d'une trame. Ces octets sont présent dans la table ASCII sous le nom de `DLE` (Data Link Escape, qui indique un délimiteur), `STX` (Start-of-Text, qui indique un début de trame),  `ETX` (End-of-Text Character, qui indique la fin d'une trame).

Cependant cela pose un problème dans le cas où les données contiennent l'un de ces octets. Pour résoudre ce problème on va utilsier un troisième octet spécial de la table ASCII qui est `ESC` (Escape Character), qui sera placé devant les octets problématique dans les données. 

A la reception des données, l'octet après `ESC` sera ignoré. Et bien sûr on peut utiliser ESC pour ignorer ESC lui même. 

Dans l'image suivante, le `FLAG` représente `DLE STX` (en début de trame) ou `DLE ETX` (en fin de trame)

![https://media.geeksforgeeks.org/wp-content/uploads/20200721204752/t6y1.png](https://media.geeksforgeeks.org/wp-content/uploads/20200721204752/t6y1.png)

### Bit stuffing

Une autre méthode est le **bit stuffing** qui fonctionne de la même manière que le character stuffing, mais aux niveaux des bits. Ici `11111` est interpreté comme un début ou fin de trame. 

Pour empécher que cela cause des problèmes si la séquence se trouve dans les données, on va la suffixer par un `0` de transparence. Ensuite à la réception toutes les séquences de `111110` seront remplacée par `11111`.

## Détection d'erreurs

En plus des marqueurs de début et fin de trame et des données on va également indiquer une donnée supplémentaire pour savoir si l'information a été modifiée ou non, ainsi le destinataire peut vérifier l'intégrité de la donnée reçue. 

La performance de détection des erreurs dépends de la méthode utilisée.

- La **parité simple** consiste à simplement garder un bit de parité qui indique si il y a un nombre pair ou impair de bit dans la séquence. Cela signifie cependant que cette méthode ne peut détecter qu'un nombre impair d'erreurs.

- La **parité à deux dimensions** consiste à considérer les données comme un tableau et de garder un bit de partié pour chaque ligne et colonne. Cette méthode a l'avantage de permettre de pouvoir corriger une erreur et en détecter 2.

- Le **CRC (Cyclic Reddancy Check)** consiste à prendre un "code générateur", connu d'avance par les deux parties. Puis d'attacher une valeur aux donnée de sorte que les données soit exactement divisible par le code générateur. Ainsi à la reception, le destinataire a simplement besoin de vérifier que le reste de la division avec le code générateur est bien 0. 

## Accès au media

L'élément physique qui interconnecte les hôtes peut être soit **partagé**, dans quel cas tous les hôtes partagent le même média et doivent donc mettre en place des règles précises afin d'y accéder dans entrer en collision. C'est par exemple le cas du réseau sans fil, ou du cable coaxial.

Sinon ils peuvent être aussi dédié, où chaque hôte est relié séparément au réseau (par exemple via des cables RJ-45).

Il existe deux types de liaisons, les liaisons point-à-point (un émetteur et un destinataire, par exemple en xDSL) et les lignes boradcast (plusieurs émetteurs et plusieurs destinataires, par exemple en réseau local)

### Eviter les collisions

Pour éviter que plusieurs machines parlent en même temps sur la même ligne (écrasant ou en corrompant ainsi les données), il existe 3 grand types de protocoles.

- Les protocoles de **partitionnement du canal** où chaque noeud reçoit exactement une part équitable.

- Les protocoles à **accès aléatoire** où il est possible d'envoyer de l'information n'importe quand et où il y a une possibilité de collision entre deux hôtes qui émettent en même temps.

- Les protocoles **taking-turns** où chaque hôte peut émettre à un omment déterminé et pendant un temps déterminé.

#### Protocole de partionnement du canal

Ces protocoles divisent la ligne pour que chaque noeud puisse en avoir une partie. Cette division est soite une **TDM (Time Division Multiplexing)**, c'est-à-dire une division où chaque noeud a un espace temps qui lui est réservé.

L'autre division est la division par fréquence ou **FDM (Frequency Division Multiplexing)** qui consiste à diviser le canal en fréquence. Chaque noeud peut alors communiquer continuellement sur sa fréquence sur la ligne.

Cette méthode a l'avantage d'être équitable et de n'avoir aucune collision. Cependant lorsqu'un noeud n'émet rien, la bande passante est perdue et transmet avec un débit moyen de R/N bps (c'est-à-dire le débit de la ligne divisé par le nombre de noeuds).

#### Protocole à accès aléatoire

Les différents noeuds de la ligne vont "apprendre la politesse", c'est-à-dire qu'un noeud vas donner à tout le monde l'occasion de s'exprimer, ne vas pas parler sans y avoir été invité, va éviter de monopoliser la conversation, demander avant de faire quelque chose et ne pas interrompre quelqu'un qui parle.

Un exemple de tel mécanisme est celui mis en place par le satellite Slotted-ALOHA permettant de connecter plusieurs îles entre-elles. Pour empécher les collisions, le satellite envois un signal pour indiquer le début d'un "slot", c'est-à-dire un espace temps où il est permit d'émettre, cet espace temps à une durée déterminée.

Cela signifie donc que pour chaque trame (slot), elle peut soit être valide, vide ou en collision. Cela permet donc de limiter le nombre de collision plus facilement.

Un autre exemple de mécanisme comme celui-ci est le CSMA/CD (on y reviendra plus tard) qui va écouter avant de transmettre, donc si quelqu'un transmet déjà, on va attendre qu'iel ait fini.

#### Protocole taking-turns

Un noeud est désigné comme le noeud maître, celui-ci offre à chacun la possibilité de dialoguer en offrant un certain slot de temps à tout le monde.

Si un noeud n'a rien n'a transmettre on passe au suivant. Cela permet ainsi de disposer de toute la bande passante pour faire la transmission.

Un exemple d'implémentation d'un tel système est d'utiliser un **jeton**, le noeud maître va émettre un jeton, et l'envois à un noeud. Si un noeud n'a rien à transmettre il le réenvois, sinon il envois ses données suivit du jeton. Le jeton fonctionne ainsi un peu comme un baton de parole.

Si ce baton de parole est perdu, le noeud maître peut alors le réenvoyer.

*Voici une représentation d'un réseau broadcast de 3 noeuds, un noeud maître et un jeton :*

![](https://hatierparents.fr/sites/default/files/batondeparole.jpg)
