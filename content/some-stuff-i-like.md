+++
title = "Some shit I've discovered and like recently"
date = 2024-05-14T09:15:00+02:00
draft = false

[taxonomies]
tags = [ "links", "music", "recommendations", "blogs", "english", "short" ]
+++

- [Nemo](https://www.youtube.com/@nemothings), singer and winner of Eurovision 2024. I really like their music and I find them inspiring and very uplifting.
- [Maia Arson Crimew's blog](https://maia.crimew.gay/), the personal blog of [Maia](https://en.wikipedia.org/wiki/maia_arson_crimew) a cool af hacker. I especially enjoy reading the stalkerware hacking articles.

<!-- more -->
