+++
title = "Mapping Our Madness"
date = 2022-04-22T09:50:00+02:00
[taxonomies]
tags = ["english", "mental_health", "anarchy", "praxis"]
[extra]
image = "mapping.jpg"
+++

This is a pure text version of the PDF "Mapping Our Madness" by "Momo" on The Icarus Project (fireweed collective).<!-- more --> You can find the original [here](http://nycicarus.org/images/MappingOurMadness.pdf) and the Crisis Toolkit [here](https://fireweedcollective.org/crisis-toolkit/). The original is beautiful but doesn't work well if you have a disability or if you don't like PDFs.

A workbook for navigating crisis, extreme states, or just foul moods.

## Intro
**Hey there**. This is a workbook zine I made for the purpose of helping folks map, plan and navigate crisis, madness or just foul moods. The idea basically is that this is a toolbox to keep near you, so in the event that you're in a crisis situation or experiencing some intense emotions, you'll have some of the tools that you (or the folks around you) need to help you take care of yourself. I know for me in the moment, when I'm experiencing intense emotional states it's hard forme to think about what I need, much less be able to communicate that to the folks around me. The point of this workbook is to help you make a plan, in case things get real; to help facilitate that communication of needs, and also hopefully prevent those crisis situations from happening in the first place. 

I know this format doesn't work for everyone and realize that maybe notall of the pages in here are relevant to you. I encourage anyone who uses this to alter it in any way that makes sense to them, or in the very least, I hope it inspires them to make something else great for themselves.

**Some notes about the motivations for making this workbook**: The ways we experience our mental health, the ways we experience ourselves and our realities, are inextricably entangled with the world around us. In other words, it doesn't make sense for us to divorce ourselves from what surrounds us (the systems that connect us, or pit us against each other: that tell us how to be, or coerce us into submission), to isolate the ways we feel and then label ourselves as “sick.” It’s difficult to understand the ways we experience our mental health without also acknowledging the ways we are affected by the forces of systemic oppression (racism, sexism, homophobia, classism, etc.) that are imposed on us. Put simply, perhaps we are not so “crazy” for being affected by a world that is so fucked up.

It’s important to challenge the idea that these differing experiences are “wrong” and that we are “sick” or “broken” for having them. That we are something that needs correcting and must undergo treatment in order to be “fixed” Rather, these experiences can be seen as gifts. Like superpowers to be held close to our hearts, to understand and to know their limits so that we may use them as tools to liberate ourselves and to build better worlds.

This is an effort in hopes of imagining a world where the different ways our minds and bodies may function are recognized and supported as valid experiences. Where we have the time and the space to look into ourselves and uncover our identities without fear of being dismissed or denied for what we come up with. This is an effort in hopes of supporting a radical re-envisioning of mutual support within our communities. 

*This zine was made with love by one weirdo who sometimes feels too much, Jor all the great minds she has (and hasn't) met. For tinkerers and poets, for brains with engines that are prone to over-heating, for those who hear and see what others can't, for dimly lit and sunken hearts, and for those whose light can become so bright, it’s blinding. You are not alone. This is for you; I made this for you.*

> You're stronger than they can ever imagine, ❤️ Momo 

## Some suggestions
You know yourself best, but I have few suggestions for filling this out. You can take them or leave them:

* **Fill it out when you're in a good mood** (whatever that means for you.) I know that sometimes, when you're feeling good, it's hard to, think about the possibility of crisis or feeling bad again an it's easier to just stay feeling good than it is to plan ahead in case you don't feel so good sometime in the future.
* **Leave space for more**. There's a lot to think about it here and you'll be constantly growing and learning more about yourself and finding more tools that help you. I'll leave some blank pages in the back too so you can make your own pages.
* **Some of these things can be hard to think about or write down**. And maybe some of these things you haven't thought much about before. **Try filling some of this out with a close friend**. Put your brains together to envision mutual support! This doesn't mean that it's a bad idea to spend some time working on this alone.

*(On some pages, there may be more speeific suggestions in parentheses but those are just suggestions or prompts to get you thinking.)*

## The questions
> On the original PDF, all those questions had blank lines to fill them (one quesiton per page). I recommend you writing down those questions and answer them one by one.

* **Daily Things to Take Care of Myself** (this can be things like exercising daily, to tinctures, herbal supplements, vitamins, and medications that you may want to take regularly) 
* **Signs of Oncoming Crisis or Extreme State** (Sometimes these things look like not eating or sleeping and becoming less social. It could also be the total opposite. It could be things like feeling as if you're feeling way too much too handle, or it could be that you feel numb. You could also include ways to recognize you may be triggered. Again, You know yourself best)
* **Triggers** (A Trigger is something that can set off a flashback, or re-living of past trauma. It could be anything from sights and smells, to something someone says or how they act, to a place, ect. If you don't identify as a survivor of trauma, this could also double for a page of things that may set off intense emotions or states.)
* **My Immediate Needs** (What are your immediate needs in a crisis situation, extreme state, or when you're feeling triggered? This could be things like moving to a safer space, being away from people, sleep, food, etc.)
* **What can my friends do when I'm experiencing crisis, in an extreme state, or feeling triggered?** (This could also be a prompt that you can write up elsewhere and send to your friends. Make sure you have their consent first! You could also include some of the recognizable signs of oncoming crisis from page 6 or what you may look like when you're triggered or in an extreme state) 
* **People I'd can Talk to, or would like to Be Around** (This can include names and contact information. You may wan to check in with these people before filling this out, as well as regularly, to make sure they are able and willing to support you)
* **Spaces I feel Safe/Safer in** (You can include addresses, maps, or directions you think it will help you or friends get you to one of these places) 
* **Things that make me Laugh** (this could be anything! Books, movies, comics, pictures, people, silly Internet videos, etc.) 
* **Easy Food a Friend or I could Make** (This can include recipes and favorite foods you're more likely to'eat if you're having a hard time eating. It can also include dietary preferences, restrictions or allergies) 
* **Here are some Nouns I'd like to Avoid** (This could be things you do to deal with pain that you deem unhealthy, as well as people and places you may not want to be around. These things could possibly look like: substance abuse, harmful habits, self mutilation, foods that make you feel gross, music that makes you sad, etc.) 
* **Activities that Help Me Feel Better** (This could be anything! Writing, painting, riding bikes, screaming, dancing making music, cooking, cuddling, etc.) 
* **Note to Self** (This is a page to write a note to yourself when you're feeling alright. It can be about how great you are, the things that you care about and that care about you, or a reminder that you won't be feeling the way you are forever. Send yourself some love!) 

## Resources
* [The Icarus Project](https://theicarusproject.net) - Hella resources (zine, forums, etc.) here: “The Icarus Project is a radical mental health support network, online community, and alternative media project by and for people struggling with extreme emotional distress that often gets labeled as mental illness.” 
* [Learning to Love You More](https://learningtoloveyoumore.net) - “Assignment“ or activities to keep you busy and loving yourself.
* [SuperBetter](https://superbetter.com) - Created by game designers to build  personal resillence (the ability to stay strong, optimistic, and curious in the face of difficult challenges) by harnessing the power of positive emotion and social connection.
* [Warmline](https://warmline.org) - A warmline is an alternative to a crisis line, peer-run by people in recovery themselves.
* [Hearing Voices Network](https://hearing-voices.org) - The Hearing Voices Networks offers resources (information and forums) for folks who experience voice hearing, visions, tactile sensations and other sensory experiences. HVN does not approach these experiences from a medical or pathological framework.
* [Mad in America](https://madinarmerica.com) - A journalistic website “designed to Serve as a resource and a community for those interested in rethinking psychiatric care in the United States and abroad,” They have good resources for those seeking alternative mental health practitioners or for information and studies on psychopharmaceuticals.

## Envelope
> On the final page of the zine there's a rectangle with the following text on it:

**Envelope**: Glue an envelope here to keep things that make you smile. Letters from friends, pictures, comics, cds, ect. 
