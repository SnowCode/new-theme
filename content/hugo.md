+++
title = "Creating a Hugo blog from scratch"
date = 2021-04-29T21:55:45+02:00
[taxonomies]
tags = ["english", "self-hosted"]
+++
As you may have seen this blog have been made from scratch, there are some important advantages to do a blog using Hugo from scratch:<!-- more -->

* The blog is super-fast and super-light
* It's fun
* It's minimalist
* You get exactly what you want, nothing more, nothing less

So this is how I done it:

## Step 1: Installing Hugo
I am not going to cover all the installation options of Hugo, so just follow the method you want from [this page](https://gohugo.io/getting-started/installing).

## Step 2: Creating the theme skeleton
Now we need to create an empty blog with an empty theme:

```bash
hugo new site quickstart
cd quickstart/
hugo new theme custom
cd themes/custom/
```

You are now in the root directory of your theme, which is for now empty, but we're going to fill it soon.

## Step 3: Edit the _defaults layouts
That directory (`layouts/_defaults`) contains the basic layouts (that will contains other layouts inside called "partials"). So let's go into that directory:

```bash
cd layouts/_defaults
```

The `baseof.html` layout contains the root layout, every pages will depend on that one.

The `list.html` layout is used for list of posts, for now just add the following content to it: (it's basically just a link to a partial we'll create later)

```html
{{ define "main" }}
{{- partial "posts.html" . -}}
{{ end }}
```

The `single.html` layout is used for displaying articles. Same thing as for the precedent, we'll link it to a partial. That all the meaningful files will be in the "partials" directory.

```html
{{ define "main" }}
{{- partial "post.html" . -}}
{{ end }}
```

If you want you can now create a nice error message for the `404.html` file.

## Step 4: Edit the partials
The partials are small snippets of code that are integrated in `_default` layouts. Let's get into that dir:

```bash
cd ../partials
```

> Note: All the partials with the exception of `post.html`, `posts.html`. Doesn't actually require templating...

Let's treat it file by file:

The `footer.html` file contains the following:

```html
<footer id="footer">
    {{ .Site.Params.copyright }}
</footer>
```

The `header.html` file contains the code that will be displayed on the top of the website, you can create a menu there if you want, I personally just done this:

```html
<header id="banner">
    <h2><a href="{{ .Site.BaseURL }}">{{ .Site.Title }}</a></h2>
</header>
```

The `head.html` file contains the meta-data of the website. I personally just have the following:

```html
<head>
    <link rel="stylesheet" href="/css/style.css">
</head>
```

## Step 5: New partials
Now we'll create 2 new partials, `post.html` and `posts.html`.

Let's start with the content of `post.html`. This is mine:

```html
<article>
    <header id="post-header">
        <h1>{{ .Title }}</h1>
            <div>
            {{- if isset .Params "date" -}}
                {{ if eq .Lastmod .Date }}
                <time>{{ .Date.Format "January 2, 2006" }}</time>
                {{ else }}
                Updated <time>{{ .Lastmod.Format "January 2, 2006" }}</time>
                {{ end }}
            {{- end -}}
            </div>
    </header>
    {{- .Content -}}
</article>
```

If you need more metadata, you can simply check out other's themes or visit [the documentation](https://gohugo.io/templates/).

Then this is my `posts.html` theme:

```html
<h3>Posts</h3>
<ul id="posts">
{{- range where site.RegularPages "Type" "in" site.Params.mainSections }}
    <li>
      <span class="date">{{ .Date.Format "Jan 2, 2006" }}</span>
      <a class="title" href="{{ .Permalink }}">{{ .Title }}</a>
    </li>
{{- end }}
</ul>
```

We now have an operational basic template, we can now attack the styling...

## Step 7: Set your index page
Now you have the set your home page, simply add the following snippet into `layouts/index.html`

```html
{{ define "main" }}
{{- partial "posts.html" . -}}
{{ end }}
```

## Step 6: Styling
First we'll need to run the instance so let's create a fake post and run it:

```bash
cd ../../../../ # Return to the root of your blog
hugo new posts/hello.md
echo "Hello World" >> content/posts/hello.md
```

Then we have to set the theme to "custom" in the settings and run the server:

```bash
echo theme = '"custom"' >> config.toml
hugo server -D
```

Now go on the url and open the inspector, and open "Style Editor". You can now modify things on the page without restarting the server.

> **NEVER RELOAD THE PAGE** if you do so, all your changes will be lost!!!

If you're not familiar with css, simply understand the syntax:

```css
body {
  font-family: Karla;
}

<element> {
  <variable>: <value>;
}
```

Then you can simply use the inspector to get CSS code from other websites for instance. 

Ideas of variables to change:

* `margin-top`, `margin-bottom`, `margin-left` and `margin-right`. Warning, those settings are sure important but they may not fit for all device's sizes. 
* `font-family` and `font-size` to get better fonts than the default one.
* `background-color` and `color` to implement dark theme.
* `text-align` to center the titles for instance.

## Step 7: More styling
Now let's make our styles more versatile for instance, let's make those margins "mobile-friendly". Simply do something along those lines:

```css
body {
  <settings to apply for most devices> 
}

@media (max-width: 1000px) {
  body {
    <settings to apply for phones>
  }
}
```

For all your other needs, simply look on the web how to do it, or copy the code from somebody else.

Once you done all of this, you can copy all the css code into `themes/custom/static/css/style.css`.

## Step 8: Write posts
To write posts, simply run:

```bash
hugo new posts/<name in url>.md
nano content/posts/<name in url>.md
```

Then fill that file with your post, written in markdown.

## Step 9: Publish
You can now create a publishing script, so you just have to run it to publish all your posts to your server:

```bash
#!/bin/bash
hugo -D
rsync -r public/* <username>@<ip of your server>:<path to your web server directory>
```

## Conclusion
So that's how I created this blog. Note the stylesheet still evolves over time depending on my inspiration :P.

