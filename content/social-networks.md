+++
title = "Privacy respecting and secure social networks and chats"
date = 2024-07-01T07:05:00+02:00
draft = false

[taxonomies]
tags = [ "english", "tech", "security", "privacy", "chat", "fediverse", "social networks" ]
+++
I few days ago I started using XMPP again and it gave me the idea of using more privacy respecting social networks and chats. So I decided to write an article aobut it to share my personal recommendations about them.
<!-- more -->

## Bye bye Reddit, hello Raddle
The only mainstream social network I was on was Reddit, but with time I noticed the amount of reposts on most subs (especially the big ones) and started to get sick of it. 

Also, I noticed that being some time on Reddit often make me feel "empty" and sometimes even angry seeing ableist, queerphobic or generally very closed-minded behavior in very mainstream subreddits.

When I had a discussion on Reddit about basically any, even slightly, sensitive topic, the discussion had absolutely no empathy with most discussions are based on judgements and ridiculizing others.

So after one of those discussion that made me feel especially bad, I just decided to nuke my Reddit account entirely and it was just an amazing decision.

After being on Reddit, I found a nice place on the forum [Raddle](https://raddle.me), it works kind of the same way as Reddit except subreddits are called "forums" and the downvote button doesn't mean "fuck you, I disagree" but is simply a way to mark the content as spam or breaking the ToS (which is also what Reddit was suposed to be like originally).

Actually, before being on Raddle I first got on [Lemmy](https://lemmy.ml) which is also a Reddit alternative (but also a lot more popular). However I didn't really like it because I was often feeling uncomfortable and the instance was predominantly run by Marxists-Leninists.

Meanwhile Raddle is an instance run by anarchists, which is more aligned with my values and thus make me more comfortable.

## The fediverse
I never really used Twitter, Instagram, TikTok or other similar apps. The only social media I've used other than Reddit or Discord is the fediverse.

The fediverse is basically a network of social networks. There is many different networks available there, the most popular ones being Mastodon (a twitter-like platform), Pleroma (also rather twitter-like but with more options), PixelFed (an instagram alternative) and Peertube (a YouTube alternative).

What's pretty cool about the fediverse is the ability to communicate and follow content from different networks. So from mastodon, you can follow content from pixelfed and peertube for instance. It's kinda like you would be able to talk and interact with people on Instagram and YouTube while being on Twitter.

I've been on the fediverse before, I hosted my own Pleroma instance. However it's fair to mention that it remains a social media, and like social media in general, even though it tries its best to not use shitty tactics to gain your attention and stuff it remains pretty addictive.

This is one of the reason why I stopped using it, the second reason is that I had my own instance and one day it broke so I lost everything I had on it for an unknown reason and I never got the strength to debug the thing or re-install it.

Now I actually came back on it, I decided to just go for an existing instance here, I used the [ni.hil.ist](https://ni.hil.ist) Mastodon instance with the [Moshidon](https://f-droid.org/packages/org.joinmastodon.android.moshinda/) Android app (there is a ton of others but I just picked this one randomly because I found it cool).

The reason I'm coming back on it is just because it's kind of fun, I'm kind of bored and there are people I know and love there.

## Chatting 
On a technical side I often was amazed by chats and experimented with a ton of alternative open-source, privacy-respecting, secure chats such as Nostr, XMPP, Matrix, Briar, Signal, Tox, Rocket Chat, Mattermost, Session, Zulip, SimpleX, Mumble, enhanced IRC and many others.

The problem I would often find myself with is with the lack of connection with people there. I was just using the chat for the fun of using the chat but was really struggling to find people with interests more diverse than the technology itself there.

Also I wasn't exactly very social with people, even by text online and I wanted to make my experience stick with the chat thing so much that I would actually put myself too much pressure and end up giving up because of it.

One other thing which made me sad was to have to continue using some other, shitty chat applications such as Whatsapp or Discord because the people there would not switch to anything else. I think the reason why it makes me sad is because I hate being stuck on a proprietary platform (especially since most are very privacy invasive) and makes me feel "constraint".

### Signal, good-enough centralized communication
However there is one platform which kind of get out of the mass in this: [Signal](https://signal.org/). Signal, despite controversies has the merit to be pretty open about their actions and goals, are independant and have a strong end-to-end encryption and even more privacy and security options available.

Unlike apps like Whatsapp which, while also having end-to-end encryption shares a ton of data aobut you to Facebook (Meta). Meta also has a long history of being very very cooperative to data-requests from US government, while Signal tries to say as little as possible. They said publically they had a subpena [on their website](https://signal.org/bigbrother/cd-california-grand-jury/) and according to them, this is the only data they sent to the government:

![A screenshot of a table containing a censored phone number on the left and the account creation date + last connection in UNIX time on the right](/signal-subpeona.png)

It's good however to remember that while the Signal application is open-source, the company seems rather trustworthy (or at least a lot more than others) and the encryption standard is well known, the privacy of services such as Signal continue to reside on trust because nobody, except them, can tell what is really going on on the server and what data is recorded. Also I am personally uncomfortable with Signal requiring a phone number to create an account or being heavily centralized.

This is why you might want to consider some alternative platform, especially if you don't want to trust Signal or have more specific privacy needs.

If you want to secure your usage of Signal, especially as an activist, I invite you to check out [this zine](https://www.sproutdistro.com/catalog/zines/security/turn-off-your-phone/) and [this zine](https://www.sproutdistro.com/catalog/zines/security/signal-fails/) and/or even [this one](https://www.sproutdistro.com/catalog/zines/security/mobile-phone-activists/). You can get more info about security in general and other stuff from that website. If you want to learn even more, check out [this website](https://opsec.riotmedicine.net/).

### XMPP and decentralized communication
Most of the criticisms I talked about with Signal come from the simple fact that Signal is centralized so the servers are only managed by the Signal company and nobody else. You can't go on any other server to use Signal.

But it doesn't have to be that way, you can also use decentralized/federated chatting services. The most popular one is [Matrix](https://matrix.org), however I don't recommend it.

Matrix is also end-to-end encrypted and decentralized, however it's also very heavy, technically complicated, and most people uses the main server matrix.org which makes it almost like a centralized server.

I tried several times to install Matrix for myself but I had a lot of issues based on how memory-consuming the server is (especially for small server like raspberry pi or very low cost VPS) and a load of technical issues making federation not working.

Another option, while less popular, has been more tested with time is XMPP. XMPP is an old but still very efficient protocol standard for messaging applications. It was even implemented by Whatsapp or Telegram in the early days, making it inter-operable with others servers.

XMPP has the advantage of being very lightweight to run on a server, but also made pretty easy by the deployment of [Snikket](https://snikket.org/) which is a project that aims to make it as easy as possible to setup your own XMPP instance.

XMPP is also end-to-end encrypted (using OMEMO), supports basically everything you need (reactions, attachments, calls, encrypted group chats, public chats, encrypted personal chats, etc). The main problem with XMPP is that most clients suffer from some design issues compared to things like Matrix or Signal. 

My recommendation would be to use an application like [Conversations](https://conversations.im/) or [Monocle chat](https://f-droid.org/en/packages/de.monocles.chat/) on Android.

You can use [Gajim](https://gajim.org/) or [Dino](https://dino.im/) if you're on a computer. 

You can find more options of clients [on the official list of XMPP clients](https://xmpp.org/software).

### Briar, peer-to-peer communication
Briar is fasinating to me. It's a completely peer-to-peer chatting application working over TOR. It doesn't have many features, last time I checked it didn't support calls or even images but it's probably one of the most secure options for a messaging app you can possibly get.

The app can work offline using local Wifi or bluetooth. There is no server at all all the data is encrypted and the entire system has been tested, audited and is generally very well done.

By default your local account is protected by a password which encrypts all your data. Which means that if the app is closed, it's impossible to retrieve any data without the password.

I'm not going to go over all the great technical aspects of it but if you really need a very secure platform, go for it. Just remember that sometimes it's most often better to just take things offline than use online communciation.

The main drawbacks about Briar is the lack of features and the fact that if the app is closed, you won't be able to receive any messages (because there is no in-between server to keep track of the messages).

## Conclusion
So in a nutshell, my favorite chat platforms are Signal, XMPP (using Snikket, Monocle and Gajim), Briar, Raddle and the fediverse.

I hope you'll have fun experimenting and connecting with people (whatever the means :p) and that you've learned something or found something intersting in this. 

Bye bye, see you next time people.

Psst, while I am at it, feel free to contact me on any of those platforms :

- Signal : `snowcode.42`
- Mastodon : `@snowcode@ni.hil.ist`
- Raddle : `u/snowcode`
- XMPP : `@snowcode@chat.snowcode.ovh`
- IRL : Did you really think I would give you my address?