+++
title = "How to create a (Vanilla) minecraft server"
date = 2021-07-02
[taxonomies]
tags = ["english", "linux", "minecraft"]
+++
Just yet again a guide to make a vanilla minecraft server<!-- more -->

## Step 1: Download the server.jar file
Download the sevrer.jar file from [this website](https://mcversions.net/)

Then place the file into an empty directory:

```
$ mkdir minecraft-server/
$ cd minecraft-server/
$ mv ~/Downloads/server.jar .
```

## Step 2: Accept eula
Then you have to run the file:

```
$ java -Xmx1024M -Xms1024M -jar server.jar nogui
```

Then, replace "false" by "true" in eula.txt

```
$ sed -i "s/false/true/g" eula.txt
```

## Step 3: Change the properties
Re-run the server.jar file. Then type /stop.

```
$ java -Xmx1024M -Xms1024M -jar server.jar nogui
```

Now you can edit the server.properties file. For instance, you can change "online-mode" to false if you want to allow cracked players to join the server.

```
$ nano server.properties
```

## Step 4: Run the server and change some last stuff
Now, we can finally run the server:

```
$ java -Xmx1024M -Xms1024M -jar server.jar nogui
```

Now, into that console and change some gamerules. Those are my personal favorites:

```
/whitelist on
/whitelist add <player names>
/op <your player name>
/gamerule mobGriefing false
/gamerule doWeatherCycle false
```

## Step 5: Making the server run in the background
We will now create a systemd service:

```
# nano /etc/systemd/system/minecraft.service
```

Paste the following code:

```
[Unit]
Description=Minecraft Vanilla Server
After=network.target

[Service]
Restart=always
RestartSec=1
WorkingDirectory=<path to minecraft-server>
ExecStart=java -Xmx1024M -Xms1024M -jar server.jar nogui

[Install]
WantedBy=multi-user.target
```

Finally, you can run the service:

```
# systemctl start minecraft
# systemctl enable minecraft
```

## Conclusion
You now have a working Vanilla Miencraft Server. If you want to stop the server join the game and run /stop. Or run:

```
# systemctl stop minecraft
```

Also, if you want to make your server accessible world wide, you can open the port 25565. Your IP will be:

* If you're running the game locally: localhost
* If you're in the same network: <your hostname, find it by running hostname>
* If you're not in the same network: <the public ipv4 address of the host>

