+++
title = "How to setup Inspircd with SSL on Debian."
date = 2021-11-06T00:28:47+01:00
[taxonomies]
tags = ["english", "self-hosted"]
+++
This is a quick guide to setup Inspircd with SSL on Debian. I've had some troubles doing this, I don't understnad why, maybe it was because I did it at 4AM?<!-- more -->

The reason I use inspircd for this is mainly because Inspircd is a pretty popular irc server daemon.

## Step 1: Installing dependencies
We're going to use inspircd from source, so we have to install some inspircd building dependencies (including the ones for SSL)

```bash
sudo apt install wget make build-essential letsencrypt gnutls-bin
```

## Step 2: Downloading inspircd
Now let's download the source code:

```bash
wget https://github.com/inspircd/inspircd/archive/refs/tags/v3.11.0.tar.gz
tar xvf v3.11.0.tar.gz
cd inspircd-3.11.0/
```

## Step 3: Building the source code
Now that you are in the source code, let's configure and build. It will also enable the `ssl_gnutls` that is necessary for configuring SSL.

```bash
./configure --enable-extra ssl_gnutls
./configure
make install
```

## Step 4: Edit the configuration
Let's copy the config file and edit it:

```bash
cd run/conf/
cp examples/inspircd.conf.example inspircd
nano inspircd.conf
```

Here I let you go through the file and edit values to your wishes. I'm only going to tell you which ones you MUST change to have a working server.

First, you need to add the following part that will import the SSL certificates and files:

```xml
<sslprofile name="Clients"
            provider="gnutls"
            cafile=""
            certfile="cert.pem"
            crlfile=""
            dhfile="dhparams.pem"
            hash="sha256"
            keyfile="key.pem"
            mindhbits="1024"
            outrecsize="2048"
            priority="SECURE192"
            requestclientcert="yes"
            strictpriority="no">
```

Then, uncomment the following line:

```xml
#<include file="examples/modules.conf.example">
```

Then save, exist and edit the modules file

```bash
nano examples/modules.conf.example
```

Here, you can enable the modules you want, but you must uncomment this line:

```xml
#<module name="ssl_gnutls">
```

Finally, save and exit and go to the next step.

## Step 5: Generate the certificate and move it
Let's generate the certificate using certbot. To do so you have to disable nginx (or apache, or httpd depending on what you use):

```bash
sudo systemctl stop nginx
sudo certbot certonly --standalone -d YOUR.DOMAIN.HERE
sudo systemctl start nginx
```

Then move those files to the inspircd conf directory:

```bash
sudo cp /etc/letsencrypt/live/YOUR.DOMAIN.HERE/fullchain.pem cert.pem
sudo cp /etc/letsencrypt/live/YOUR.DOMAIN.HERE/privkey.pem key.pem
certtool --generate-dh-params --sec-param normal --outfile dhparams.pem
```

Then you have to set the permissions to the current user, otherwise the ownership of the files will be to `root` and the current user won't be able to read it.

```bash
sudo chown -R $USER:$USER .
```

## Step 6: Run inspircd 
Now you should be good. Just run inspircd using the following commands:

```bash
cd ..
./inspircd start
```

If you have to debug things, run those commands:

```bash
./inspircd stop
./inspircd start --nofork --debug
```

If you can connect in plaintext connection (6667 port), but not SSL connection (6697 port), then run the SSL test script (inspircd must be already running)

```bash
./bin/inspircd-testssl 127.0.0.1 6697
```

## Additional notes
The reasons this took me so much time may be because gnutls was outdated and I think I didn't use the right files. I don't exactly remember, I done a lot of troubleshooting at 4AM lol. 

If you have any problem with this, please tell me so I can improve the guide. Be free to ask questions on [#SnowCodeBlog on Libera](https://web.libera.chat/#SnowCodeBlog) or via the official inspircd support in #inspircd on irc.inspircd.org.

I also plan to make 2 other posts, one about [KiwiIRC](https://kiwiirc.com) and another one about [ngircd](/ngircd), which is a lighter and easier alternative to inspircd.

I am also planning on making 2 other posts about IRC in general, one about Weechat and another one about what I think of IRC.

PS: If you want to configure this with TOR as well and know how to connect it via Weechat, checkout the end of [my post about ngircd](/ngircd).
