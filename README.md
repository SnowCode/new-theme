# Snow theme for Zola
This theme is a fork of [Anpu](https://github.com/zbrox/anpu-zola-theme) with a few improvements for my blog:

## Features
* Simple theme (both in design and infrastructure, no JS)
* Tags taxonomies
* Customizable menu
* Pagination

Improvements by this theme:

* Dark theme
* Different layout for the header
* Feature images and videos in post list
* Metadata of pages for embeds in social media (like Discord for instance)
* My own content (remove it for yours)

Stuff that have been removed from the original theme: 

* Categories

## Fork it
Download it and put your content on it then list the changes you want to add to the site and change CSS and HTML code. 
