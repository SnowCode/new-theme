#outline(depth: 2, indent: 2em)

= Qu'est ce que le droit ?
Ce que l\'on apelle généralement \"le droit\", s\'appelle plus
précisément le #strong[droit positif];. Le droit positif est un ensemble
de règles qui régissent les relations sociales à un moment donné, au
regard d\'un groupe de personnes donné sur un territoire déterminé. Par
exemple la loi belge actuelle à un moment donné \(maintenant et
jusqu\'au prochain changement), à l\'égard d\'un groupe de personnes
donné \(les citoyen·ne·e belges), sur un territoire déterminé \(la
Belgique).

La règle doit toujours être accompagnée d\'une sanction appliquée par la
force publique.

On parle de #strong[droit objectif] lorsque l\'on parle de la règle en
générale, le droit de tout le monde \(par exemple \"le droit du
mariage\"), et on parle de #strong[droit subjectif] lorsque l\'on parle
d\'une situation particulière, individuelle \(\"le droit que #emph[je]
me marie\").

Les droits objectifs et subjectifs sont donc deux manières de voir les
règles faisant partie du droit positif.

Pour faire un recours contre une règle de droit objectif \(par exemple
contre une nouvelle règle), il faut pouvoir se baser sur une règle de
plus haut niveau \(par exemple, la constitution).

La #strong[morale] est un ensemble de valeurs qui se décline en
principes et comportements. La morale, contrairement au droit est
personelle. #strong[L\'éthique] est l\'étude de la morale.

== Le droit public et le droit privé
<le-droit-public-et-le-droit-privé>
Il existe deux sortes de droit. Le droit dit #strong[privé] qui est
celui qui régit les rapportes entre les personnes physiques ou morales
\(entreprises, associations, organisations, etc) et le droit
#strong[public] qui régit les rapports avec l\'État \(c\'est-à-dire avec
les instituations, l\'administration, la police, etc).

Dans le droit #strong[privé] on va retrouver notament, le #strong[droit
civil] \(régit les relations entre les personnes privées), le
#strong[droit des affaires] \(régit les rapports entre les entreprises),
le #strong[droit social] \(régit les rapports employeur-employé), le
#strong[droit des assurances];.

Dans le droit #strong[civil];, il y a un coté plaignant et un coté
défenseur. C\'est par exemple le cas de conflits de famille, de contrats
ou de successions. Pour réparer un dommage, cela se fait généralement
par l\'exécution d\'une obligation ou des dommages et interêts versé à
la partie adverse.

Parmi le droit public, on va retrouver le #strong[droit consitutionnel]
qui définit les libertés fondamentales et la structure de l\'État, le
#strong[droit administratif] \(définit les droits et obligations des
administrations), le #strong[droit fiscal] \(droit relatif aux impots et
à l\'organisation financière de l\'État) et le #strong[droit pénal]
\(droit qui réprime les comportements dit comme atteinte à l\'ordre de
la société elle-même).

Dans le droit #strong[pénal] cependant, il y a un accusé et un
représentant de \"la société\", qui est le ministre public \(ou
\"parquet\"). Il s\'agit de punir les \"infractions\", tel que les
contraventions, les délits ou les crimes. Pour sanctionner les
infractions il s\'agit soit d\'une ammende versée au trésor public, ou
alors d\'une peine d\'emprisonnement \(ou dit \"réclusion\" pour les
crimes).

Il a également une sorte de droit qui tombe un peu dans les deux
catégories qui est le #strong[droit judiciaire] qui est une partie du
droit qui traite l\'organisation des tribunaux et de la procédure
judiciaire. Elle est du droit public car elle concerne une institution
publique, mais aussi du droit privé car elle concerne aussi les droits
des individus et entreprises.

= Notions de droit public
L\'État est séparé en trois pouvoir. Le pouvoir #strong[législatif] \(le
parlement) qui s\'occupe de rédiger les lois. L\'#strong[exécutif] \(le
gouvernement) met les lois en application et enfin le pouvoir
#strong[judiciaire] qui contrôle l\'application de la loi.

Chaque pouvoir est donc supposé être indépendant et exercé un contrôle
sur les autres, ainsi le gouvernement ne peut donc normalement pas
imposer une loi ou définir comment sont gérés les procès.

Le pouvoir #strong[législatif] va adopter les lois et contrôler le
pouvoir exécutif. La consition le décrit comme deux chambres, la chambre
des représentants et le sénat. La #strong[chambre des représentants]
assure l\'élaboration de la majorité des lois tandis que le
#strong[sénat] s\'occupe de l\'élaboration des lois les plus importantes
tel que la constition. Le sénat est constitué de représantants de chaque
communauté et région.

Le législatif contrôle le pouvoir exécutif avec des #strong[questions
parlementaires] qui sont des séances publiques où des parlementaires
pose des questions au ministre de l\'intérieur pour lui demander de
s\'expliquer sur les actions du gouvernement.

== Pouvoir législatif : La création d\'une loi
<pouvoir-législatif--la-création-dune-loi>
La création d\'une loi se fait en plusieurs étapes.

+ Le parti politique définit des objectifs de loi et choisis un député
  pour porter la proposition de loi
+ Le débuté dépose une proposition de loi qui sera discuté et voté à la
  #strong[commission de la chambre];. Cette commission est spécifique au
  sujet abordé \(énonomique, intérieur, etc).
+ Si la loi est votée à majorité à la commission, elle passe en
  #strong[séance plénière] avec tous les députés de la chambre. La loi
  est alors discuttée et votée.
+ Si la loi est votée à majorité à la séance pleinère, elle est alors
  signée par le loi et devient officiellement une loi. C\'est ce que
  l\'on appelle la #emph[promulgation];.
+ La loi est alors publiée au moniteur belge et est appliquée à
  tous·te·s.

La loi indique quand elle doit être appliquée. Si elle ne prévois rien,
on considère que la loi est appliquée 10 jours après sa publication au
moniteur belge.

Il est aussi pour le gouvernement \(pouvoir exécutif) de publier un
projet de loi qui suivra le même chemin que celui du député à
l\'exception qu\'il passera d\'abord par l\'avis du conseil d\'État qui
est un conseil de spécialistes qui va donner son avis sur la
proposition.

== Pouvoir exécutif
<pouvoir-exécutif>
Le rôle du pouvoir exécutif va diriger et gèrer le pays. Son rôle est
donc d\'appliquer les lois de manière concrète en leur allouant du
budget et en assurant que les sanctions sont bien prises.

Le pouvoir exécutif est composé du gouvernement et du roi. Le
gouvernement est théoriquement les conseillés du roi.

== Pouvoir judicaire : Contrôle de l\'application de la loi
<pouvoir-judicaire--contrôle-de-lapplication-de-la-loi>
Le rôle des cours et tribunaux consiste à contrôler l\'application de la
loi, régler les litiges mais également de contrôler que les décisions du
gouvernement sont conforme à la loi.

Il va également contrôler les actions du pouvoir législatif pour
s\'assurer que les lois ne causes pas de dommages \(par exemple une loi
rendant obligatoire un vaccin qui s\'avèrerai être dangereux).

Il y a beaucoup de cours et de tribunaux différents pour traiter
différents sujets \(famille, enfance, pénal→assises, entreprise,
travail, police, etc) afin de rendre le contrôle de la loi plus rapide
et efficace.

== Structure de l\'État et des niveaux de pouvoir en Belgique
<structure-de-létat-et-des-niveaux-de-pouvoir-en-belgique>
Le pouvoir en Belgique en plus d\'être divisé en trois est également
divisé en regions \(wallone, bruxelles et flamande) et communautés
\(flamande, germanophone, wallonie-bruxelles).

Les #strong[régions] s\'occupent de tout ce qui est en lien avec
l\'économie, l\'argiculture, l\'urbanisme, transport, énergie,
l\'emploi, etc.

Les #strong[commmunautés] s\'occupent de tout ce qui est en lien avec la
culture, l\'enseignemnet, les politiques de santé, l\'aide au personnes,
les langues.

Les pouvoirs sont ensuite divisés par sous-régions au niveau provincial,
puis au niveau communal.

#image("../pouvoirs-en-belgique.png")

Les institutions #strong[fédérales] tel que le parlement, le roi ou le
gouvernement fédéral s\'occupe donc des choses communes à tout le pays
tel que l\'armée, ou des politiques générales.

= Sources du droit positif belge
Il y a plusieurs sources pour le droit civil tel que le droit
#strong[international];, la loi au sens large, la coutume, la doctrine
et la #strong[jurisprudence];.

Le droit #strong[international public] est l\'ensemble de règles de
droit qui régissent les rapports entre les États et les organisations
internationales. Elle s\'occupe nottament de définir ce qui constitute
un État et sa souvraineté, les compétences de tout État, les relations
internationales et la coopératoin entre les États \(droit international
de la mer, droit humains, etc).

Le droit #strong[international privé] régit les relations de droit privé
présentant un élément externe. Par exemple un contrat dont les parties
sont étalies dans des États différents, ou une faillite d\'un groupe
d\'entreprise international, ou encore la sucesssions des biens à
l\'étranger. Ce droit détermine donc la juridiction compétente pour
trancher le litige \(conflit de juridiction) et la loi à appliquer pour
trancher le litige \(conflit de loi).

== Droit européen
<droit-européen>
La différence principale entre le droit internationale et le droit
#strong[européen] est que l\'union européenne est plus intégrée et plus
uniformisée. Il se compose principalement de traités, règlements et
directives.

Les #strong[traités européens] définissent les objectifs poursuivis par
l\'UE, les règles de fonctionnement des institutions, les processus
décisionnels, les relations entre l\'UE et ses états membres, etc. Ces
tratiés sont négociés et adopés par l\'ensemble des états membres avant
d\'être ratifier par leurs parlements.

Les #strong[règlements] sont des actes législatifs qui s\'appliquent dès
leur entrée en vigueur de manière automatique et uniforme dans tous les
pays de l\'UE sans devoir être transposés dans la législation nationale.
Ils sont donc obligatoires dans tous les pays de l\'UE. C\'est notament
le cas de la loi sur la protection des données privées \(RGPD).

Les #strong[directives] instaurent une obligation de résultats mais sont
plus permissifs pour les États que les règlements. Chaque pays doit
adopter des mesures dans leur législation nationale afin d\'arriver aux
objectifs de la directive. Les autorités nationales doivent ainsi
communiquer ces mesures à la commission européenne.

Les directives doivent être tranposé dans les législations nationales
dans un délai fixé lors de leur adoption, qui est généralement 2 ans. Si
un pays ne transpose pas la directive la Commission européenne peut
engager une procédure d\'infraction.

== Organisation de la loi
<organisation-de-la-loi>
Généralement le droit international prime sur le droit national. A
l\'exception de la constitution qui prime tout de même quoi qu\'il
arrive. Il arrive donc sur des règles qui impactent les citoyens qu\'il
fasse valoir son droit international ou européen au delà de celui
national car il peut être plus protecteur et prime sur le droit
national.

Généralement la #strong[constitution] est un texte fondateur
relativement simple et qui ne change pas beaucoup. Cependant en
Belgique, la constitution est un texte plus complexe qui est
régulièrement réformée.

Dans ce qui dérive de la constitution, on trouve la #strong[loi
fédérale] qui consiste de lois adoptées par la chambre de représentats
et/ou le sénat.

Les #strong[décrets] sont du même principes que les lois fédérales mais
au niveaux des régions et des communautés seulement.

Les #strong[ordonnances] sont très similaires au décrets mais pour la
région de Bruxelles-capitale avec la différence que les ordonnances ont
moins de pouvoir que les décrets.

Les décrets et les loi fédérales sont au même niveau d\'importance. Si
il y a un conflit entre une loi et un décret, ce sera la règle la plus
récente qui prime. Il arrive donc parfois que des décrets modifient des
lois fédérales et priment donc sur la loi fédérale.

Enfin il y a les actes du pouvoir exécutif qui vont mettre en place les
lois du pouvoir législatif \(loi fédérale, décrets et ordonannces), ces
actes du pouvoir exécutifs sont appellés #strong[arrêtés d\'exécutions];.
Les arrétés d\'exécutions sont donc beaucoup plus concrets que les lois.
Les lois précise les concepts généraux tandis que les arrétés précise
comment les lois sont appliquées dans les détails.

Les arrétés d\'exécutions au niveau fédéral sont appellé #strong[arrétés
royaux];, au niveau des communautés ou des régions on parle
d\'#strong[arrété du gouvernement];.

== Loi vs contrat
<loi-vs-contrat>
L\'article 1.3C du code civil dit :

#quote(block: true)[
Art. 1.3. Acte juridique

L\'acte juridique est la manifestation de volonté par laquelle une ou
plusieurs personnes ont l\'intention de faire naître des effets de
droit.

Sauf disposition légale contraire, toute personne, physique ou morale,
possède la capacité de jouissance et la capacité d\'exercice.

On ne peut déroger à l\'ordre public ni aux règles impératives.

Est d\'ordre public la règle de droit qui touche aux intérêts essentiels
de l\'Etat ou de la collectivité, ou qui fixe, dans le droit privé, les
bases juridiques sur lesquelles repose la société, telles que l\'ordre
économique, moral, social ou environnemental.

Est impérative la règle de droit édictée pour la protection d\'une
partie réputée plus faible par la loi.
]

Le code civil dit donc ici que l\'on peut déroger \(par exemple avec un
contrat) à toute loi qui n\'est pas d\'ordre public ou de règles
impératives.

Il y a donc trois types de lois :

- Les lois d\'ordre public
- Les lois impératives
- Les lois supplétives

Les lois d\'#strong[ordre public] qui définit un ordre moral, politique,
économique et social de la société ou des principes essentiels de
fonctionnement de l\'État. C\'est par exemple la legislation sur les
faillites, la législation fiscale, le code pénal, la sécurité sociale,
etc.

On ne peut pas déroger aux dispositions qui relèvent de l\'ordre public.
On ne peux donc pas autoriser quelqu\'un à tuer quelqu\'un d\'autre dans
un contrat par exemple, car cela reviendrait à déroger à une loi
d\'ordre public \(le code pénal).

Il y ensuite également les lois #strong[impératives] qui ont pour but de
protéger une des parties contre des abus de l\'autre en octroyant
d\'office certains droits ou en interdisant à l\'autre partie de lui
imposer certaines obligations.

C\'est par exemple le cas des lois sur la protection du consommateur en
ajoutant une interdiction formelle de clauses dites abusives dans les
contrats. Il est donc impossible de déroger une loi impérative ou
d\'ordre public par un contrat.

Ces règles peuvent souvent être reconnus par l\'utilisation d\'une
terminologie particulière tel que #emph[#strong[nonobstant toutes
dispositions contraires];] ou #emph[#strong[tout acte juridique
contraire sera déclaré nul];];.

Enfin toute loi non impérative et qui n\'est pas d\'ordre public est
appellée #strong[loi supplétive];. Si les parties n\'ont rien prévu de
différents dans leur contrat, les règles supplétives s\'appliqueront. Ce
sont en quelques sorte des règles \"par défault\" qui peuvent être
changée explicitement dans un contrat si besoin.

Ces règles utilisent souvent des terminologies tel que
#emph[#strong[sauf disposition contraire];] ou #emph[#strong[sauf si les
statuts ou le contrat en dispose autrement];] ou #emph[#strong[sauf si
les parIes en ont convenu autrement];];.

== La coutume
<la-coutume>
La coutume est une source de règles de droit non-écrite qui est une
pratique généralisée par la population sur un certain territoire.

Il s\'agit donc de toutes les règles de droit qui se dégagent des faits
sans intervention d\'un législateur. La loi est cependant toujours
dominante sur la coutume.

Par exemple, si deux personnes sont fiancées \(qui ont donc promis de se
marier) et que lors de la planification d\'un marriage, une des deux
personnes paye tous les frais, et que l\'autre rétracte sa volonté de se
marier et annule le marriage et refuse de payer les frais \(traiteur,
salle, etc).

Si la personne qui a payé tous ces frais demande un procès, l\'autre
peut se voir demander de payer des dommages et interets car la coutume
de la fiancaille est tellement répandue que le juge considèrera qu\'il y
a eu un engagement et que l\'autre personne a rompu l\'engagement.

Et ce même si le concept de fiancaille n\'existe nulle part dans la loi.

== La jurisprudence
<la-jurisprudence>
La jurisprudence est l\'ensemble des décisions judiciaires rendues par
les cours et tribunaux.

En Belgique, chaque juge est indépendant et peut trancher un litige sans
tenir compte de la jurisprudence à l\'exception d\'un cas où un litige
se trouve en double cassation \(il a été retrouvé en cours de cassation
deux fois).

Le juge ne peut cependant pas faire n\'importe quoi non plus, il y a
tout de même une influence morale qui pousse à s\'inspirer de la
jurisprudence.

Le juge va donc décider au cas par cas ce qu\'est le comportement d\'une
personne \"prudente et raisonnable\" dans une situation donnée.

Le juge peut également définir certaines situations comme des
#strong[abus de droit];, c\'est-à-dire que quelqu\'un utilise son droit
uniquement dans le but de nuire et dépasse donc les limites de
l\'exercice normal de ce droit.

== La doctrine
<la-doctrine>
La doctrine est l\'ensemble de travaux académiques spécialisés en droit
qui peuvent par exemple expliquer des façons de traiter des litiges,
etc.

La doctrine n\'a pas force de droit mais elle va avoir une influence car
les juges vont s\'en inspirer pour leur pratique. La doctrine est
particulièrement pratique lorsque des juges sont en difficulté par
rapport à l\'application d\'une loi, la documentation de la doctrine
peut alors les aider à y voir plus clair.

La doctrine n\'a donc aucun caractère obligatoire, c\'est simplement une
aide pour le juge.

= Ethique et morale
On dit qu\'un problème est #strong[éthique] ou #strong[morale] si il met
en jeu des idéaux qui donnent sens à notre vie ou des règles auxquelles
on se sent soumis.

Il faut donc bien distinguer le droit de l\'éthique et la morale. Le
droit est basé sur la morale et pour certaines personnes la morale est
basée sur le droit. Mais il est important de comprendre que l\'éthique
et la morale sont strictement personnel pour chaque individu,
contrairement au droit.

Il y a beaucoup de définition des mots \"ethique\" et \"morale\" mais
une définition proposée dans le cours est celle-ci :

- L\'#strong[éthique] est l\'étude philosophique de la morale. C\'est
  une réflexion argumentée en vue du bien agîr.
- La #strong[morale] est l\'objet de l\'éthique et un ensemble de
  valeurs et de principes moraux \(bien, mal, juste, injuste, etc)

Il est donc important de comprendre que ce n\'est pas parce que quelque
chose est illegal qu\'il est immoral ou que par ce que quelque chose est
immoral il est illégal.

Ce n\'est pas non plus par ce que la morale est personnelle qu\'il n\'y
a pas de sanction, il peut y avoir une sanction par le désapprouvement
par une personne ou par un groupe.

== Déontologie
<déontologie>
La #strong[déontologie] est un ensemble d\'obligation refletant des
valeurs et des principes jugés fondamentaux que des personnes sont
tenues de respecter dans leur profession.

Le code de déontologie est donc adopté par les personnes afin de
protéger le public et préserver la réputation de leur profession et de
leur place dans la profession.

= Les contrats
#image("../cycle-vie-contrat.png")

Dans cette section nous allons voir le chapitre sur les contrats. Nous
allons principalement voir les bases, le cycle d\'un contrat lorsque
tout se passe comme prévu. Nous allons voir dans d\'autres articles ce
qu\'il se passe lorsqu\'un contrat est invalide ou n\'est pas respecté.

Un #strong[contrat] est un #strong[accord de volontés] entre plusieurs
personnes avec l\'intention de faire naitre des #strong[effets de droit]
\(intention de bénéficier d\'un droit et d\'assumer des obligations).

Un contrat a un cycle de vie bien défini qui reprends la négotiation,
l\'accord de volontés, la rédaction, la signature, l\'exécution, le
contrôle et la fin.

== 1. Phase de négotiation d\'un contrat
<1-phase-de-négotiation-dun-contrat>
Ce cycle de vie commence par la #strong[négociation] d\'un contrat qui
consiste à discuter les termes du contrat. Cette phase n\'engage
généralement rien, mais pas toujours. Parfois, cloturer les négotiations
prématurément peut être considéré comme une faute, par exemple si on
fait perdre beaucoup de temps à une entreprise.

Cela est généralement pourquoi avant de commencer une négotiation on la
signe pour limiter la clôture du contrat, cela s\'appelle un
#strong[NDA] \(Non Disclosure Agreement) ou accord de confidentialité en
français. Cela permet également de protéger les informations échangées
dans le contrat, notament en les étampillant les informations
considérées comme confidentielles.

Un NDA est en quelque sorte un contrat à propos du contrat.

== 2. Accord de volonté
<2-accord-de-volonté>
Comme vu précédemment l\'accord de volonté est le coeur même d\'un
contrat. Le fait que le simple accord de volonté suffise à définir un
contrat montre à quel point un contrat est une notion vague.

Il existe donc par conséquent beaucoup de types de contrats, de
catégories et de variations. Nous allons en voir quelques une ici.

Un contrat peut être #strong[unilatéral] ou #strong[bilatéral];. On dit
qu\'un contrat est #strong[unilatéral] si une seule des partie n\'a des
obligations \(exemple : donation ou caution), et on dit qu\'il est
#strong[bilatéral] si les deux parties ont des obligations \(exemple :
prestation rémunérée).

Un contrat peut également être #strong[gratuit] \(si une seule partie
n\'a des avantages) ou #strong[onéreux] \(si les deux parties reçoivent
des avantages).

Dans le cas d\'un contrat onéreux, si le gain d\'une des partie n\'est
pas certain \(exemple, \"vous recevrez 10% des gains potentiel de
l\'entreprise\"), on dit que le contrat est #strong[aléatoire];. Dans le
cas où le gain des deux parties est certain \(par exemple, \"tu me donne
1€, je te donne un chocolat\"), on dit que le contrat est
#strong[commutatif];.

Un contrat peut également être #strong[pur];, #strong[à terme] ou
#strong[sous condition];. Un contrat est dit #strong[pur] si
l\'obligation qui s\'en dégage est immédiate après la création du
contrat.

Un contrat est dit #strong[à terme] si il ne se met en oeuvre qu\'a une
certaine date.

Enfin un contrat est dit #strong[à condition] si sa mise en action
dépends d\'autre facteurs. La condition peut être soit
#strong[suspentive] \(le contrat commence lorsque la condition arrive),
ou #strong[résolutoire] \(le contrat se termine lorsque la condition
arrive).

=== Principe de consensualisme
<principe-de-consensualisme>
Il existe différentes formes, le contrat #strong[consensuel]
\(#strong[principe de consensualisme];) qui est formé par le seul accord
des volontés \(pas d\'écrit ou juste obligatoire, pas d\'exigence de
forme pour être valide).

Un exemple de tel type de contrat est la vente. Pour vendre quelque
chose il n\'y a pas besoin de remplir un formulaire ou de faire quoi que
ce soit de particulier. Cependant avoir une preuve écrite est utile pour
pouvoir prouver un #strong[vice de consentement] \(on verra cela plus
tard). La preuve écrite est donc un moyen de se protéger sur un contrat
en pouvant prouver que l\'autre partie n\'a pas respecté ses
obligations.

Le contrat #strong[formel] est une exception au principe de
consensualisme vu plus tot, ici pour être valide il doit être soumis à
une exigence de forme.

Un exemple de contrat forml est le crédit hypothécaire, le contrat de
mariage ou le crédit à la consommation qui ont tous des exigences de
forme pour être valide. Si le formalisme n\'est pas respecté, le contrat
est nul \(il est considéré comme inexistant).

Le contrat #strong[réel] qui est aussi une exception au principe de
consensualisme où ici le contrat est formé par la remise d\'une chose à
l\'autre partie, en plus de l\'échange de consentement.

Un exemple de contrat réel est le contrat de dépot ou de prêt \(on
demande à quelqu\'un de garder quelque chose ou on lui prête un objet),
dans ces contrats, si la chose n\'est pas remise physiquement, on ne
peut pas considéré que le contrat est valide. Le contrat n\'est donc
seulement valide qu\'a partir du moment où la chose a été remise.

== 3 et 4. Rédaction et signature
<3-et-4-rédaction-et-signature>
Une fois que les termes du contrat a été négocié et que les deux parties
ont indiqué être d\'accord. Le contrat peut alors être rédigé et signé
\(comme dit plus tot ce n\'est pas forcément nécessaire, par exemple
dans le cas d\'une vente).

Mais si elle est faite de manière formelle, la signature peut être de
deux formes soit manuscrite \(qui doit correspondre à celle de la carte
d\'identité), ou électronique \(scan de document, cryptographie,
biométrique, etc).

La personne qui signe un contrat est une personne physique ou un
mandataire réprésentant une personne morale \(entreprise, organisation,
ASBL, etc)

Une personne #strong[morale] est un groupement d\'individus réunis dans
un interet commun auquel la loi confère une existence et une personalité
juridique propre. Une #strong[personne] en droit est donc quelque chose
ou quelqu\'un disposant de droits et d\'obligations.

=== Signature électronique
<signature-électronique>
Il existe plusieurs sortes de signature électronique, la signature
électronique #strong[ordinaire] qui peut simplement consister à cocher
une case ou écrire son nom en bas d\'un e-mail ou d\'un document, la
signature électronique #strong[avancée] qui consiste à utiliser un
utiliser un système permettant de lier un contenu, une identité et une
signature ensemble de façon à assurer qu\'elle est authentique, non
falsifiable et non réutilisable.

Enfin il a une dernière forme de signature électronique qui est la seule
qui est assimilée en droit au même niveau qu\'une signature manuscrite.
C\'est la signature électronique #strong[qualifiée] qui utilise un
certificat de sécurité officiel. C\'est notament le cas des signatures
faite avec une carte d\'identité. Cette forme renforce ainsi
l\'authenticité de la signature par rapport à la signature électronique
avancée.

== 5. Exécution et contrôle du contrat
<5-exécution-et-contrôle-du-contrat>
A partir du moment où le contrat est signé, il est alors mis en
application et son application est contrôlée mutuellement par les deux
parties.

Ainsi chaque partie doit appliquer les termes du contrat. Il y adeux
pratiques d\'exécutions :

Le principe de la #strong[convention-loi] qui indique que chaque partie
doit exécuter ce qui a été convenu, qu\'une partie ne peut pas définir
#emph[seule] la fin d\'un contrat ou le modifier, à moins qu\'une
exception ait été explicitement définie dans le contrat lui-même.

Le deuxième principe est #strong[l\'exécution de bonne foi];. Pendant
toute la vie du contrat on considère qu\'il ne faut pas abuser de sa
position économique, ni sanctionner/mettre fin au contrat pour un
manquement sans gravité.

Ce principe va dans les deux sens, il ne faut donc pas abuser de ses
droits \(voir \"abus de droit\"), et il est également du devoir de la
victime de limiter son dommage et d\'éviter qu\'il ne s\'aggrave.

Vous allons voir dans un autre post, ce qu\'il se passe lorsqu\'une des
partie ne respecte pas ses engagements.

= Validité d'un contrat et vices de consentements
Un contrat est considéré comme valide par la loi dans le cas où il
respecte 4 conditions :

Chaque partie du contrat doit être en #strong[capacité] de contracter.
Cela signifie donc que les personnes de moins de 18 ans ne peuvent par
exemple pas contracer. Certaines personnes sous protection judiciaire
\(où une tierce personne gère les biens par exemple) peuvent également
avoir l\'interdiction légale de conclure certains contrats.

Le contrat doit avoir un #strong[objet déterminable et licite] \(doit
porter sur des choses légales) et ne doivent pas créer des déséquilibres
manifests entre les droits et obligations de chaque partie.

La #strong[cause du contrat] \(sa raison d\'être) doit être
#strong[licite];. C\'est-à-dire que le contrat ne peut pas servir à des
fins illégales.

Enfin, il doit y avoir le #strong[consentement libre et éclairé] des
deux parties du contrat \(donc pas d\'usage de violence, d\'abus de
position dominante, fraude, erreur, etc). Dans le cas contraire, on dit
qu\'il y a #strong[vice de consentement];.

== Types de vice de consentement
<types-de-vice-de-consentement>
Un premier type de vice est l\'#strong[erreur] sur une qualité
déterminante de la chose-objet du contrat autre que la valeur de la
chose \(le prix) ou la personne \(sauf si le contrat a été conclu en
considération de celle-ci).

Pour qu\'une erreur conduise à la nullité du contrat, il faut également
qu\'elle soit #strong[excusable] \(c\'est-à-dire qu\'elle aurait pu être
commise par une personne raisonnable et prudente et qui n\'est pas pris
à la légère) et #strong[commune] \(la qualité sur laquelle porte
l\'erreur doit être subtantielle par les deux parties).

Un deuxième type de vice est la #strong[lésion] qui est une
disproportion manifest entre les prestations des parties dès la
conclusion du contrat. La lésion n\'est #emph[pas] un vice de
consentement SAUF dans les cas où la loi le prévois \(par exemple pour
le partage d\'une succession ou si la personne lésée est un·e mineur·e
émancipé·e).

Un troisième type de vice est l\'#strong[abus de circonstances] qui est
un déséquilibre manifeste entre les prestationss des deux parties
\(comme la lésion) où il y a une position de faiblesse de l\'autre
partie et où cette faiblesse a eu une influence déterminante sur le
consentement de la partie victime de l\'abus.

A partir de là si sans abus, il n\'y aurait eu aucun contrat, le contrat
est considéré nul. Si sans abus, le contrat aurait été accepté mais à
d\'autres conditions, l\'autre partie doit reverser des dommages et
intérêts.

Un quatrième type de vice est la #strong[violence];, c\'est-à-dire des
menaces quant à l\'intégrité physique ou morale ou aux biens d\'une
personne. Cela consiste à impressionner la personne afin qu\'elle se
sente obliger d\'accepter le contrat. Comme pour l\'abus de
circonstance, si sans violence, le contrat n\'aurait pas été accepté,
alors le contrat est considéré nul. Et si sans violence, le contrat
aurait été accepté à d\'autres conditions, alors l\'autre partie doit
reverser des dommages et intérêts.

Enfin un ciquième et dernier type de vice est le #strong[dol];,
c\'est-à-dire des manoeuvres intentionelles dans le but de tromper, de
mentir ou de ne pas dire des informations crutiales sur le contrat.
Cette tromperie peut venir du cocontractant ou d\'un tiers complice du
cocontractant. Pareil que pour les deux derniers, dépendant de
l\'absence de ces tromperies, le contrat peut être considéré nul ou le
cocontractant peut être obligé de payer des dommage et intérêts.

Un vice est dit #strong[incident] si sans ce dernier, le contrat aurait
tout de même été accepté mais sous des conditions différentes. Dans les
cas de vices incidents l\'autre partie doit reverser des dommages et
intérêts.

Un vice est dit #strong[principal] si sans ce dernier, le contrat
n\'aurait pas été accepté. Dans quel cas le contrat est considéré comme
nul.

= Gestion d'un projet entrepreneurial
Il est important de d\'abord bien comprendre ce qu\'est une entreprise.
Notament le fait qu\'il ne faut pas confondre le terme \"entreprise\" et
\"société\". Toutes les sociétés sont des entreprises mais toutes les
entreprises ne sont pas des sociétés.

La notion d\'entreprise a été également agrandir pour inclure toute
initiative économique. La notion qui la précédait était celle de
commerçant. Il faut donc maintenant utiliser le terme entreprise à la
place. Il ne faut #strong[jamais] utiliser le mot \"commerçant\" dans
des réponses de l\'examen, il faut la remplacer par le mot
\"entreprise\".

== Personalité juridique
<personalité-juridique>
Une personne en droit est le fait d\'avoir des droits et des
obligations.

En Belgique, il y a deux types de personnes juridiques. Les
#strong[personnes physiques] qui correspond à toute être humain né et
viable. Et les #strong[personnes morales] qui sont virtuelle et sont
typiquement des groupes de personnes qui agissent ensemble dans un but
commun \(exemples : fondations, ASBL, institutions publics, mutuelles,
SA).

Une personne morale est crée grâce à une loi qui la définit et le
respect d\'une formalité prescrite par cette loi pour acquérir la
personalité juridique.

Une personne morale est un concept basé sur les personnes physiques. Une
personne physique en droit est défninie par son nom/prénom, son adresse,
sa nationalité et son patrimoine \(ensemble de biens/actif et
dettes/passif). Une personne morale va avoir un nom unique, une adresse
de société \(ou siège), une nationalité \(là où se situe son siège), et
un patrimoine.

== Entreprise
<entreprise>
Une #strong[entreprise] est définit comme tout organisme qui remplit une
de ces trois conditions :

- Est une personne physique exerçant une activité professionnelle de
  façon indépendante \(exemple : boucher, libraire, comptable, avocat,
  artisan, etc)
- Est une personne morale \(exemple : SA, SRL, SC, ASBL, fondations,
  etc) #strong[SAUF] les personnes morales de droit public qui ne sont
  des entreprise QUE si elle proposent des bien et des services sur le
  marché.
- Est une organisation sans personalité juridique \(les sociétés sans
  personalité juridique) à condition d\'avoir comme but la distribution
  d\'argent à ses membres \(exemple : les sociétés simple)

= Contrats de collaboration
Les #strong[contrats de collaboration] permettent de collaborer en
restant des personalités juridiques distincte. On ne va donc pas créer
une personne morale, mais simplement collaborer entre entreprises.

Il existe différents types de contrats. Tel que le contrat sur la
propriété intellectuelle, contrat d\'entreprise, l\'agence commerciale,
la concession de vente ou le contrat de franchise. Nous n\'allons pas
ici voir en détail le contrat sur la propriété intellectuelle car elle
est dédiée à un chapitre à part.

== Contrat d\'entreprise
<contrat-dentreprise>
Les #strong[contrats d\'entreprise] sont des contrats par lequel une
personne \(physique ou morale) s\'engage à effectuer de manière
indépendante un travail au profit d\'un cocontractant sans représenter
celui-ci, moyennement un engagement réciproque de lui payer un certain
prix. Par exemple, si la ville de Verviers engage une entreprise de
travaux, cela se fera grâce à un contrat d\'entreprise.

Le contrat d\'entreprise est donc un contrat de location de service. Si
vous décidez de vous lancer en tant qu\'indépendant·e, le contrat
d\'entreprise est donc le contrat de base car c\'est celui que vous
allez signer avec vos client·e·s et vos collaborateur·ice·s.

== Le contrat d\'agence commerciale
<le-contrat-dagence-commerciale>
Le #strong[contrat d\'agence commerciale] est lorsque l\'on confie à un
tiers la négociation de contrats. En somme cela consiste à sous-traitre
la vente de biens ou services.

Dans un contrat d\'agence commerciale, l\'agent commercial est chargé de
façon permanente et moyennant rémunération par l\'autre partie, sans
être soumis à son autorité, la négociation et la conclusion d\'affaires
au nom du commettant.

Cela permet ainsi de ne pas avoir à employer des vendeurs en interne. Et
contrairement au contrat d\'entreprise, les contrats sont ici fait en
votre nom plus tot qu\'au nom du sous-traitant.

Ce type de contrat est particulièrement protégé par des #strong[lois
impératives] \(pour la fin de contrat) qui protège les agents
commerciaux qui sont vu comme la partie faible du contrat. Cela empèche
donc d\'engager un agent pour ammener des clients, et une fois ces
clients fidéliser, virer l\'agent et continuer de profiter des clients
car cela metterait en danger l\'agent.

La loi dit que le contrat d\'agence commerciale peut être soit d\'une
durée #strong[déterminée] ou #strong[indéterminée];.

Si le contrat est de durée #strong[déterminée];, alors le contrat devra
impérativement être exécuté #strong[jusqu\'a son terme];. Si le
commetteur souhaite mettre fin au contrat pour des raisons autre qu\'un
très bon motif \(faute grave par exemple), il devra reverser une
indemnité couvrant en principe le manque à gagner de l\'agent. Cependant
le contrat peut définir en avance des justes motifs permettant de mettre
fin au contrat prématurément.

Si le contrat est de durée #strong[indéterminée];, alors le contrat
pourra prendre fin moyennant la notification de l\'autre partie d\'un
préavis raisonable d\'au moins un mois par année commencée et pouvant
aller jusqu\'a maximum 6 mois. Ainsi pour renvoyer une personne engagée
il y a 3 ans, il faudra la prévenir 3 mois à l\'avance par exemple.

L\'agent commercial est ici rémunéré via les commissions, c\'est-à-dire
qu\'il prend une partie des ventes qu\'il fait. Soit via un pourcentage
\(exemple, 10% de chaque vente) ou un montant fixe \(10€ par vente) ou
une combinaison des deux. Ce montant est donc précisé dans le contrat.

La commission peut être payée au plus tard le dernier jour du mois qui
suit le trimestre au cours duquel le tiers et le commettant ont effectué
leurs obligations contractuelles. Par exemple, si un contrat a été signé
en janvier alors la date limite pour payer la commission correspondante
est le 30 avril.

Afin de pouvoir calculer la rémunération de l\'agent il faudra que
l\'agent ai un relevé des commissions dues. L\'agent peut ainsi exiger
que ces informations lui soit fournie, notament un extrait des livres
comtables afin de bien pouvoir vérifier que le montant des commissions
qui lui sont due est correct.

Il y a encore une protection par loi impérative supplémentaire pour
l\'agent qui est qu\'il peut demander une #strong[indemnité d\'éviction]
à la fin de son contrat si il prouve qu\'il a apporté un réel apport à
l\'entreprise. Le montant de cette indemnité peut monter jusqu\'a un an
de rémunératio. Ce montant est donc calculer sur base de l\'apport de
l\'agent à l\'entreprise.

== Contrat de concession de vente
<contrat-de-concession-de-vente>
Dans un #strong[contrat de concession de vente] un concessionnaire va
vendre les biens. A la différence de l\'agent commercial, le
concessionnaire vend les biens #strong[en son propre nom];.

Cela signifie que le concessionnaire est donc bien mieux payé \(car il
vend comme il veut) mais c\'est plus risqué car il doit gérer lui-même
ses stock par exemple.

Le contrat de concession de vente peut accorder une #strong[exclusivité]
concessionnaire\(s) indiquant que le fournisseur ne vendra les
marchandise qu\'a une certaine liste de concessionaire et à personne
d\'autres \(offrant ainsi un fort avantage aux concessionaires). En
contrepartie de cela, le concessionaire peut se voir demander de
réaliser un certain chiffre d\'affaires, de commander une certaine
quantité de produit au concédant \(fournisseur) ou de constament avoir
un certain stock de marchandise disponible.

Un contrat peut également accorder une #strong[exclusivité] inverse qui
dit que le concessionaire ne peut acheter des marchandise que depuis le
concédant et ne peut donc pas vendre des produits similaires venant
d\'autres fournisseurs.

Si le contrat exige une exclusivité pour le concessionnaire \(qu\'il ne
peut acheter que chez le concédant), alors le contrat sera un contrat à
durée indéterminée. Et lorsque lorsque le concédant souhaite terminer le
contrat, il devra donner un préavis raisonable ou une indemnité du
manque à gagner. En plus de cela le concessionnaire peut demander une
indemnité complémentaire de la même manière que pour l\'agent commercial
par rapport à la plus value de clientelle pour le concédant.

== Contrat de franchise
<contrat-de-franchise>
Contrairement aux autres types de partenariat, le contrat de franchise
n\'est pas réglé ou défini par la loi.

La #strong[franchise] est un système de commercialisation de produits
et/ou services basé sur la collaboration étroite et continue entre des
entreprises. Il impose l\'obligation d\'exploiter une entreprise en
conformiter avec le concept du franchiseur.

Le franchisé va donc devoir utiliser l\'enseigne, la marque, le
savoir-faire et les règles du franchiseur en échange d\'une contribution
financière directe ou indirecte de sa part.

C\'est par exemple le cas de beaucoup de supermarchés tel que Carrefour
par exemple. Le groupe Carrefour est le franchiseur et chaque
supermarché individuel est une franchise de Carrefour. Ce sont donc des
entreprises indépendentes du groupe Carrefour mais qui sont en
collaboration avec le groupe et respectent donc les règles de Carrefour
en échange de leur contribution financière et de la disposition de leur
produits.

= Les sociétés
Comme dit plus tot une entreprise et une société, ce n\'est pas la même
chose. Toutes les sociétés sont eds entreprises mais pas l\'inverse. Il
n\'est donc pas obligatoire de créer une société pour fonder un projet
entrepreneurial.

Il y a plusieurs raison de vouloir constituer une société :

- La société #emph[peut] disposer d\'une personnalité juridique
- La société permet de mettre en commun des biens ou des fonds entre
  plusieurs personnes
- La société peut également garantir une responsabilité limitée en
  raison du fait que si on crée une personalité juridique, la société
  aura son patrimoine propre et sera séparé du patrimoine des personnes
  qui la constitue
- Le fait que les biens et les fonds reste dans la société permet de
  rendre la société plus pérenne car elle peut vivre plus longtemps que
  les personnes qui sont dedans
- La société bénéficie d\'un statut fiscal différent d\'une personne
  physique. Cependant l\'avantage de ce statut diminue avec les
  nombreuses réformes successives.
- La société offre un statut social différent que celui d\'une personne
  physique. C\'est-à-dire que le calcul des charges sociales \(la
  différence entre le salaire brut et le salaire net), est différent
  pour une société que pour une personne physique indépendante.

Une #strong[société] est constituée par un acte juridique par lequel une
ou plusieurs personnes \(associés) font un apport de patrimoine pour
exercer une ou plusieurs activités déterminées. Mais un des buts de la
société est de redistribuer l\'argent à ses associés.

Il existe plusieurs formes de sociétés en fonction de certains critères
\(personnalité juridique, responsabilité, nombre d\'associés, etc).

Une société va être divisées en \"parts\" qui seront reversée aux
différents associés.

Il existe 4 formes légales de sociétés, les sociétés #strong[simple];,
les sociétés à #strong[responsabilité limitée \(SRL)];, les sociétés
#strong[coopératives \(SC)] et les sociétés #strong[anonymes \(SA)];.

== Société responsabilité limitée
<société-responsabilité-limitée>
Dans une SRL, le patrimoine est le patrimoine #emph[de la société];. Les
créanciers ne peuvent donc pas aller demander des dettes à n\'importe
quel associé de la société.

Le risque financier des associés est donc limité à leurs apports dans la
sociétés. Et une fois leurs apports libérés, leurs patrimoines privés ne
sont plus exposés aux risques financiers de la société, à l\'exception
des fondateurs, des gérants et #strong[administrateurs] \(pour faute de
gestion) et des titulaires d\'une profession libérale \(liée aux actes
de leur profession). Mais très souvent les assoicés vont mettre leur
patrimoine personnel en jeu par eux même \(par exemple pour rassurer une
banque par rapport à la capacité de paiement d\'un prêt).

Les #strong[administrateurs] sont des personnes nommées par les associés
et chargé de la gestoin de la société \(conseil d\'administration).

== Société à responsabilité illimitée
<société-à-responsabilité-illimitée>
Dans une société à responsabilité illimitée, tout le monde est
responsable de tout. Les créanciers peuvnet donc demander le paiement
d\'une dette à n\'importe quel associé de la société.

C\'est donc une forme bien moins avantageuse que celle de la SRL.

== Choisir la forme de société:
<choisir-la-forme-de-société>
#image("../choix-structure-societe.png")

== Personnes morales autre que les sociétés
<personnes-morales-autre-que-les-sociétés>
L\'#strong[association] est une convention entre plusieurs personnes
\(membres) qui poursuit un but désintéressé dans le cadre d\'une ou
plusieurs activités déterminées. Elle n\'a donc pas pour but
\(contrairement aux sociétés) de redistribuer l\'argent à ses membres.

Une #strong[fondation] est une personne morale dépourvue de membres qui
est constituée par des fondateurs. Le patrimoine est utilisé pour un but
désintéressé dans le cadre d\'une ou plusieurs activités déterminées.
Elle ne peut donc pas redistribuer l\'argent à ses fondateurs.

