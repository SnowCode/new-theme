#set list(marker: ([•], [–], [+], [‣]))
#outline(depth: 2, indent: 2em)

== Les bases du droit
<les-bases-du-droit>
- Différentes perspectives sur le droit
  - Droit #strong[positif]
    - Ensemble de règles autour des relations sociales
    - Pour un groupe de personnes donné
    - Sur un territoire donné
    - Doit être accompagné d\'une sanction
  - Droit #strong[objectif] lorsque l\'on parle de la règle en général
    dans ses grands principes
    - Exemple : \"le droit du mariage\"
  - Droit #strong[subjectif] lorsque l\'on parle de la règle dans un cas
    particulier
    - Exemple : \"Jeanne et Alice veulent se marier mais se font refuser
      leur droit.\"
- Ethique et morale
  - Sont personnels et différents pour chaque individu, contrairement au
    droit
  - L\'éthique est l\'étude philosophique de la morale
  - La morale est l\'ensemble des principes et valeurs d\'une personne
  - La déontologie est une obligation morale pour une profession donnée
    afin de protéger le public et protéger sa réputation dans une
    profession \(ou protéger la réputation de la profession elle-même)
- Droit public et privé
  - Le droit #strong[privé] régit les rapport entre les personnes
    physiques et morales
    - Exemple : droit civil, droit des affaires, droit social, droit des
      assurances
    - Le droit #strong[civil] décrit la résolution de litiges entre une
      partie plaignante et une partie défenseur
      - Exemple : conflits de famille, contrats, successions.
      - La réparation de dommages se fait par des
        #strong[dommages-intérêts] versé à la partie adverse
  - Le droit #strong[public] régit les rapports entre les personnes
    physiques et morales et l\'État
    - Exemple : administration, police, institutions, etc
    - Le droit #strong[constitutionnel] définit les libertés
      fondamentales et la structure de l\'État
    - Le droit #strong[administratif] définit les droits et obligations
      des administrations
    - Le droit #strong[fiscal] définit les droits relatifs aux impots et
      à l\'organisation financière de l\'État
    - Le droit #strong[pénal] réprime les comportements dangereux pour
      la société elle-même \(crimes, etc)
  - Le droit #strong[judiciaire] tombe dans les deux catégories \(public
    et civil). Il traite l\'organisation des tribunaux et de la
    procédure judiciaire.
- Notions de droit public
  - État est séparé en 3 pouvoirs
    - Le pouvoir #strong[législatif] \(parlement) s\'occupe de rédiger
      les lois
    - Le pouvoir #strong[exécutif] \(gouvernement) met en application
      les lois
    - Le pouvoir #strong[judiciaire] contrôle l\'application de la loi
  - Niveaux de pouvoirs
    - Le niveau de pouvoir #strong[fédéral] gère tout ce qui est commun
      à tout le territoire \(armée par exemple)
    - Le niveau de pouvoir #strong[communautaire] gère tout ce qui est
      en rapport avec la culture, l\'enseignemnet, la santé, l\'aide au
      personnes et aux langues.
    - Le niveau de pouvoir #strong[régional] gère tout ce qui est en
      rapport avec l\'économie, la culture, l\'urbanisme, le transport,
      l\'énergie, l\'emploi, etc
    - Le niveau de pouvoir #strong[provincial]
    - Le niveau de pouvoir #strong[communal]

== Sources de droit positif belge
<sources-de-droit-positif-belge>
- Droit international
  - Le droit #strong[international public] : ensemble de règles qui
    régissent les rapports entre les États et les organisation
    internationales
  - Le droit #strong[international privé] : régit les relations de droit
    privé présentant un élément externe \(par exemple, parties établies
    dans des états différents)
  - Le droit #strong[européen] est une branche du droit international
    qui est plus uniforme et intégrée
    - Les #strong[traités européens] définissent les grandes lignes de
      l\'union européenne et sa structure
    - Les #strong[règlements] sont des actes législatif qui
      s\'appliquent de manière uniforme dans toute l\'union européenne
      \(exemple, RGPD)
    - Les #strong[directives] sont des obligation de résultats pour tous
      les États membres. Les pays doivent donc transcrire les directives
      dans leur léglisation nationale afin d\'arriver aux objectifs
      donnés.
- Organisation de la loi belge
  - Généralement le droit international prime sur le droit national
    \(par exemple, le RGPD)
  - La #strong[loi fédérale] est une loi adoptée par la chambre de
    représentats et/ou le sénat
  - Les #strong[décrets] sont des lois adoptée au niveau des régions et
    des communautés uniquement
    - Les décrets et les lois fédérales ont le même niveau
      d\'importance. Si elles entre en conflit, la règle la plus récente
      prime.
  - Les #strong[ordonnances] sont des décrets moins puissant pour la
    région de Bruxelles-Capitale
  - Les #strong[arrétés d\'exécutions] sont de sactes du pouvoir
    exécutif qui détaille l\'application d\'une loi et sont donc
    beaucoup plus concret
    - Les #strong[arrétés royaux] sont des arrétés d\'exécution au
      niveau fédéral
    - Les #strong[arrétés du gouvernement] sont des arrétés d\'exécution
      au niveau des communautés ou régions
- Types de lois
  - Les #strong[lois d\'ordre public] sont des lois qui touche au coeur
    même de la structure de l\'état et de l\'ordre socal \(code pénal,
    droit fiscal, etc)
  - Les #strong[lois impératives] sont des lois auquel on ne peut pas
    déroger par un contrat. Elles servent à protéger une partie \(dite
    \"partie faible\") des abus d\'une autre partie
    - Est reconnue par la mention \"#emph[nonobstant toutes dispositions
      contraires];\" ou \"#emph[tout acte juridique contraire sera
      déclaré nul];\"
    - Exemple : droit de protection du consommateur, cela empèche la
      création de contrats dangereux pour le consommateur
  - Les #strong[lois supplétives] sont des lois auquel on peut déroger
    par un contrat. Elles sont appliquées si le contrat ne prévois rien
    à ce sujet
    - Est reconnue par la mention \"#emph[sauf disposition contraire];\"
      ou \"#emph[sauf si les statuts ou le contrat en dispose
      autrement];\" ou encore \"#emph[sauf si les parIes en ont convenu
      autrement];\"
- La #strong[coutume];, règle de droit non-écrite pratiquée par la
  population sur un certain territoire.
  - Exemple : Le fait que deux personnes soit fiancées est vu comme le
    juge comme une marque d\'engagement alors même que le concept de
    \"fiancé\" n\'existe nulle part dans la législation
- La #strong[jurisprudence] est l\'ensemble de décisions judicaires
  rendues par les cours et tribunaux
  - En Belgique, la jurisprudence n\'est qu\'une source d\'inspiration
    pour les juges mais les juges restent tout à fait indépendant
  - Dans certaines situations les juges peuvent déclarer des
    #strong[abus de droit] qu\'une personne a utilisé son droit afin de
    nuire à une autre partie
- La #strong[doctrine] est l\'ensemble de travaux académiques
  spécialisées en droit. La doctrine est utilisée comme
  \"documentation\" pour les juges pour voir différentes manières de
  tracher un litige

== Les contrats
<les-contrats>
- Un #strong[contrat] est un accord de volonté entre plusieurs parties
  avec l\'intention de faire naitre des effets de droit \(des droits et
  des obligations)
- La négociation d\'un contrat consiste à discuter les termes du contrat
  - Parfois la négociation peut être encadrée par un autre contrat, le
    #strong[NDA] \(Non Disclosure Agreement ou contrat de
    confidentialité)
- L\'accord de volonté
  - Le simple fait que deux personnes sont d\'accord sur quelque chose,
    crée le contrat. → #strong[principe de consensualisme]
  - Un contrat peut être #strong[unilatéral] \(don ou caution) ou
    #strong[bilatéral] \(prestation rémunérée)
  - Un contrat peut être #strong[gratuit] \(une seule partie a des
    avantages, exemple : don) ou #strong[onéreux] \(si les deux parties
    reçoivent des avantages)
  - Si le gain d\'un contrat est certain, alors le contrat est
    #strong[commutatif];, si les gains dépendent d\'aléats \(exemple,
    d\'un pourcentage de gain de l\'entreprise), alors on dit que le
    contrat est #strong[aléatoire]
  - Le contrat est dit #strong[pur] si il est applicable immédiatement,
    #strong[à terme] si il ne s\'applique qu\'a un moment déterminé dans
    le futur, ou #strong[sous condition] si il s\'applique lorsque
    certaines conditions sont remplies
    - On parle de #strong[condition suspentive] lorsque la condition
      déclenche l\'application du contrat
    - On parle de #strong[condition résolutoire] lorsque la condition
      déclenche la fin du contrat
  - Types de contrats
    - Contrat #strong[consensuel];, est le contrat \"normal\", pas de
      forme demandée, simple accord de volonté suffit
      - Exemple : vente
    - Contrat #strong[formel];, est un contrat où une certaine forme est
      imposée et si la forme n\'est pas respectée, le contrat n\'est pas
      valide
      - Exemple : crédit hypothécaire, contrat de mariage, crédit à la
        consommation, etc.
    - Contrat #strong[réel] où le contrat dépends de la remise d\'une
      chose à l\'autre partie
      - Exemple : contrat de dépot ou de prêt
- Une fois les termes négocié et l\'accord donné, le contrat est rédigé
  et signé. Parfois cela est obligatoire \(contrat formel) mais le reste
  du temps c\'est fortement recommandé car cela permet d\'avoir une
  preuve des engagements.
  - Le contrat peut être signé de manère manuscrite ou électronique
    - La signature électronique peut être #strong[ordinaire] \(simple
      signature en bas de mail par exempl), #strong[avancée]
      \(cryptographie par exemple), ou #strong[qualifiée]
      \(cryptographie certifiée, avec une carte d\'identité par
      exemple).
    - Seul la signature électronique qualifiée est équivalente à une
      signature manuscrite
  - Le contrat peut être signé soit par une personne physique soit par
    un représentant d\'une personne morale.
- Une fois signé le contrat est mis en application selon ses termes
  - Le principe de #strong[convention-loi] indique que chaque partie
    doit exécuter ce qui a été convenu et qu\'une partie ne peut pas
    décider seule de mettre fin au contrat ou de le modifier \(à moins
    que cela soit prévu dans le contrat lui-même)
  - Le principe de #strong[bonne foi] consiste à ne pas abuser de sa
    position économique ou de sanctionner des manquements sans gravités.
    De même une victime doit tout faire pour empécher que son dommage ne
    s\'aggrave.

=== Validité d\'un contrat et vice de consentements
<validité-dun-contrat-et-vice-de-consentements>
- Chaque partie doit être en #strong[capacité] de contracter \(exemple :
  ne doit pas être mineur, ou sous influence de l\'alcool ou de drogues)
- L\'objet du contrat doit être déterminable et licite \(doit porter sur
  des prestations légales)
- La cause du contrat \(sa raison d\'être) doit être licite \(légale)
- Le consentement libre et éclairé des deux parties du contrat. Il peut
  avoir plusieurs types de vices de consentements
  - L\'#strong[erreur] portant sur une qualité du contrat autre que la
    valeur \(le prix) ou la personne qui contracte \(sauf si le contrat
    a été fait en vue de la personne). Pour que l\'erreur conduise à la
    nullité du contrat il faut qu\'elle soit excusable \(aurait pu être
    faite par une personne prudente, pas pris à la légère)
  - La #strong[lésion] lorsqu\'il y a un déséquillibre manifeste entre
    les obligations des deux parties. La lésion n\'est un vice de
    consenetement que dans certains cas prévus par la loi.
  - L\'#strong[abus de circonstance] lorsqu\'il y a un déséquillibre
    manifeste entre les obligations des deux parties et une position de
    faiblesse de l\'autre partie où cette faiblesse est influente sur
    l\'acceptation du contrat
  - La #strong[violence] consiste en une menace à l\'intégrité d\'une
    personne. La violence consiste à impressioner une personne pour la
    pousser à accepter un contrat.
  - Le #strong[dol] consiste à mentir, tromper ou ne pas divulguer des
    informations pour pousser la personne à accepter le contrat.
  - Un vice de consentement est dit #strong[incident] si sans lui, le
    contrat aurait été accepté sous d\'autres conditions, alors la
    personne doit donner des dommages et intérets. Et il est dit
    #strong[principal] si sans lui, le contrat n\'aurait pas été accepté
    du tout, si c\'est le cas le contrat est déclaré nul.

== Entreprises
<entreprises>
- La définition de \"commercant\" a été remplacée par celle
  d\'entreprise qui est beaucoup plus large. Est une entreprise, toute
  organisation ou personne entrant dans l\'une de ces catégories
  - Personne physique exerçant une activité professionnelle de façon
    indépendante
  - Personne morale SAUF personnes morales de droit public qui ne
    proposent pas de biens sur le marché
  - Organisation sans personalité juridique \(sociétés sans personalité
    juridique) à condition d\'avoir comme but la redistribution
    d\'argent à ses membres
- Types de contrats de collaboration entre entreprises
  - #strong[Contrat d\'entreprise];, consiste à louer un service à une
    autre entrperise. C\'est le contrat de base quand on travaille comme
    indépendant
  - #strong[Contrat d\'agence commerciale];, consiste à confier la
    négociation de contrats à un cocontractant.
    - Les agents sont protégé par beaucoup de lois impératives
    - Si le contrat a une durée déterminée, le contrat doit aller
      jusqu\'a son terme, ou alors il faut reverser des indemnités à
      l\'agent
    - Si le contrat a une durée indéterminée, le contrat peut prendre
      fin en mettant un préavis suffisament en avance.
    - L\'agent est payé par concessions sur les ventes qu\'iel effectue
    - Si l\'agent a demandé une vrai plus value à l\'entreprise, iel
      peut demander une indemnité d\'éviction à la fin de son contrat
  - #strong[Contrat de concession de vente];, consiste à définir une
    autre entreprise comme étant chargée de la vente \(à son nom) des
    biens ou services de notre entreprise
    - Le contrat peut indiquer une exclusivité, par exemple le
      concessionnaire ne peut vendre que les produits du fournisseurs;
      et/ou le fournisseur ne peut vendre qu\'au concessionnaire.
    - Si le contrat inclu une exclusivité pour le concessionnaire, il
      faut donner un préavis suffisant avant la fin du contrat \(ou une
      indemnité suffisante) et peut également demander une indemnité
      complémentaire.
  - #strong[Contrat de franchise] consiste à définir une collaboration
    étroite entre un franchiseur et des entreprises franchisées qui vont
    agir en respectant le concept et la marque de l\'entreprise.
    - Ce type de contrat n\'est pas formalisé dans la loi

== Les sociétés
<les-sociétés>
- Toutes les sociétés sont des entreprises mais toutes les entreprises
  ne sont pas des sociétés
- Les sociétés peuvent offrir certaines avantages par rapport au status
  d\'indépendant
  - Disposition d\'une personalité juridique
  - Mise en commun de fonds
  - Garantir une responsabilité limitée pour les membres de la société
  - Plus durable que les gens qui la compose
  - Status fiscal et social différent \(et parfois plus avantageux)
- Les sociétés sont constituée par un acte juridique où des personnes
  \(associés ou actionnaires) apporte du patrimoine à la société. La
  société doit avoir pour but de redistribuer du patrimoine aux associés
- Formes de sociétés
  - SRL, SC et SA → Société à responsabilité limitée, avec une
    personalité juridique. Les patrimoines privés des associés sont
    protégé par la personalité juridique de la SRL.
  - Société simple → société à responsabilité illimitée \(avec ou sans
    personalité juridique), tous les membres sont responsable du
    patrimoine. Un créancier peut donc demander le paiement d\'une dette
    à n\'importe quel associé.
- Autres formes d\'entreprise que les sociétés
  - L\'#strong[association] qui est une convention entre plusieurs
    membres dans un but désintéressé pour des activités déterminées et
    sans avoir pour objectif de redistribuer l\'argent aux membres
  - La #strong[fondation] qui est une personne morale dépourvue de
    membres et qui est constituée par les fondateurs. Elle est utilisée
    pour un but désintéressé et pour des activités déterminées \(donc
    pas de redistribution aux fondateurs, en théorie).

