#show image: i => {
  set image(height: 200pt)
  align(center, i)
}


#show raw: c => {
  set text(font: "CMU Typewriter Text")
  if (c.block == true) {
    block(fill: rgb("#EDF0F2"), inset: 8pt, radius: 5pt, width: 100%, c)
  } else {
    box(fill: rgb("#EDF0F2"), inset: 2.5pt, radius: 3pt, baseline: 20%, c)
  }
}

#align(center, [= Synthèse de math théorique B2])

#align(center, block(fill: red, inset: 2em, radius: 1em, strong(text(fill: white, "Malheureusement cette synthèse n'est pas aussi complète ou qualitative que je l'aurais voulu, donc elle peut être utile pour contextualiser, mais pas comme source principale"))))

#outline(depth: 3, indent: 2em)

= Introduction sur la théorie des graphes
== Premières définitions sur les graphes
<premières-définitions-sur-les-graphes>
Un #strong[graphe] fini est défini comme un ensemble de #strong[sommets]
\(vertices) et un ensemble d\'#strong[arêtes] \(edges).

Chaque #strong[arête] est définie comme une paire non ordonnée de
sommets. Les sommets dans une arête sont appelé des #strong[extrémités];.

Si deux sommets sont reliés par une arête on dit qu\'ils sont
#strong[adjacents] et ont dit que l\'arête est #strong[incidente];.

Le nombre de sommets définit l\'#strong[ordre] du graphe. Ainsi un
graphe de 3 sommets, sera ainsi définit comme un graphe d\'ordre 3.

== Représentations graphiques d\'un graphe
<représentations-graphiques-dun-graphe>
Si on souhaite définir un graphe sans qu\'aucune de ses arêtes ne se
croise, on dit que le graphe est #strong[planaire];.

Il est important de savoir qu\'un graphe est un concept théorique, il y
a donc une infinité de représentations graphiques possibles pour un même
graphe.

Voici par exemple un même graphe avec deux représentations. Le fait que
dans la deuxième représentation, aucune arête ne se touche indique que
le graphe est #strong[planaire];.

#image("graphe-plannaire.png")

== Types de graphes
<types-de-graphes>
Un graphe #strong[simple] est un graphe où maximum une arête relie deux
sommets et où il n\'y a pas de boucles sur un sommet.

A l\'inverse il y a le #strong[multigraphe] qui est un graphe où il peut
y avoir plusieurs arêtes entre deux même sommets et où une arête peut
relier un sommet à lui-même.

Pour les graphes il existe la notation #strong[p-graphe] où `p`
représente le nombre d\'arête maximum entre deux sommets. Par exemple si
il y a au plus deux arêtes entre deux sommets, on dira que c\'est un
#emph[2-graphe];.

Le plus souvent on étudira des 1-graphe.

#image("p-graph.png")

Un graphe est #strong[pondéré] si des valeurs sont associées aux arêtes.
La valeur associée à une arête est appellée le #strong[poids] de
l\'arête.

#image("graphe-pondere.png")

=== Connectivités des graphes
<connectivités-des-graphes>
Un graphe est dit #strong[connexe] lorsqu\'il est possible de trouver un
chemin pour relier n\'importe quel sommet à n\'importe quel autre
sommet. En d\'autres termes cela signifie que tous les sommets sont
connectés d\'une manirèe ou d\'une autre.

A l\'inverse un graphe #strong[non connexe] peut être décomposé en
#strong[composantes connexes];.

Une arête est appellée un #strong[pont] si sa suppression augmente le
nombre de composantes connexes du graphe.

#image("composantes-connexes.png")

Le graphe ci-dessus est un graphe non-connexe car il est impossible de
relier 6 à 1 par exemple car 5 et 6 forme un groupe à part du graphe.

Si sur le graphe ci-dessus, on supprime l\'arête reliant 2 et 3, 2 se
retrouve déconnecté du reste, par conséquent le nombre de composantes
connexes augmente. Il en va de même pour l\'arête 5 à 6. Ces deux arêtes
sont donc des #strong[ponts] dans le graphe.

=== Graphes complets et biparti
<graphes-complets-et-biparti>
Un graphe est dit #strong[complet] si chaque sommet du graphe est relié
directement à tous les autres sommets.

#image("graphe-complet.png")

Un graphe complet est souvent représenté avec la notation #strong[K]
ainsi un graphe complet #strong[K5] signifie qu\'il s\'agit d\'un graphe
complet à 5 sommets.

Un graphe est dit #strong[biparti] si ses sommets peuvent être divisés
en deux ensembles de sorte que les sommets d\'un ensemble n\'ont
d\'arrête que vers des sommets de l\'autre ensemble.

#image("graphe-biparti.png")

Le graphe ci-dessus est biparti car les sommets 1, 3 et 5 ne sont pas
reliés entre eux mais mais on des liens vers des sommets de l\'ensemble
2 et 4. Il en va de même pour 2 et 4 qui sont liés aux sommets de
l\'ensemble 2,3,5 mais pas entre eux.

Enfin il existe les graphes #strong[biparti complèts] lorsque chaque
sommet de chaque ensemble est connecté à tous les sommets de l\'autre
ensemble.

= Première partie de théorie sur les graphes
== Graphes partiels et sous-graphes
<graphes-partiels-et-sous-graphes>
Un graphe est un graphe #strong[partiel] d\'un autre graphe lorsqu\'il
correspond à l\'autre graphe mais avec des arêtes en moins \(en
d\'autres termes, il a les même sommets, juste moins d\'arêtes).

Un #strong[sous-graphe] en revanche est une partie de graphe. Il s\'agit
donc d\'enlever des sommets ainsi que toutes les arêtes incidentes \(qui
sont liées) à ces sommets.

#image("sous-graphe.png")

Un #strong[sous-graphe partiel] désigne un graphe partiel d\'un sous
graphe \(soit une partie d\'un graphe plus grand, mais avec moins
d\'arêtes).

Une #strong[clique] désigne un sous-graphe complet d\'un graphe plus
grand. Par exemple dans le graphe ci-dessous le sous graphe composés des
sommets 5, 1 et 2 est une clique car ils sont tous liés entre eux
directement.

#image("clique.png")

== Degré de sommets et de graphes
<degré-de-sommets-et-de-graphes>
Le #strong[degré] d\'un sommet correspond au nombres d\'arêtes qui lui
sont incidente \(qui le relie). Dans un graphe simple, le degré d\'un
sommet est donc simplement le nombre de voisins de ce sommet.

Si un sommet est relié à lui même, l\'arête qui est liée à lui-même sera
considéré comme 2.

#image("degre-multigraphe.png")

Il est possible de tirer certaines conclusions sur base des arêtes et
des degrés. On sait par exemple que la somme des degrés des sommets
d\'un graphe correspond à deux fois le nombre d\'arête car chaque arête
contribue pour 2 sommets \(donc 2 degrés).

Egalement un graphe simple aura toujours un nombre pair de sommets de
degré impair.

Le #strong[degré d\'un graphe] est le degré maximum de tous ses sommets.
Donc si le plus haut degré de sommet d\'un graphe est 4, le degré du
graphe sera 4.

On parlera de #strong[k-régulier] où k est le degré, pour définir un
graphe où tous les sommets ont le même degré.

Pour donner un exemple concret de ce principe, les degrés peuvent nous
aider à résoudre cette situation :

#quote(block: true)[
Est-il possible de relier 15 ordinateurs de sorte que chaque appareil
soit relié avec exactement trois autres ?
]

La situation peut être représenté par un graphe simple où chaque sommet
est un ordinateur et chaque connexion est une arête.

Si chaque sommets est relié à 3 autres, alors chaque sommet aura un
degré de 3, ce qui est impossible au vu du théorème vu plus tot qui dit
que #emph[Un graphe simple aura toujours un nombre pair de degré
impair];. Ici il y a un nombre impair \(15) de sommets qui ont un degré
impair \(3).

== Chaînes et cycles
<chaînes-et-cycles>
Une #strong[chaîne] dans un graphe est une suite de sommets encadré par
des arrêtes. Par exemple `1 - 2 - 3 - 4` \(où les nombres sont des
sommets, et les tirets sont des arêtes).

La #strong[longueur de la chaine] correspond au nombre d\'arrête. Pour
l\'exemple donné plus tot, la longueur de la chaine est donc 3.

La #strong[distance] entre deux sommets, correspond à la longueur de la
plus petite chaîne permettant de les relier.

Le #strong[diamètre] d\'un graphe correspond à la plus longue distance
entre deux sommets du graphe.

Une chaîne est dite #strong[élémentaire] si chaque sommet y apparait au
plus une fois, en d\'autres termes, si on ne passe jamais 2 fois par le
même sommet.

Une chaine est dite #strong[simple] si chaque #emph[arète] apparaît au
plus une fois \(on ne passe pas 2 fois par la même arête).

Une chaîne où les sommets de départ et de fin est appellée une
#strong[chaîne fermée];. Et une chaîne fermée simple \(où on ne passe
qu\'une seule fois sur chaque arrête) est appellée un #strong[cycle];.

Cette définiton de cycle permet égalmeent de définir les graphes
biparti. Un graphe biparti peut être défini comme un graphe qui ne
contient aucun cycle de longueur impaire.

== Graphes, chaine et cycles eulériens
<graphes-chaine-et-cycles-eulériens>
Un #strong[cycle eulérien] est un cycle passant une seule fois par
chacune des arêtes du graphe.

Un #strong[graphe eulérien] est un graphe possédant un cycle eulérien.

Une #strong[chaine eulérienne] est une chaîne passant une seule fois par
chacunes des arêtes du graphe.

Un #strong[graphe semi-eulérien] est un graphe ne possédant que des
chaînes eulériennes.

Il est possible de savoir si un graphe est eulérien ou semi-eulérien
très facilement :

- Si le graphe est connexe et possède exactement deux sommets de degré
  impair, alors il est #strong[semi-eulérien]
- Si le graphe est connexe et possède uniquement des sommets de degré
  pair, alors il est #strong[eulérien]

#image("probleme-7-ponts.png")

Grâce à ces informations il est donc possible d\'expliquer pourquoi le
problème des 7 ponts de Königsberg \(représenté ci-dessus) n\'a pas de
solution.

Le problème est de trouver un chemin qui permet de retourner à son point
de départ en passant par chaque pont de la ville.

On peut voir que toutes les îles ont un nombre impair de ponts \(C en as
3, D en as 3, B en as 3 et A en as 5), il ne rempli donc ni la condition
pour être semi-eulérien, ni la condition pour être eulérien.

== Graphes, chaine et cycles hamiltoniens
<graphes-chaine-et-cycles-hamiltoniens>
Un #strong[cycle hamiltonien] est un cycle passant une seule fois par
chacun des sommets du graphe.

Un #strong[graphe hamiltonien] est un graphe qui possède un cycle
hamiltonien.

Une #strong[chaîne hamiltonienne] est une chaîne passant une seule fois
par chacun des sommets du graphe.

Un #strong[graphe semi-hamiltonien] est un graphe ne possédant que des
chaînes hamiltoniennes.

Si le graphe possède un sommet de degré 1, alors le graphe ne peut pas
être hamiltonien. Si le graphe possède un sommet de degré 2, alors les
deux arrêtes incidentes à ce sommet doivent faire partie du cycle. Tous
les graphes complets sont hamiltoniens.

Il n\'existe pas de caractérisation simple des graphes
semi-hamiltoniens.

#image("graphes-hamiltoniens-euleriens.png")

== Couplage
<couplage>
Un #strong[couplage] de graphe est un sous-graphe \(partie du graphe)
partiel \(auquel il manque des arêtes) 1-régulier \(chaque sommet à un
seul voisin) de celui-ci.

En somme un couplage est constitué de 2 sommets et un ou plusieurs
arêtes entre les deux. Il doit être impossible de lier des couplages
entre eux, donc chaque sommet peut faire partie de maximum un seul
couplage.

Un sommet faisant partie d\'un couplage est dit #strong[saturé] \(et
dans le cas contraire, il est dit #strong[insaturé];).

Un #strong[couplage maximum] contient le plus grand nombre possible
d\'arrête.

Un #strong[couplage parfait] est un couplage où chaque sommet du graphe
est saturé.

#image("couplage.png")

Une #strong[chaîne alternée] est une chaîne élémentaire \(où on ne passe
qu\'une seule fois par chaque sommet) qui alterne à chaque fois entre
une arrête dans un couplage, et une arrête hors du couplage.

#image("chaine-alternee.png")

Une #strong[chaîne augmentante] est une chaîne alternée qui relie deux
sommets insaturés. C\'est notament l\'exemple du cas au dessus car on
relie le sommet 1 et 6 qui sont tous les deux insaturés.

Par définition un couplage est maximum \(tous les sommets sont saturés)
uniquement si il n\'y a pas de chaine augmentante relative au couplage
\(pas de sommets insaturés).

== Graphes planaires
<graphes-planaires>
Comme dit plus tot un graphe est dit #strong[planaire] si il est
possible de le dessiner dans un plan en 2D de sorte que ses arêtes ne se
croisent pas \(à savoir que les arêtes ne sont pas forcément droites).

Une #strong[carte] ou un #strong[graphe planaire topologique] est une
représentation graphique qui satisfait cette condition.

Une carte divise le plan en plusieurs #strong[régions];, par exemple les
lettres représentes les différentes régions dans la carte ci-dessous :

#image("graphe-carte.png")

Le #strong[degré d\'une région] \(noté #emph[d\(r)];) est la longueur de
chaîne fermée #emph[minimum] passant par #emph[tous] les sommets qui
délimitent la région.

Par exemple dans la carte vue plus tot, la région A a un degré de 6 car
la longueur de la chaine `1 - 5 - 6 - 5 - 3 - 2 - 1` est 6.

La sommes des degrés des régions d\'une carte connexe \(carte d\'un
graphe connexe) correspond à deux fois le nombre d\'arêtes du graphe.

Il existe égalmenet une relation \(relation d\'Euler) entre le nombre de
sommets \(S), le nombre d\'arêtes \(A) et le nombre de régions \(R).
Cette relation est `S - A + R = 2`.

Par exemple dans le même exemple vu plus tot, il y a 7 sommets, 9 arêtes
et 4 régions, soit `7 - 9 + 4 = 2`.

Un graphe est #strong[non planaire] si il contient un sous-graphe
homéomorphe \(?) au graphe biparti K3,3 ou au graphe complet K5.

Grâce à cela il est donc possible de savoir si un graphe est planaire ou
non.

#image("graphe-k3-3.png")

Par exemple si on veut prouver que le graphe ci-dessus n\'est pas
planaire :

Le graphe a 6 sommets et 9 arrêtes, selon la relation d\'Euler, on peut
donc en déduire qu\'il doit y avoir 5 régions.

On peut aussi savoir que le degré total de toutes les régions correspond
à 2 fois le nombre d\'arrêtes. Donc dans ce cas, 18.

Ensuite on peut essayer de trouver la longueur de chaine fermée minimum
du graphe \(par exemple `1-2-3-4-1`) qui est donc d\'une longueur de 4.
Sauf que si on suppose que toutes les régions font cette longueur
minimale, alors la somme ferrait 5 \(le nombre de régions) fois 4 \(la
longueur minimale), ce qui donnerait 20 et pas 18 comme ça devrait
l\'être normalement.

= Représentations non graphiques de graphes non-orientés
== Matrice d\'adjacence
<matrice-dadjacence>
Il est possible de représenter un graphe simple par une #strong[matrice
d\'ajacence] où chaque ligne et chaque colonne représente un sommet.

Dans un graphe simple \(non pondéré), si il existe un lien entre deux
sommets, on va trouver le croisement des deux et y indiquer un 1, sinon
on indique 0.

#image("matrice-adjacence-graphe-simple.png")

La diagonale a toujours des zéros à moins qu\'il existe une boucle d\'un
sommet vers lui-même.

Les matrices d\'ajacence ont l\'avantage de permettre de savoir très
simplement si une adjacence existe entre deux sommets. Il suffit de voir
leur croisement dans la matrice.

Les matrices d\'ajacence ont cependant le désavantage de nécessiter plus
de stockage puis ce qu\'il faut également stocker les adjacences qui
n\'existe pas \(avec un 0). Le stockage nécessaire est donc S^2 \(si il
y a 5 sommets, il faudra 25 unités de stockage).

== Liste d\'adjacence
<liste-dadjacence>
Il est aussi possible de représenter un graphe simple en donnant la
liste des sommets qui lui sont adjacent.

Ces listes sont généralement ordonnée du sommet avec le nombre le plus
petit, au sommet avec le nombre le plus grand.

#image("liste-adjacence-graphe-simple.png")

Les listes d\'adjacence ont l\'avantage d\'être plus compacte que les
matrices d\'ajacence car elles ne stoquent que les adjacences qui existe
réellement.

Elles ont cependant le désavantage d\'être beaucoup moins efficace à
chercher des adjacence entre deux points car il faut itérer toute la
liste d\'un sommmet \(si un sommet a beaucoup d\'adjacence, cela peut
donc prendre beaucoup de temps). 

= La base sur les arbres
Un #strong[arbre] est tout graphe connexe sans cycle.

Et un graphe sans cycle mais non connexe est appellé une #strong[forêt]
\(car ca fait plusieurs graphes connexe sans cycle, donc plusieurs
arbres).

#image("jdg-cest-logique.gif")

Une #strong[feuille] ou #strong[sommet pendant] est un sommet de degré 1
\(qui se trouve donc au bout d\'un arbre).

Un arbre est une structure fondamentale en informatique par ce qu\'elle
est utilisée pour répertorier les fichiers, créer de stables de
matières, des sortes de tri \(heapsort), des expressions algébriques,
des frameworks, etc.

Un arbre peut être compris comme un graphe hierarchique qui possède une
#strong[racine] duquel tous les autres sommets partent.

#image("graphe-arbre.png")

Par exemple dans l\'image ci-dessus le sommet 4 au dessus de tous les
autres peut être pris comme la racine, mais cela aurait très bien pu
être le sommet 3 par exemple.

Pour représenter les liens entre les différents éléments on peut
utiliser la terminologie généalogique tel que \"père\", \"fils\",
\"ancêtre\", etc.

On peut également partitioner l\'ensemble des sommets en #strong[rangs];.
Les rangs est la profondeur d\'un élément par rapport à la racine. Par
exemple la racine a un rang de 0, un enfant de la racine a un rang de 1,
le petit enfant un rang de 2, et ainsi de suite.

La #strong[hauteur] d\'un arbre est la valeur de son plus grand rang.

== Propriétés d\'un arbre
<propriétés-dun-arbre>
Un graphe est un arbre si il respecte l\'une des conditions suivantes
\(chaque condition est équivalente aux autres) :

- Le graphe est sans cycle et connexe
- Le graphe est sans cycle et comporte n-1 arêtes \(où n est le nombre
  de sommets)
- Le graphe est connexe et comporte n-1 arêtes \(où n est le nombre de
  sommets)
- Pour chaque paire de sommet est reliée par une seule chaîne simple
  \(le graphe est donc sans boucle)

Cela signifie que si un graphe connexe et qu\'il a n-1 arêtes où n est
le nombre de sommets, alors le graphe est un arbre.

Egalement tout arbre avec au moins deux sommets comporte au moins deux
feuilles.

== Arbres binaires
<arbres-binaires>
On parle de #strong[n-arbre] pour définir des abres où chaque sommet a
au plus n enfants.

Un arbre binaire est donc un 2-arbre, ce qui signifie que chaque sommet
peut avoir au plus 2 enfants.

On dit qu\'un arbre binaire est complet lorsque tous ses niveaux sont
complètement remplis.

#image("arbre-binaire-complet-incomplet.png")

Les sommets d\'un arbre binaire peuvent être appellé par des séquences
binaires.

#image("arbre-alphabet.png")

Un arbre binaire peut également être utilisé pour représenter des
expressions algébriques, tel que celles-ci :

#image("arbre-binaire-priorite-algebrique.png")

Dans ce type d\'arbre les priorités d\'opérations les plus élevées se
trouveront en haut de l\'arbre.

=== Parcours d\'un arbre binaire
<parcours-dun-arbre-binaire>
Il existe 3 types de manières de parcourir un arbre binaire.

- Le parcours #strong[préfixe] qui consiste à visiter chaque sommet
  avant ses enfants. Donc on va visiter un sommet, puis visiter
  l\'enfant de gauche de ce dernier, puis visiter l\'enfant de droite de
  ce dernier.
- Le parcours #strong[postfixe] qui consiste à visiter chaque sommet
  après ses enfants. Donc on va visiter l\'enfant de gauche du sommet,
  puis visiter l\'enfant de droite du sommet puis enfin visiter le
  sommet lui-même.
- Le parcours #strong[infixe] qui sonsite à visiter chaque sommet entre
  ses enfants. Donc on va visiter l\'enfant de gauche du sommet, puis on
  visite le sommet, puis on visite l\'enfant de droite du sommet.

=== Arbres binaires de recherche \(ABR)
<arbres-binaires-de-recherche-abr>
Un #strong[arbre binaire de recherche] est un arbre binaire où chaque
enfant de gauche est plus petit \(ou égal dépendant de l\'ordre) que son
parent, et chaque enfant de droite est plus grand que son parent \(ou
égal dépendant de l\'ordre).

Cela peut notament être utile pour trier les éléments d\'une suite. On
met tous les éléments dans l\'arbre, on peut alors ressortir une suite
triée en faisant un parcours infixe sur l\'arbre.

= Code de Prüfer \(représentation compacte d\'un arbre)
Le codage de Prüfer est une manière très compacte de représenter un
arbre. Elle permet très facilement de retrouver l\'arbre de manière
unique à partir de son codage mais est peu pratique pour parcourir
l\'arbre.

Le code de Prüfer est une suite de nombre qui fait une taille
correspondant au nombre de sommet, moins 2. Donc si on a un graphe à 10
sommet, la suite du code fera une longueur de 8.

En sachant cela on peut également faire une déduction sur les arbres en
général. Si on sait que l\'on a 8 sommets, on sait que la taille du code
de Prüfer sera 6. On peut alors calculer le nombre de permutations
\(soit d\'arbres possibles) avec ces 8 sommets, qui est de 8^6, soit 262
144 arbres possibles. On peut donc en déduire que le nombre d\'arbres
possibles avec un nombre de sommets donné est n^{n-2}.

== Codage de Prüfer
<codage-de-prüfer>
Le codage de Prüfer consiste à répeter les étapes suivantes jusqu\'a ce
qu\'il ne reste plus que #strong[deux] sommets dans l\'arbre :

+ Identifier la #strong[feuille] de l\'arbre avec le #strong[numéro
  minimum]
+ Ajouter le sommet adjacent de la feuille dans la suite
+ Enlever la feuille de l\'arbre

== Décodage de Prüfer
<décodage-de-prüfer>
Pour décoder la suite, il suffit alors de connaître la liste des sommets
et le code. Il faut alors répeter les étapes suivantes jusqu\'a ce
qu\'il ne reste plus que deux éléments dans la suite :

+ Identifier le plus petit élément de la liste des sommets qui n\'est
  pas dans le code \(la suite)
+ Relier le premier élément de la suite au sommet identifié
+ Enlever le premier élément de la suite et le sommet identifié

Une fois qu\'il ne reste plus que deux éléments, il suffit alors de les
relier.

#image("decodage-prufer.png")

Voici la description de l\'exemple ci-dessus étape par étape :

+ Situation initiale, on a un code S de `S = {2,3,3,3}` et une liste de
  sommets allant de 1 à 6.
+ On prends le plus petit sommet qui ne fait pas partie du code \(1), et
  on le lie au premier sommet du code \(2). Enfin on supprime 1 de la
  liste des sommets et 2 du code
+ On prends le plus petit sommet qui ne fait pas partie du code \(2), et
  on le lie au premier sommet du code \(3). Enfin on supprime 2 de la
  liste des sommets et 3 du code
+ On prends le plus petit sommet qui ne fait pas partie du code \(4), et
  on le lie au premier sommet du code \(3). Enfin on supprime 4 de la
  liste des sommets et 3 du code
+ On prends le plus petit sommet qui ne fait pas partie du code \(5), et
  on le lie au premier sommet du code \(3). Enfin on supprime 5 de la
  liste des sommets et 3 du code
+ Enfin, on relie les deux derniers sommets qui sont 5 et 6.

L\'arbre està présent reconstitué.

= Représentation informatique d\'un arbre
Une première manière de le représenter est avec un tableau où les lignes
représentent les enfants de chaque sommet. Et les indices représentent
donc les sommets.

Une deuxième manière est de le représenter avec des parenthèses, où
chaque sommet est représenté par son nom et ses enfants en parenthèse.

#image("arbre-parenthese.png")

Enfin une dernière manière de le représenter est avec des pointeurs. Où
chaque élément a un pointeur vers chacun de ses enfants, et on a ensuite
juste un pointeur en plus pour pointer vers la racine.

#image("arbre-pointeurs.png")

= Arbres couvrants et coloration
Un #strong[arbre couvrant] \(ou #strong[arbre maximal];) est un graphe
partiel qui est aussi un graphe.

#image("arbre-couvrant.png")

On peut également avoir un #strong[arbre couvrant de poids minimum]
lorsqu\'un graphe est pondéré où le but est de trouver un arbre couvrant
où la somme des pondérations est minimale.

Pour constuire un arbre couvrant de poids minimum, on peut utiliser
l\'aglgorithme de Kruskal.

Cet algorithme consiste simplement à trier les arêtes par leur poid, et
les ajouter dans l\'ordre dans que cela ne forme pas de boucle.

== Coloration
<coloration>
La #strong[coloration] des sommets d\'un graphe consiste à colorier des
sommets dans le nombre minimal de couleurs possible de sorte que deux
sommets adjacents ne soient pas de la même couleur.

On appelle #strong[stable] un ensemble de sommets qui ne sont pas
adjacents.

#image("coloration-graphe.png")

On parle de #strong[cardinal] d\'un stable pour décrire sa longueur. Et
le stable le plus long d\'un graphe \(donc qui a le cardinal le plus
élevé) est apellé le #strong[nombre de stabilité];.

Donc pour faire la coloration d\'un graphe il suffit donc de trouver un
ensemble de stables \(#strong[partition];), qui reprends l\'ensemble des
sommets.

Pour faire la coloration minimale d\'un graphe il faut donc avoir la
partition complète la plus minimale possible \(donc qui comprends tous
les sommets mais qui contient le plus petit nombre de stables).

Le #strong[nombre chromatique] d\'un graphe représente le nombre de
couleur minimale pour colorer les sommets du graphe. C\'est donc le
nombre de stable de la partition complète minimale de coloration.

=== Trouver le nombre chromatique par encadrement
<trouver-le-nombre-chromatique-par-encadrement>
Il est possible de trouver des indications sur le nombre approximatif de
couleurs nécessaires pour colorer un graphe en estimant le nombre
chromatique sur base de quelques règles.

Une première règle est que le nombre chromatique sera toujours plus
petit ou égal au plus grand degré des sommets plus 1. Cette règle peut
être représentée par la formule suivante où r représente le plus grand
degré de sommet et \\\( \\gamma\(G) \\) représente le nombre
chromatique.

$ gamma (G) lt.eq r + 1 $

Cette règle peut être exemplifiée par un graphe complet de 5 sommets, où
le degré le plus élevé est 4 \(chaque sommet connait les 4 autres
sommets du graphe). Et puis ce que chaque sommet est adjacent à chaque
autre sommet le nombre chromatique doit être 5, soit le degré le plus
élevé + 1.

Un autre type de #strong[majoration] \(estimation de la borne haute des
possibilité du nombre chromatique) est d\'utiliser le nombre de
stabilité. Cette règle est représentée par la formule suivante où \"n\"
représente le nombre de sommets.

$ gamma (G) lt.eq n + 1 - alpha (G) $

Cette règle peut être expliquée par le fait que le nombre de stabilité
représente le plus grand stable, et que chaque sommet d\'un stable peut
être coloré d\'une même couleur puis ce qu\'ils ne sont pas adjacents.
Par conséquent le nombre de couleurs totale sera forcément le nombre de
sommets - le nombre de sommets coloré par le stable + la couleur du
stable.

Enfin on peut encadrer un nombre chromatique par #strong[minoration];.
On peut le faire en le comparant à la taille de la plus grande clique
\(plus grand sous-graphe complet). Il suffit alors de connaître le
nombre de sommets dans la plus grande clique du graphe et on sait que le
nombre de couleurs \(nombre chromatique) sera forcément plus élevé puis
ce que dans un graphe complet il faut toujours autant de couleurs qu\'il
n\'y a de sommets.

Cette règle peut être représentée par la formule suivante, où \\\(
\\omega\(G) \\) est l\'ordre \(nombre de sommets) de la plus grande
clique \(sous-graphe complet) :

$ gamma (G) gt.eq omega (G) $

#quote(block: true)[
#strong[Attention] Je trouve que le chapitre sur la coloration est assez
complexe du coup je passe à autre chose pour le moment pour y revenir
plus tard, il faut donc considérer que cette partie n\'est pas complète.
Elle devrait cependant être suiffisante pour faire les exercices de
coloration disponible sur la plateforme.
]

= Introduction aux graphes orientés
En donnant un sens aux arêtes d\'un graphe on obtient un
#strong[digraphe];, aussi appellé #strong[graphe orienté];.

Cela signifie donc que les arêtes à la place d\'être un ensemble
non-ordonné de 2 sommet, est un ensemble ordonné de 2 sommets.

Il y a donc 3 types de degré pour un sommet dans un digraphe

- Le degré \(\( d^+ \\) qui est le #strong[degré extérieur];, soit le
  nombre d\'arcs qui vont du sommet vers un autre
- Le degré \(\( d^- \\) qui est le #strong[degré intérieur];, soit le
  nombre d\'arcs qui vont d\'un autre sommet vers celui-ci.
- Le degré total \(\\\( d \\)) comme la somme des degrés extérieur et
  intérieur.

Pour décrire les #strong[voisins] d\'un sommet on parle de
#strong[prédécesseurs] \(pour ceux où un arc part depuis eux vers le
sommet courant) et de #strong[successeurs] \(pour ceux qui sont au bout
d\'un arc qui part depuis le sommet courant).

Tout comme pour les graphes non-orienté il existe des cycles et des
chaines. Sauf que dans les graphes orientés un cycle est appellé un
#strong[circuit] et une chaîne est appellée un #strong[chemin] car elle
doit suivre une certaine direction indiquée par les arcs.

On parle de graphe #strong[acyclique] lorsqu\'un graphe ne contient
aucun circuit.

== Connectivité des graphes orientés
<connectivité-des-graphes-orientés>
Commes les arêtes sont maintenant des arcs qui ont une direction, la
connectivité des graphes devient un peu plus floue.

- Un graphe est dit #strong[fortement connexe] si il est possible de
  tracer un chemin vers n\'importe quel sommet
- Un graphe est dit #strong[faiblement connexe] lorsqu\'il est possible
  de tracer une chaine vers n\'importe quel sommet \(mais cette chaine
  n\'est pas dirigée dans le bon sens)
- Un graphe est dit #strong[non connexe] dans tous les autres cas

== Orientation d\'un graphe
<orientation-dun-graphe>
Une #strong[orientation] d\'un graphe non-orienté est un graphe orienté
obtenu en fixant un sens aux arêtes d\'un graphe non-orienté.

Un graphe non-orienté est dit #strong[fortement orientable] lorsqu\'il
est possible de trouver une orientation du graphe qui permette
d\'obtenir un digraphe fortement connexe.

== Représentation de graphes orientés
<représentation-de-graphes-orientés>
Il est possible d\'utiliser les même formes \(matrices et listes) pour
représenter des graphes fortement orientés.

La différence pour une matrice est que l\'on ne va noter une
intersection entre deux sommets si le sommet correspondant à l\'indice
de la ligne a un arc pointant vers le sommet correspondant à l\'indice
de la colonne.

Contrairement à un graphe orienté elle n\'est donc pas symétrique.
C\'est-à-dire que que l\'intersection de la ligne 3 avec la colonne 4 ne
donnera pas la même chose que l\'intersection de la ligne 4 et de la
colonne 3.

Il en vas de même pour les listes d\'adjacences où dans chaque liste on
ne va noter que les sommets que l\'on peut atteindre en suivant un arc
depuis le sommet courant.

== Digraphes sans circuits \(acycliques)
<digraphes-sans-circuits-acycliques>
Les digraphes sans circuits sont l\'équivalent des arbres et des forêts
dans les graphes non-dirigés.

On parle donc de rang pour chaque \'étage\' du graphe en partant d\'une
racine donnée. Et puis ce que les liens entre les sommets \(les arcs)
sont dirigés, on sait que pour n\'importe quelle arc, le rang du sommet
qui est à sa source sera toujours inférieur à celui de sa destination.

On applle un #strong[arbre dirigé] tout digraphe où il est possible de
trouver un chemin depuis une racine donnée vers tous les autres sommets
du graphe.

== PERT
<pert>
Si on part du tableau de tâche suivant :

#image("task-table-pert.png")

=== Création du graphe de départ
<création-du-graphe-de-départ>
Tout d\'abord on peut ouvrir GraphEditor, créer un nouveau graphe et
choisir le type \"PERT\".

Ensuite on peut créer deux sommets, un premier qui correspondra au début
du projet, et un deuxième qui correspond à la fin du projet.

#image("debut-fin.png")

Ensuite on peut ajouter un sommet qui correspondra à chaque précédence
possible.

#image("vertex-precedence.png")

Dans cet exemple, 4 correspond à la précédence A, 5 à la précédence C, 6
à la précédence A+B, 7 à la précédence D, 8 à la précédence E+F et enfin
9 à la précédence H.

Ensuite on peut relier les arcs \(les tâches) en précisant leur lettre +
leur durée en les liant depuis leur précédence vers la précédence dont
ils vont faire partie.

Les tâche sans successeurs seront liées à la fin de projet, et les
tâches sans prédesseurs partiront du premier sommet.

#image("linking-precedents.png")

Dans le graphe précédent, il manque cependant quelque chose, on peut
voir que le sommet 6, censé correspondre à A+B ne correspond qu\'a B. Il
faut donc pouvoir ajouter une dépendence depuis A à l\'aide d\'une tâche
fictive.

Pour cela on va ajouter une tâche nommée \"X\" de durée 0 qui va du
précédent A \(sommet 4) au précédent A+B \(sommet 6)

#image("linking-complete.png")

Pour finir la construction du graphe il faut encore s\'assurer que
toutes les directions vont vers le bas afin de pouvoir numéroter les
sommets par étage.

#image("sommets-numerotes.png")

=== Assignation des valeurs de début
<assignation-des-valeurs-de-début>
La valeur de début du premier sommet est 0.

Pour chaque sommet dans l\'ordre des numéros, on va regarder chaque
précédente du sommet et regarder lequel a la durée + début le plus
élevé. Cette valeur deviendra alors la valeur de début du sommet.

Donc si un sommet a 2 précédents, la tâche B \(durée \= 5, début \= 0)
et la tache X \(durée \= 0, début \= 4), alors la valeur de début sera 5
car 5+0 est plus grand que 4+0.

#image("graphe-debut.png")

=== Assignation des valeurs de fin
<assignation-des-valeurs-de-fin>
La valeur de fin du dernier sommet sera la même que celle de début.

Ensuite, pour chaque sommet à partir de l\'avant-dernier, pour chaque
successeur du sommet, on regarde la date de fin du successeur - la
durée. La valeur la plus minimale sera la date de fin du sommet actuel.

Donc si on a un sommet qui a 4 successeurs : la tâche X \(durée \= 0,
fin \= 14), la tâche C \(durée \= 4, fin \= 9), la tâhce E \(durée \= 5,
fin \= 10) et la tâche D \(durée \= 6, fin \= 10). On aura une date de
fin de 4, car 10-6 est la différence la plus petite comparée à 10-5, 9-4
ou 14-0.

#image("graphe-date-fin.png")

=== Trouver le chemin critique
<trouver-le-chemin-critique>
Le chemin critique est le chemin qui contient toutes les tâches les plus
critiques pour le projet. Donc toutes les tâches qui, si elles prennent
du retard, entraine un retard sur l\'ensemble du projet.

Les tâches/arc critiques sont celles qui connecte les sommets ayant la
même date de début et de fin.

Ces arcs ont également comme propriété que leur durée est égale à la
différence de date de début entre les deux sommets qu\'elle connectent.

Dans GraphEditor, on peut alors mettre une épaisseur de 2 à tous les
arcs/tâches qui font partie du chemin critique.

#image("graphe-pert-final.png")

Et voilà, le graphe PERT est à présent tracé. On peut donc dire que les
tâches critiques sont A, D, H et J.

= Cryptographie à clé secrète
Un #strong[cryptosystème symétrique] est défini par une clé à garder
secret qui sert à chiffrer et déchiffrer des messages grâce à une
#strong[fonction de chiffrement] et une #strong[fonction de
déchiffrement];.

La clé secrète est donc partagée uniquement de l\'émetteur et du
recepteur. Ce type de système est très ancien existe au moins depuis
l\'antiquité. Il est aussi améliorer par d\'avantage de sécuirté grâce à
la puissance de calcul des ordinateurs d\'aujourd\'hui.

== Chiffre de César
<chiffre-de-césar>
Le chiffre de césar est simplement un décalage de lettre. La clé est
donc simplement le décalage des lettres d\'un certain nombre, à la base
3. Ainsi un A devient un C, un B devient un D, etc. Et une fois arrivé à
Z, on recommence, donc un Z devient un B.

Cette méthode de chiffrement est très peu sûr mais est tout de même
resté jusque dans les années 800.

Ce code peut être cassé très simplement par soit l\'essai-erreur \(il
n\'y a quand même que 26 clé possible), soit par l\'analyse
fréquentielle qui consiste à regarder quelles lettres apparaissent le
plus souvent, et en déduire le décalage.

Par exemple, si on sait que le message chiffré est écrit en français et
que la lettre la plus commune est le G, on peut en déduire que cette
lettre doit en vérité être un E car c\'est la lettre la plus commune en
français.

On peut donc déduire que le décalage de E à G est 2, et décoder le
message.

== Chiffre de Vigenère
<chiffre-de-vigenère>
Le chiffre de Vigenère est une amélioration du code de César qui
consiste à utiliser un ensemble de lettres comme clé.

#image("vigenere-cipher.png")

Cela consiste simplement à répeter la clé de façon à l\'alonger à la
taille du message, puis d\'additioner les lettres. Ensuite on converi
les lettres en chiffres de position dans l\'alphabet \(où `A = 0` et
`Z = 25`), puis on les additionne et on les retransforme en lettre. Par
exemple : `C + L = (3 + 11) % 26 = 13 % 26 = 13 = N`, ou encore
`X + L = (23 + 11) % 26 = 34 % 26 = 8 = I`

En répetant cette opération pour chaque lettre on obtient donc le
cryptogramme, il suffit alors de faire l\'opération inverse pour
retrouver le message en clair. Par exemple
`I - L = (8 - 11) % 26 = -3 % 26 = 23 = X`.

Il est possible de craquer le code de Vigenère en utilisant la
#strong[méthode de Kasiski] qui consiste à deviner la longueur de la clé
en repérant des séquences de lettres répetées suseptible de correspondre
à des mots courants. Ensuite on peut effectuer une #strong[analyse
fréquentielle] aux positions qui sont à un multiple de la longueur de la
clé.

== Substitutions homophonique
<substitutions-homophonique>
Un autre type de code est le code à substitution homophonique. Qui
consiste à définir un code pour chaque lettre en fonction de sa
fréquence dans la langue. Ainsi un E aura plus de code possibles
associés.

Ensuite il suffit de remplacer chaque lettre par n\'importe quel code de
la liste pour la lettre. Ainsi cela rends impossible de faire une
analyse fréquentielle sur le code.

== PLAYFAIR
<playfair>
Playfair est un algorithme de chiffrement par substitution sur des
digrammes. Voici en quoi il consiste :

=== Création de la matrice
<création-de-la-matrice>
+ On prends une clé, exemple `DANSEZ MAINTENANT JULES`
+ On supprime les doublons ainsi que la lettre J : `DANSEZMITUL`
+ On complète avec le reste de l\'alphabet \(en évitant toujours les
  doublons et la lettre J) : `DANSEZMITULBCFGHKOPQRVWXY`
+ On met cela dans une matrice carrée de 5 par 5

```
DANSE
ZMITU
LBCFG
HKOPQ
RVWXY
```

=== Chiffrement
<chiffrement>
+ On trouve un message à chiffrer, par exemple
  `DEMANDE RENFORTS IMMEDIATEMENT`, on s\'assure que le message n\'a pas
  de lettre J
+ On découpe le message par groupe de deux lettres
  `DE MA ND ER EN FO RT SI MM ED IA TE ME NT`
+ Si un groupe a deux fois la même lettre, on interpose un X :
  `DE MA ND ER EN FO RT SI MX ME DI AT EM EN T`
+ Si une lettre est seule, on ajoute un X :
  `DE MA ND ER EN FO RT SI MX ME DI AT EM EN TX`
+ Ensuite pour chaque groupe de deux lettres, on regarde dans la matrice
  où elles sont.
  - Si les deux sont sur la même ligne, on prends la lettre
    immédiatement à droit de chaque lettre
  - Si les deux sont sur la même colonne, on prends la lettre
    immédiatement en dessous de chaque lettre
  - Sinon, on considère les deux commes les coins opposés d\'un
    rectangle et on prends la lettre du coin opposé verticalement
+ On obtient un certain résultat :
  `AD BM SA YD DS PC ZX TN VT AU ZN MS UA DS FS`

=== Déchiffrement
<déchiffrement>
+ Pour chaque élément du message à déchiffrer, on procède dans
  l\'inverse du processus de chiffrement
  - Si les deux sont sur la même ligne, on prends la lettre
    immédiatement à gauche de chaque lettre
  - Si les deux sont sur la même colonne, on prends la lettre
    immédiatement en haut de chaque lettre
  - Sinon, on considère les deux commes les coins opposés d\'un
    rectangle et on prends la lettre du coin opposé
+ On supprime les éventueles X en trop dans le résultat du message et on
  ajoute les éventuels J manquant

== ADFGVX
<adfgvx>
#image("adfgvx-matrix.png")

=== Chiffrement
<chiffrement-1>
+ On trouve un message à encoder, exemple `DEMANDE RENFORTS`
+ Pour chaque lettre, on trouve quel est sa correspondance ligne-colonne
  dans la matrice. Par exemple ici :
  `VD-GG-FX-DF-XD-VD-GG-XG-GG-XD-GF-VF-XG-VX-XF`
+ On écrit la clé dans une grille et on insère le résultat de l\'étape
  précédente à l\'intérieur
+ On trie les colonnes par ordre alphabétique des lettres de a clé
+ On concatène chaque colonne après éventuellement avoir ajouté un A de
  remplissage dans le tableau

#image("resultat-chiffrement-adfgvx.png")

=== Déchiffrement
<déchiffrement-1>
+ On divise le nombre de caractère du code par le nombre de caractère de
  la clé, ce qui arrondi à la hausse donne le nombre de ligne de la
  grille
+ On écrit la clé avec ses lettres triée par ordre alphabétique
+ On ajoute le message chiffré par colonne
+ On trie les colonnes pour revenir à la clé de départ
+ On cherche la correspondance de chaque couple dans la matrice pour
  revenir au message de départ

= Cryptographie à clé publique
Le problème principal des cryptosystèmes à clé secrète est la clé car il
faut avoir une clé par couple émetteur-récepteur ce qui dans certaines
situation peut être très embétant, et il faut préalablement communiquer
ces clés de manière sécurisée.

De plus elles ne permettent pas de vérifier l\'identité d\'une personne
ayant écrit le message, ni l\'intégrité du message transmis.

Aujourd\'hui les cryptosystèmes doivent être confidentiels, authentifiés
\(on doit pouvoir être sur de qui a écrit un message), intègre \(un
message ne peut pas être modifié durant la transmission), et
non-répudiable \(un auteur ne peut pas nier avoir écrit un message).

Les #strong[cryptosystèmes à clé révélée] permettent de résoudre ce
problème en ayant deux clés, une clé privée/secrète utilisée pour
déchiffrer ou signer des messages et une clé publique/révèlée utilisée
pour chiffrer ou vérifier des messages.

== Propriétés des fonctions
<propriétés-des-fonctions>
Les fonctions de chiffrement et de déchiffrement doivent donc avoir
certaines propriétés pour supporter deux clés.

+ Pour tout message, le déchiffrement d\'un message chiffré doit
  retourner l\'exact message encodé
+ La connaissance de la clé de chiffrement ne doit pas permettre de
  calculer le message dans un temps raisonnable
+ Ce point est plus optionel et utilisé pour les signatures : si on
  applique la fonction de chiffrement sur le résultat de la fonction de
  déchiffrement d\'un message, on retrouve le message initial.

La fonction de chiffrement est ici appellée une #strong[fonction
trappe];, c\'est une fonction à sens unique qui a besoin d\'une autre
fonction pour pouvoir inverser son opération.

Un fonction #strong[permutation trappe] est une fonction qui vérifie les
3 conditions.

== RSA
<rsa>
RSA est probablement le chiffrement à cryptosystème à clé publique le
plus connu et utilisé partout sur internet et ailleurs.

Il se base sur le fait qu\'il est très facile et rapide de calculer une
puissance élevée de nombre mais qu\'il est très compliquée sur base du
nombre en question de retrouver la base utilisée.

Vous pouvez trouver plus de détails sur le fonctionnement de RSA vers la
fin de ma synthèse d\'OS où j\'ai également mis diverses ressources, y
compris Khan Academy pour pouvoir en apprendre plus sur la cyrptographie
asymétrique ou la cryptographie en général.

== Système de cryptographie asymétrique de carte bancaire
<système-de-cryptographie-asymétrique-de-carte-bancaire>
Pour vérifier payer avec une carte bancaire, le terminal de paiement
n\'a pas forcément besoin d\'être connecté à Internet pour vérifier les
transactions. Cela peut se faire via divers mécanisme mais en voici
quelques exemples:

Le chiffrement asymétrique utilisé par la carte est du RSA 1024 bits.

=== Vérification avec les informations d\'identification
<vérification-avec-les-informations-didentification>
Tout d\'abord le terminal va demander de saisir le code confidentiel,
une fois entré, celui-ci est communiqué à la carte.

La carte va vérifier la validité du code confidentiel, si celui-ci est
correct, le terminal va lire la clé publique signée par une autorité de
certification. Cette dernière est stoquée sur la carte.

A l\'aide de la clé publique de l\'autorité de certification, le
terminal va vérifier et récupérer la clé publique.

Le terminal va alors récupérer l\'empreinte signée de la carte, cette
empreinte comporte les informations d\'identification de la carte et est
signée avec la clé secrète de la carte.

Le terminal va alors vérifier l\'empreinte et récupérer les informations
d\'identifications en utilisant la clé publique de la carte réupérée
plus tot.

Si les informations sont bonnes, tout est bon et la carte continue.

=== Vérification par challenge
<vérification-par-challenge>
Une autre manière de faire pour vérifier la validité d\'une carte est
d\'envoyer un message aléatoire à signer à la carte. La carte va alors
la signer/chiffrer avec sa clé secrète et le terminal va alors vérifier
que l\'information déchiffrée/vérifiée correspond bien à celle envoyée
au départ.

Cela a l\'avantage que contrairement aux informations de la carte qui ne
change pas, celui-ci est dynamique, ainsi si quelqu\'un intercepte les
données lors de la transaction, il ne pourra pas simuler une nouvelle
transaction.

=== Vérification par clé secrète par une banque
<vérification-par-clé-secrète-par-une-banque>
Dans certain cas on peut également utiliser la cryptographie
symmétrique, notament pour les paiements conséquents ou fait en ligne.

Pour ceux-ci la banque envois un message aléatoire à la carte, la carte
va chiffrer ce message avec une clé secrète stockée sur la carte et
renvoyer cette information à la banque.

La banque va alors déchiffrer l\'information avec la même clé secrète
\(qui était également connue par la banque) et vérifier que le résultat
est bien le même que le message envoyé au départ.

Le chiffrement est ici fait en triple-DES ou AES.

== ElGamal
<elgamal>
Pour trouver la clé partagée dans ElGamal, on procède comme suis :

+ Sur base d\'un ordre de groupe cyclique et d\'un élément générateur.
  Imaginons, un ordre `n = 30` et un générateur `g = 3`
+ La clé privée de chaque partie est n\'importe quel élément entre 1 et
  `n`. Cette clé privée sera appellée `Ka`
+ La clé publique correspond à `Ha = g^Ka mod (n + 1)`
+ Répeter le processus pour l\'autre partie
+ Trouver la clé secrète partagée en faisant `K = Ha^Ka mod (n + 1)`,
  soit la clé publique exposant la clé privée dans le groupe cyclique.

Ensuite pour chiffrer un message faisant partie du groupe cyclique, on
fait simplement `m' = m * K mod (n + 1)`, soit multiplier le message par
la clé partagée dans le groupe cyclique.

Pour la déchiffrer on peut alors faire un inverse modulaire de la clé
publique exposant la clé privée. Soit `m = m' * K^-1`.

L\'inverse modulaire peut être trouvé en résolvant l\'identité de
Bachet-Bézout pour `1 = ax + by = Ka + nb` pour y trouver a.

```
         b   a                           
     23  1   0                              
(-5)  4  0   1                           
(-1)  3  1  -5                      
      1  -1  6 <-- Valeur de a = 6
      
=> 4^-1 mod 23 = 6
```

