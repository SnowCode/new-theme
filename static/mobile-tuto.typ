
#show raw: c => {
  set text(font: "CMU Typewriter Text")
  if (c.block == true) {
    block(fill: rgb("#EDF0F2"), inset: 8pt, radius: 5pt, width: 100%, c)
  } else {
    box(fill: rgb("#EDF0F2"), inset: 2.5pt, radius: 3pt, baseline: 20%, c)
  }
}

#show image: i => {
  set image(height: 15em, fit: "contain")
  align(center, i)
}


#show outline.entry.where(level: 2): e => {
  v(5pt, weak: true)
  strong(e)
}


= Tuto de projet Android
<tuto-de-projet-android>

#outline(depth: 4, indent: 2em)

== TODO Analyse
<analyse>
== Setup initial du projet
<setup-initial-du-projet>
Avant toute chose pour faire le projet, il vous faudra Android Studio.
Une fois installé il faut suivre la procéudre pour installer Android
SDK, ça prends un certain temps mais c\'est la partie la plus longue.

Enfin pour créer votre premier projet, il vous faudra demander à Android
Studio de créer un \"Empty Views Activity\"

#image("Tuto_de_projet_Android/2024-04-27_17-35-46_screenshot.png")

=== Installation des dépendences et configuration du projet
<installation-des-dépendences-et-configuration-du-projet>
Une fois cela fait, il va prendre un certain temps pour ce configurer
tout seul. Une fois que c\'est fini, vous pouvez ouvrir le fichier
`app/build.gradle.kts` et dans la section des dépendences ajouter les
lignes suivantes :

```kotlin
// Dépénendences pour la création de fragments, c'est à dire des écrans
implementation("androidx.fragment:fragment-ktx:1.6.2")
implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.7.0")

// Dépendence pour la navigation entre les différents fragments
implementation("androidx.navigation:navigation-fragment-ktx:2.7.7")
implementation("androidx.navigation:navigation-ui-ktx:2.7.7")

// Dépendences pour les coroutines/threads (possiblement pas nécessaire)
implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.0")
implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.8.0")

// Dépendences pour l'accès à la base de donnée de l'application
implementation("androidx.room:room-runtime:2.6.1")
implementation("androidx.room:room-ktx:2.6.1")
ksp("androidx.room:room-compiler:2.6.1")
```

Ensuite il faut ajouter certains plugins au début de ce fichiers

```kotlin
// Utile pour passer des arguments/données entre les différents écrans
id("androidx.navigation.safeargs")

// Nécessaire pour importer le compilateur de Room qui va interpreter certaines annotations
id("com.google.devtools.ksp")
```

Ensuite pour chacun de ces plugins, il faut encore aller modifier le
fichier `build.gradle.kts` à la racine pour aller importer la bonne
version.

```kotlin
id("androidx.navigation.safeargs.kotlin") version "2.7.7" apply false
id("com.google.devtools.ksp") version "1.9.0-1.0.13" apply false
```

Enfin on peut retourner dans le fichier `app/build.gradle.kts` pour
ajouter des fonctionalités de build qui permetteront d\'automatiquement
bind des champs et méthodes du ViewModel à éléménts ou évènemnets de la
vue.

```kotlin
buildFeatures {
    dataBinding = true
    viewBinding = true
}
```

=== Configuration de la navigation entre fragments
<configuration-de-la-navigation-entre-fragments>
Ensuite on peut créer une nouvelle ressource de type \"navigation\" que
l\'on peut appeller navGraph.

#image("Guide_Android/2024-04-26_11-29-10_screenshot.png")

#image("Guide_Android/2024-04-26_11-29-39_screenshot.png")

Une fois que le plan de la navigatoin est établi, il faut définir
l\'utilisation de la navigation dans le `activity_main.xml` en utilisant
un `FragmentContainerView` comme ceci :

```xml
<androidx.fragment.app.FragmentContainerView xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/fragment_container"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:name="androidx.navigation.fragment.NavHostFragment"
    app:defaultNavHost="true"
    app:navGraph="@navigation/nav_graph" />
```

Le MainActivity.kt lui a besoin d\'être nettoyé de ce qu\'il avait avant
comme ceci:

```kotlin
package be.helmo.placereport

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import be.helmo.placereport.databinding.ActivityMainBinding

class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
```

== Création des modèles
<création-des-modèles>
La prochaine étape est maintenant de créer les modèles qui ont été
défini dans la phase d\'analyse, dans l\'application. Pour cela on peut
créer un nouveau package `models`. Puis créer des classes correspondant
à chaque modèle en les annotant avec les annotations de Room. Cela
permettera ainsi de générer la base de donnée automatiquement.

```kotlin
// Cette classe va être placée dans le package model
package be.helmo.placereport.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date
import java.util.UUID

// L'annotation @Entity est ce qui va indiquer à Room que c'est une entité à mapper en base de donnée
// L'annotation @PrimaryKey indique quel champ sera utilisé comme clé primaire de la base de donnée
// Cette classe est une "data class" ce qui est globalement l'équivalent d'un record
@Entity
data class Place(@PrimaryKey val id: UUID, val name: String, val date: Date)
```

Avant de pouvoir créer la base de donnée en elle même, on doit prendre
le soin de convertir le type \"Date\" en quelque chose pouvant être
stoqué en base de donnée \(la base de donnée ne supporte pas/mal ce
type). Pour cela on peut créer un package `database` \(qui contiendra
aussi plus tard la base de donnée) ainsi qu\'un fichier
`DateConverter.kt`

```kotlin
package be.helmo.placereport.database

import androidx.room.TypeConverter
import java.util.Date

// Cette classe définit simplement des méthodes from et to Date pour faire correspondre un Date en Long dans la base de donnée
class DateConverter {
    @TypeConverter
    fun fromDate(date: Date): Long{
        return date.time
    }

    @TypeConverter
    fun toDate(timeInMillis: Long): Date{
        return Date(timeInMillis)
    }
}
```

== Création des DAO
<création-des-dao>
Pour que l\'on puisse récupérer des objets de la base de donnée, il faut
créer un objet DAO \(Data Access Object) qui va lier les queries pour
faire les différentes opérations pour chaque objet sur la base de donnée
\(CRUD : Create Read Update Delete). Pour cela on peut créer un fichier
`PlaceDao.kt` dans database.

```kotlin
package be.helmo.placereport.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import be.helmo.placereport.model.Place
import kotlinx.coroutines.flow.Flow
import java.util.UUID

// L'annotation Dao est là pour signifier à Room qu'il peut automatiquemnet implémenter cette interface
@Dao
interface PlaceDao {
    // L'annotation Query représente ce qui va être lancé sur la base de donnée pour récupérer des objets
    // Si on utilise juste List<Place> il faudrait retourner toutes les valeurs d'un coup, tandis que le Flow permet de les calucler de manière asynchrone
    @Query("SELECT * FROM place")
    fun getPlaces(): Flow<List<Place>>

    // Le mot clé suspend est utilisé pour indiquer que la méthode doit obligatoirement être faite dans une coroutine
    @Query("SELECT * FROM place WHERE id=(:id)")
    suspend fun getPlace(id: UUID): Place

    // Pour ce qui est de l'update et du delete, il n'y a pas besoin d'écrire la requète SQL soit même, il suffit d'utiliser les annotations adéquates
    @Update
    suspend fun updatePlace(place: Place)

    @Insert
    suspend fun addPlace(place: Place)
}
```

== Création de la base de donnée
<création-de-la-base-de-donnée>
Une fois cela fait, il faut maintenant demander à Room de créer la base
de donnée, on va par exemple ajouter une classe `PlaceDatabase` par
exemple. Contrairement aux DAO il n\'y a seulement besoin que de créer
une seule classe de base de donnée pour tous les modèles. En revanche il
est nécessaire d\'avoir un DAO par modèle.

```kotlin
package be.helmo.placereport.database

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import be.helmo.placereport.model.Place

// Cette annotation permet de spécifier les différentes entités à inclure dans la base de donnée
// Cette base de donnée est marquée comme étant la première version et le "exportSchema" à false permet d'éviter certains warnings
@Database(entities = [Place::class], version=1, exportSchema = false)
// Ici on peut lier la base de donnée au convertisseur que l'on a créé précédemment pour permettre à la base de donnée de supporter de nouveaux types
@TypeConverters(DateConverter::class)
abstract class PlaceDatabase : RoomDatabase() {
    // Cette méthode abstraite va être automatiquement implémentée par Room pour implémenter un Dao
    abstract fun placeDao(): PlaceDao

    // Un "companion object" permet de ne pas avoir à instancier la classe pour utiliser ses éléments (un peu comme "static" dans d'autres langages)
    companion object {
        // On définit ici une seule instance de la base de donnée, assurant donc que cela est un singleton (assure qu'il n'y a qu'un seul élément)
        private var INSTANCE : PlaceDatabase? = null
        // Ici on définit le nom de la base de donnée dans une constante
        val DATABASE_NAME = "place-report-database"

        // On crée une méthode factory qui va créer la base de donnée
        fun create(context: Context) {
            Log.d("PlaceDatabase", "create PlaceDatabase")
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                PlaceDatabase::class.java,
                DATABASE_NAME)
                .build()
        }

        // On crée une méthode pour récupérer la base de donnée qui va retourner l'instance ou retourner une exception si elle est à null
        fun get(): PlaceDatabase {
            return INSTANCE ?: throw IllegalStateException("PlaceDatabase must be initialized")
        }
    }
}
```

Maintenant qu\'on a une entité, un dao et des méthodes pour créer la
base de donnée, il faut maintenant initialiser la base de donnée au
début de l\'application. Cela ne se fait généralement pas dans des
Activités ou dans des fragments car cela se répeterait tout le temps, ce
qui n\'est pas pratique.

On peut alors créer une classe Kotlin d\'application :

```kotlin
// La classe se trouve dans le package principal
package be.helmo.placereport

import android.app.Application
import android.util.Log
import be.helmo.placereport.database.PlaceDatabase
import be.helmo.placereport.database.PlaceRepository

// La classe hérite de Application qui est la méthode qui est généralement appellée lors de l'ouverture de l'application
class PlaceReportApplication: Application() {

    // Il est cependant possible de surcharger certaines méthodes, par exemple celle de l'ouverture de l'application où on va créer la base de donnée
    override fun onCreate() {
        super.onCreate()
        PlaceDatabase.create(this)
    }
}
```

On doit alors définir que cette nouvelle surcharge de l\'application
doit être utilisée par défault, pour cela il suffit d\'ajouter une ligne
pour préciser le nom de la nouvelle application dans le
AndroidManifest.xml

```xml
<application android:name="be.helmo.placereport.PlaceReportApplication" ...
```

== Création des Repository
<création-des-repository>
Une fois que la base de donnée est prête il faut donner une manière aux
viewmodels d\'intéragir simplement avec la base de donnée car les DAO
tendent à être très bas-niveau et donc peu pratique. Les repository eux
sont parfait pour faire plus de traitement. On peut donc créer un
Repository par modèle \(aussi appellé entité).

On peut donc créer une classe de Repository comme ceci :

```kotlin
// La classe a un constructeur private (qui ne peut donc être utilisé que dans des méthodes de constructions du companion object, comme les méthodes factory static en java).
// La classe prends le coroutine scope (qui indique la durée de vie maximale des threads) et la base de donnée en argument
class PlaceRepository private constructor(private val database: PlaceDatabase, private val coroutineScope: CoroutineScope = GlobalScope) {

    // Ici on peut définir des méthodes qui vont intéragir avec le dao pour faire les différentes actions
    fun getPlaces(): Flow<List<Place>> = database.placeDao().getPlaces()

    // Cette méthode nécessite la création d'un thread pour pouvoir être exécutée (à cause du suspend et du fait qu'elle appelle une méthode suspend)
    suspend fun getPlace(id: UUID): Place {
        Log.d("PlaceRepository", "getPlace started ${id}")
        val place = database.placeDao().getPlace(id)
        Log.d("PlaceRepository", "getPlace ended ${place.name}")
        return place
    }

    // Cette méthode crée un thread et exécute la modification d'un élément. On fait cela car on attends pas spécialement de réponse tout de suite, on peut ainsi créer un thread qui va faire la modification et continuer le reste des opérations
    fun updatePlace(place: Place) {
        coroutineScope.launch {
            database.placeDao().updatePlace(place)
        }
    }

    // La méthode appPlace est définie avec suspend car elle retourne une réponse avant de continuer à exécuter les opérations
    suspend fun addPlace(place: Place) = database.placeDao().addPlace(place)

    // De manière similaire à la base de donnée on définit un singleton pour le repository ainsi qu'une méthode get qui va soit créer un repository sur base de la base de donnée, ou alors retourner l'instance existante.
    companion object {
        private var INSTANCE : PlaceRepository? = null

        fun get(): PlaceRepository {
            if(INSTANCE == null)
                INSTANCE = PlaceRepository(PlaceDatabase.get())
            return INSTANCE!!
        }
    }
}
```

== Création des ViewModels
<création-des-viewmodels>
Les ViewModels sont des classes qui permettent de séparer le modèle de
la vue de façon à ce que la vue n\'ai accès que à ce qu\'elle a besoin.
Dans ce cas ci nous allons également utiliser des bindings automatique
afin de pouvoir simplifier la liaison entre la vue et les viewmodels.
Toutes les classes de viewmodels peuvent être placé dans un package
\"viewmodel\" pour avoir une certaine structure.

Premièrement dès qu\'un écran aurait besoin de quelque chose, par
exemple une élément d\'une liste à afficher, à la place de lui passer le
modèle lui-même, on va lui passer un \"viewmodel\" qui va représenter
les informations du modèle dont la vue a besoin.

```kotlin
// Ici on crée une classe de viewmodel pour substituer l'entité place pour la vue. On lui passe donc une place dans un constructeur privé, puis on extrait les inforamations nécessaires à la vue dans des attributs final (val)
class PlaceListItemViewModel(private val place: Place) {
    // DateFormatter est une classe créée à part (qui ne sera pas décrite ici) mais qui contient simplement une méthode pour converitr une date en string et inversément
    val df: DateFormatter = DateFormatter()

    // Ici on converti la date en string pour être affiché dans la vue afin de minimiser le traitement que la vue a à faire
    val date = df.fromDate(place.date)

    val id = place.id
    val name = place.name
}
```

Lorsque le viewmodel devra être utilisé, il faudra donc convertir les
modèles en viewmodel avant de les rendre disponible pour la vue. Voici
un exemple à partir d\'un Flow d\'une liste de Place à transformer en
PlaceListItemViewModel :

```kotlin
// Ceci est la base d'un code que l'on a vu plus tot sur les bases de données
placeRepository.getPlaces().collect {
        // Mais on ajoute ici un stream et un map afin de le convertir dans une liste de viewmodel plus tot qu'une liste de places
        _places.value = it.stream().map {
                PlaceListItemViewModel(it)
        }.collect(Collectors.toList())
}
```

Ensuite nous allons pouvoir créer le ViewModel principale qui sera lié à
un écran \(il faut donc un ViewModel par écran de l\'application)

```kotlin
// Le constructeur de cette classe prends une lambda "onChangeDate" en argument, cela permet au viewmodel d'exécuter du code injecté depuis l'extérieur
// Dans ce cas ci pour naviguer vers un nouveau fragment (celui de la modification de la date)
class PlaceDetailViewModel(placeId : UUID, private val onChangeDate: (date: Date) -> Unit) : ViewModel() {
    // On récupère le repository créé plus tot à l'aide de sa fonction
    private val placeRepository = PlaceRepository.get()

    val df: DateFormatter = DateFormatter()

    // On garde en attribut PRIVATE les modèles
    private var _place: Place? = null

    // On défini ici les champs à bind en MutableStateFlow, si on veut qu'il soit lisible mais non modifiable on peut le mettre en "StateFlow" à la place
    val name: MutableStateFlow<String> = MutableStateFlow("")
    val date: MutableStateFlow<String> = MutableStateFlow("")

    init {
        viewModelScope.launch {
            Log.d("PlaceDetailViewModel", "init started with $placeId")
            // On récupère les modèles depuis les repository pour les placer dans les attributs PRIVATE
            val place = placeRepository.getPlace(placeId)
            _place = place

            // On peut alors les initialiser ici ↓
            name.value = place.name
            date.value = df.fromDate(place.date)

            Log.d("PlaceDetailViewModel", "init ended ${place.name}")
        }
    }

    // Méthode qui sera appellée depuis la vue et qui appellera la lambda passée en paramètre pour aller dans un nouveau fragment
    // On peut alors définir des méthodes comme celle ci qui pourra être appellée lors d'un click sur le layout (on verra ça plus tard)
    fun changeDate() {
        _place?.let {
            onChangeDate(it.date)
        }

    }

    // Ensuite on peut utiliser la méthode onCleared pour sauvegarder les informations dans la base de donnée une fois que l'on quitte le fragment
    // On va alors récupérer les données du bind dans une nouvelle entité Place puis demander au repository de la modifier en base de donnée
    override fun onCleared() {
        super.onCleared()

        // Si le modèle n'est pas null, alors on en fait une copie que l'on modifie avec les attributs modifiés de la vue et on demande au repository de le mettre à jour
        _place?.let {place ->
            df.toDate(this.date.value)?.let { _place = place.copy(name = this.name.value, date = it) }
            placeRepository.updatePlace(_place!!)
        }
    }
}

// Enfin on peut créer un viewmodel factory qui override une méthode de ViewModelProvider.Factory afin de pouvoir créer un viewmodel depuis un fragment
class PlaceDetailViewModelFactory (private val placeId: UUID, private val onChangeDate: (date: Date) -> Unit) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PlaceDetailViewModel(placeId, onChangeDate) as T
    }
}
```

Une fois que les ViewModels sont défini on peut commencer à créer des
layouts.

== Création des layouts
<création-des-layouts>
Une fois que les viewmodels sont créé, nous pouvons maintennat créer les
layouts qui seront la vue même de l\'application. Pour cela, nous allons
utiliser des ConstraintLayout qui sont la méthode recommandée pour faire
des layouts Android.

#image("Guide_Android/2024-04-26_12-45-59_screenshot.png")

Ensuite on peut indiquer le nom du layout du fragment qui doit être
préfixé par `fragment_`

#image("Guide_Android/2024-04-26_12-46-35_screenshot.png")

D\'ici on peut alors ajouter de nouveaux éléments, il est généralement
recommandé d\'utiliser un ConstraintLayout aujourd\'hui pour les
éléments. Le constraint layout a nottament l\'avantage de permettre de
positioner les éléments visuellement avec des flèches.

#image("Guide_Android/2024-04-26_15-58-13_screenshot.png")

Mais tout d\'abord il faut définir le viewModel afin de pouvoir avoir un
binding automatique depuis le modèle.

```xml
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools">
    <data>
        <variable
            name="viewModel"
            type="be.helmo.placereport.viewmodel.PlaceDetailViewModel" />
    </data>

    <!-- Mettre le constriant layout ici ! -->
</layout>
```

Grâce à ce binding on peut alors récupérer des propriétés du viewmodel
en faisant `@{viewModel.nomDeLaPropriete}` ou encore bind un champ à une
propriété avec `@={viewModel.nomDeLaPropriete}`. Il est aussi possible
d\'attacher des évènements au clic de cette façon :
`android:onClick="@{() -> viewModel.methodeAAppeller()}"`.

En dehors du binding on peut définir des éléments supplémentaires dans
le code XML du layout tel que l\'ID \(si on souhaite rendre l\'élément
dynamique) grâce à `android:id="@+id/hello_text"`.

Il est également possible de set des propriétés, tel que la valeur pour
qu\'elle ne s\'affiche que dans le preview d\'android studio mais pas
dans l\'application elle-même \(dans le but de rendre l\'élément
dynamique plus tard) avec
`tools:text="This text will only display in the preview"`

Il est aussi possible de récupérer des messages à afficher via les
ressources string.xml. Dans un layout on peut spécifier
`@string/your_message` et faire alt+enter dessus pour créer un nouveau
message. L\'avantage de ne pas avoir des strings directement dans le
layout ou dans le code est que cela simplifie grandement les traductions
de l\'application car il est possible d\'avoir plusieurs fichiers
strings différents et d\'en sélectionner un automatiquement avec la
locale.

,\#+DOWNLOADED: screenshot \@ 2024-04-26 15:46:40
#image("Guide_Android/2024-04-26_15-46-40_screenshot.png")

Il est également possible d\'y accéder depuis un Fragment avec
`getString`

```kotlin
getString(R.string.your_message)
```

=== Création d\'un layout variable
<création-dun-layout-variable>
Lorsque l\'on crée un layout, il est possible que ce soit difficile de
faire en sorte qu\'il fonctionne bien sur tous les écrans et avec toutes
les orientations.

Il est donc possible de définir des layouts alternatifs qui seront
utilisés en cas d\'autres dimensions ou d\'autres orientations. Il
suffit de faire clic droit sur le dossier layout, puis \"New \> Layout
Resource File\"

#image("Guide_Android/2024-04-26_16-13-36_screenshot.png")

Et c\'est tout ! Il n\'y a rien d\'autre à faire, le bon layout sera
sélectionné automatiquement en fonction de la situation. Il est
d\'ailleurs possible de faire cela pour tous les fichiers de ressources,
c\'est nottament comme cela que les traductions fonctionnes car il
suffit de faire un fichier de string.xml alternatif en fonction de la
locale \(la langue) de l\'appareil.

== Création des Fragments
<création-des-fragments>
Une fois que le layout est prêt on peut maintenant créer le fragment. On
peut donc créer un package `view` pour contenir les fragments et y
ajouter une nouvelle classe.

```kotlin
// Cette classe est présente dans le package "view"

class TestFragment : Fragment() {
    // On définit le binding de la classe comme ceci
    // La classe FragmentTestBinding est une classe automatiquement générée à partir du layout
    // Cela nous permet d'accéder simplement, via les ID aux différents composants du layout pour y ajouter des évènemnets ou les modifier
    private lateinit var binding: FragmentTestBinding;

    // On peut ensuite récupérer automatiquement le viewmodel de cette façon
    // Cela nous permettera alors d'intéragir avec la base de donnée de l'application plus simplement
    // private val testViewModel: TestViewModel by viewModels()

    // Dans notre cas le viewmodel a besoin de paramètres défini dans la factory du viewmodel plus tot
    private val placeDetailViewModel : PlaceDetailViewModel by viewModels {
         // On appelle la méthode du factory de viewmodel pour créer le viewmodel
         PlaceDetailViewModelFactory(args.placeId) {
              // On passe le code pour naviguer vers un autre fragment comme lambda du viewmodel
              // Nous allons voir juste après comment définir les arguments des fragments
              // La classe Directions est automatiquement générée pour chaque classe définie dans le plan de navigation
              findNavController().navigate(
                  TestFragmentDirections.selectDate(it)
              )
         }
    }

    // Un fragment peut récupérer les arguments définis dans le plan de navigation en utilisant ceci
    private val args: PlaceDetailFragmentArgs by navArgs()
    // Ensuite on peut accéder aux propriétés en faisant args.nomDeLaPropriete

    // La méthode override onCreateView est appelée au moment de l'instantiation de la vue
    // On va ici l'utiliser pour intialiser le binding que l'on a définit plus tot en récupérant le layout
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTestBinding.inflate(inflater, container, false);
        // Il est nécessaire de préciser ceci pour faire fonctionner le binding view--viewmodel
        binding.lifecycleOwner = this
        return binding.root;
    }

    // La méthode onViewCreated est appelée lorsque la vue a fini d'être crée, on peut alors récupérer les éléments de la vue et intéragir avec
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // On peut alors activer le binding une fois que la vue a été créé
        binding.viewModel = homeViewModel
        binding.executePendingBindings()
    }
}
```

Une fois que le fragment est créé, on peut alors l\'ajouter dans le plan
de navigation. On peut donc ouvrir le fichier `nav_graph.xml` défini à
la création du projet. Une fois cela fait on peut ajouter les différents
fragments à lier et les lier entre eux dans l\'interface

#image("Guide_Android/2024-04-26_11-34-06_screenshot.png")

Ensuite on peut aussi corriger certaines choses dans le code XML qui est
généré, principalement les ID des actions pour passer vers d\'autres
fragments et pour avoir le preview dans l\'IDE. Pour avoir le preview on
peut ajouter un élément tel que
`tools:layout="@layout/fragment_place_list"` qui va ajouter un preview
du layout dans l\'IDE pour le fragment.

Ou modifier les ID des actions vers quelque chose de plus court. Ces ID
deviennent alors les méthodes qui permettent de naviguer entre plusieurs
fragment. Par exemple l\'ID `select_date` devient alors la méthode
`selectDate` de la classe Directions générée.

Il est aussi possible comme dit plus tot d\'introduire des arguments à
cette méthode en ajoutant un élément \"argument\" dans le code XML du
fragment :

```xml
<argument android:name="place_id" app:argType="java.util.UUID"/>
```

Une fois cela fait, avec tout ce qui a été vu plus tot, votre
application devrait être fonctionnelle.

== Ajouter un menu
<ajouter-un-menu>
Pour pouvoir ajouter un menu en haut de l\'application il faut commencer
par modifier le thème de l\'application pour ajouter une barre au
dessus. Dans le fichier `themes.xml` on peut alors ajouter ceci :

```xml
<style name="Theme.TestApplication" parent="Theme.MaterialComponents.Light.DarkActionBar" />
```

Une fois cela fait, on peut alors créer le menu lui-même en allant dans
res \> new \> android resource file et définir la création d\'un nouveau
menu.

#image("Ajouter_un_menu/2024-04-28_11-25-07_screenshot.png")

#image("Ajouter_un_menu/2024-04-28_11-24-46_screenshot.png")

Pour pouvoir faire un truc un peu plus joli, on va ajouter une icone
dans le menu. Pour cela, on va aller créer un nouveau \"vector asset\"
depuis le dossier \"res\"

#image("Ajouter_un_menu/2024-04-28_11-29-34_screenshot.png")

On peut alors sélectionner une icone déjà existante dans android,
sélectionner sa couleur et confirmer

#image("Ajouter_un_menu/2024-04-28_11-30-05_screenshot.png")

Une fois l\'icone ajoutée on peut retourner dans le menu et ajouter un
nouvel item

```xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item xmlns:app="http://schemas.android.com/apk/res-auto"
          android:id="@+id/add_button"
          android:title="Ajouter"
          android:icon="@drawable/ic_plus"
          app:showAsAction="always" />
</menu>
```

Une fois que l\'item est ajouté dans le menu il faut maintenant faire en
sorte que le fragment utilise le menu en question. Il faut donc que le
fragment implémente `MenuProvider`. Ceci doit être sur le fragment
auquel les actions seront liées. La barre elle, restera quoi qu\'il
arrive car elle fait maitenant partie de l\'application, seul le contenu
de son menu varie.

Lorsque vous implémentez MenuProvider sur la classe du fragmnet, il vous
demandera d\'implémenter deux méthodes :

```kotlin
// Cette méthode va créer le menu depuis les ressources
override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
    menuInflater.inflate(R.menu.nom_du_menu, menu)
}

// Cette méthode est appellée lorsque l'on clique sur un élément du menu, elle retourne true si l'opération est supportée et false dans le cas contraire
// Ici, si l'action correspond à "new_place" alors on appelle une méthode et on retourne true, et on retourne false dans tous les autres cas
override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
    return when(menuItem.itemId) {
        R.id.new_place -> {
            showNewPlace()
            true
        }
        else -> false
    }
}
```

Ensuite on peut ajouter une nouvelle ligne pour définir la classe comme
menu provider dans la méthode `onViewCreated`

```kotlin
requireActivity().addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
```

Le menu devrait à présent être opérationel.

== Ajouter une liste dynamique et scrollable \(RecyclerView)
<ajouter-une-liste-dynamique-et-scrollable-recyclerview>
Jusqu\'ici on a uniquement vu des layouts qui font simplement la taille
de l\'écran. Mais nous n\'avons pas encore vu de moyen de faire des
listes d\'éléments longues qui nécessiterait une scrollbar.

Pour faire cela il faut utiliser l\'élément RecyclerView, cependant
celui-ci est plus complexe que les autres que l\'on a vu jusqu\'ici.

On peut alors commencer par créer un nouveau layout de type
\"RecyclerView\" du genre \"fragment#sub[list];\"

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.recyclerview.widget.RecyclerView xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/place_recycler_view"
    android:layout_width="match_parent"
    android:layout_height="match_parent"/>
```

Il est également important de créer un viewmodel comme décrit plus tot
pour représenter UN élément de la liste, voici un exemple

```xml
package be.helmo.placereport.viewmodel

import be.helmo.placereport.model.Place

class PlaceListItemViewModel(private val place: Place) {

    val df: DateFormatter = DateFormatter()

    val id = place.id
    val name = place.name
    val date = df.fromDate(place.date)
}
```

Ensuite on peut créer un layout représentant un seul élément et qui se
bind au viewmodel que l\'on vient de créer.

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto">
    <data>
        <variable
            name="viewModel"
            type="be.helmo.placereport.viewmodel.PlaceListItemViewModel" />
    </data>
    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:padding="8dp">

        <TextView
            android:id="@+id/place_name"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            tools:text="Place name"
            android:text="@{viewModel.name}"
            app:layout_constraintBottom_toTopOf="@id/place_date"
            app:layout_constraintEnd_toStartOf="@+id/imageView"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/place_date"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            tools:text="Place date"
            android:text="@{viewModel.date}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toStartOf="@+id/imageView"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/place_name" />

        <ImageView
            android:id="@+id/imageView"
            android:layout_width="40dp"
            android:layout_height="40dp"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
```

Ensuite on doit créer un RecyclerView holder qui va s\'occuper
d\'afficher un élément et de mettre en place le binding.

```kotlin
class PlaceHolder(val binding: ListItemPlaceBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(place: PlaceListItemViewModel, onPlaceClicked: (placeId: UUID) -> Unit) {
        binding.viewModel = place
        binding.executePendingBindings()
        binding.root.setOnClickListener {
            onPlaceClicked(place.id)
        }
    }
}
```

Ensuite il faut mettre en place un RecyclerView Adapter qui va prendre
une liste d\'éléments et les faire afficher à l\'aide de la classe
Holder que l\'on vient de créer.

```kotlin
// Notez la présnece de la lambda onPlaceClicked qui est passée jusqu'au placeholder pour pouvoir créer des évènements sur le clic d'éléments de la liste
class PlaceListAdapter(
    private val places: List<PlaceListItemViewModel>,
    private val onPlaceClicked: (placeId: UUID) -> Unit
) : RecyclerView.Adapter<PlaceHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) : PlaceHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemPlaceBinding.inflate(inflater, parent, false)
        return PlaceHolder(binding)
    }
    override fun onBindViewHolder(holder: PlaceHolder, position: Int) {
        holder.bind(places[position], onPlaceClicked)
    }
    override fun getItemCount() = places.size
}
```

Ensuite dans le fragment qui correspond à la liste, on peut maintenant
appeller l\'adapter pour afficher la liste. On peut alors ajouter un
code comme celui-ci dans la méthode `onViewCreated`

```kotlin
viewLifecycleOwner.lifecycleScope.launch {
    viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
        // On récupère un flow d'éléménts depuis le viewmodel du fragment et on en extrait la liste à l'intérieur avec .collect
        // Ensuite on définit l'adapter du recyclerview à celui que l'on a créé plus tot
        // C'est également ici que l'on peut définir la lambda associée au clic d'un fragment
        placeListViewModel.places.collect { places ->
            Log.d(this.javaClass.name, "Total places: ${places.size}")
            binding.placeRecyclerView.adapter = PlaceListAdapter(places) {id ->
                showPlace(id)
            }
        }
    }
}
```

Il faut également définir le layoutmanager du recyclerview pour indiquer
de quelle façon les éléments doivent se suivre dans la liste. On peut
par exemple choisir LinearLayoutManager pour avoir quelque chose qui se
suit, ou GridLayoutManager pour avoir une grille. Pour cela il faut
ajouter une ligne comme celle-ci dans le `onCreateView`

```kotlin
binding.placeRecyclerView.layoutManager = LinearLayoutManager(context)
```
